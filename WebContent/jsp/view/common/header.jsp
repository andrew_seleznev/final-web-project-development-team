<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="devteamtags" prefix="dtg"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<form name="createTrForm" method="get" action="<c:url value="/controller"/>">
		<input type="hidden" name="command" value="change_locale" />
		<div id="header">
			<div id="visitorInfo"><dtg:visitor-info visitor="${ loggedVisitor }"/></div>
			<div class="lang_btn">
			<c:if test="${ not empty loggedVisitor }"><a href="controller?command=logout"><fmt:message key="index.logout" /></a></c:if>
			<input type="submit" name="language" value="ru"/>
			<input type="submit" name="language" value="en"/></div>
			<div id="message"><c:out value="${message}" /></div>
		</div>
	</form>
</body>
</fmt:bundle>
</html>