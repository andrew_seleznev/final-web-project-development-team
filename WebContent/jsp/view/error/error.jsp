<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="devteamtags" prefix="dtg"%>

<html>
<fmt:setLocale value="${locale}" />
<link type="text/css" rel="stylesheet"
	href="<c:url value="/css/style.css" />" />
<fmt:bundle basename="properties.text" >
<head>
<title>Welcome</title>
</head>
<body>
	<div id="container">
		<div id="menu">
			<h3>ERROR PAGE</h3>
		</div>
		<div id="main_panel">
			<jsp:useBean id="header" class="java.util.LinkedList" scope="page"/>
			<c:set var="*" scope="page" value="${pageScope.header.offer('Request from:')}"></c:set>
			<c:set var="*" scope="page" value="${pageScope.header.offer('Servlet name or type:')}"></c:set>
			<c:set var="*" scope="page" value="${pageScope.header.offer('Status code:')}"></c:set>
			<c:set var="*" scope="page" value="${pageScope.header.offer('Exception:')}"></c:set>
			<c:set var="*" scope="page" value="${pageScope.header.offer('Error message:')}"></c:set>
			<jsp:useBean id="values" class="java.util.LinkedList" scope="page"/>
			<c:set var="*" scope="page" value="${pageScope.values.offer(pageContext.errorData.requestURI)}"></c:set>
			<c:set var="*" scope="page" value="${pageScope.values.offer(pageContext.errorData.servletName)}"></c:set>
			<c:set var="*" scope="page" value="${pageScope.values.offer(pageContext.errorData.statusCode)}"></c:set>
			<c:set var="*" scope="page" value="${pageScope.values.offer(pageContext.errorData.throwable)}"></c:set>
			<c:set var="*" scope="page" value="${pageScope.values.offer(errorMessage)}"></c:set>
			<hr>
			<div class="dev_team_error_table">
			<dtg:table-error rows="${pageScope.header.size()}" list="${pageScope.header}">
				${pageScope.values.poll()}
			</dtg:table-error>
			</div>
			<hr>
			<a href="controller?command=logout">Back to main page</a>
		</div>
	</div>
	<c:import url="../common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>