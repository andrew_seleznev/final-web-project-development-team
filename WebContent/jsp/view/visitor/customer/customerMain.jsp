<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" prefix="customer.">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<head>
<title><fmt:message key="main_label" /></title>
</head>
<body>
	<c:import url="../../common/header.jsp" charEncoding="UTF-8" />
	<div id="container">
		<div id="menu">
			<br>
			<h3><fmt:message key="main_label" /></h3>
		</div>
		<div id="main_panel">
			<form name="viewAllTechnicalRequirementsForm" method="post" action="controller">
				<input type="hidden" name="command" value="view_all_tr" /> 
				<div class="btn"><input type="submit" value="<fmt:message key="tr_view_btn" />" /></div>
			</form>	
			<hr>
			<form name="createTrForm" method="post" action="controller">
				<input type="hidden" name="command" value="create_tr" />
				<span id="bolt_text"><label><fmt:message key="create_new_tr_header" /></label></span><br>
				<br/>
				<label class="required-label"><fmt:message key="tr_title" /></label><br>
				<input type="text" name="tr_title" value="" autocomplete="off" size="50"
					title="<fmt:message key="error_tr_title_format" />" 
					pattern="^[а-яА-ЯёЁa-zA-Z0-9\s-]{1,20}$" required="required"/> <br/>
				<label class="required-label"><fmt:message key="tr_time" /></label><br>
				<input type="text" name="tr_time" value="" autocomplete="off" size="50" 
					title="<fmt:message key="error_tr_time_format" />" 
					pattern="^[\d]{1,10}$" required="required"/> <br/>
				<label class="required-label"><fmt:message key="tr_description" /></label><br>
				<textarea rows="15" cols="100" name="tr_description" maxlength="10000"></textarea> <br/>
				<br/>
				<hr>
				<div class="btn"><input type="submit" value="<fmt:message key="tr_create_btn" />" /></div>
			</form>
		</div>
	</div>
	<c:import url="../../common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>