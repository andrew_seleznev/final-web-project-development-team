<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" prefix="customer.">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<head>
<title><fmt:message key="confirm_requirement_label" /></title>
</head>
<body>
	<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<c:import url="../../common/header.jsp" charEncoding="UTF-8" />
	<div id="container">
		<div id="menu">
			<br>
			<h3><fmt:message key="confirm_requirement_label" /></h3>
		</div>
		<div id="main_panel">
			<form name="viewAllItemsInfoForm" method="post" action="controller">
				<input type="hidden" name="command" value="move_to_create_item" />
				<div class="btn"><input type="submit" value="<fmt:message key="move_to_create_item_btn" />" /></div>
			</form>
			<form name="viewAllItemsInfoForm" method="post" action="controller">
				<input type="hidden" name="command" value="remove_item" />
				<hr>
				<span id="bolt_text"><label><fmt:message key="technical_requirement_title" /></label></span> ${current_requirement.title}<br>
				<span id="bolt_text"><label><fmt:message key="technical_requirement_time" /></label></span> ${current_requirement.time}<br/>
				<span id="bolt_text"><label><fmt:message key="technical_requirement_description" /></label></span> ${current_requirement.description}<br/>
				<hr>
				<span id="bolt_text"><label><fmt:message key="selected_item_label" /></label></span>
				<input type="text" id="item_number" name="remove_item_number" size="5" readonly="readonly"/><br>
				<br>
				<div class="btn"><input type="submit" value="<fmt:message key="item_remove_btn" />" /></div>
				<hr>
			</form>
			<c:if test="${ current_requirement.items.size() > 0 }">
			<form name="viewAllItemsConfirmForm" method="post" action="controller">
				<input type="hidden" name="command" value="confirm_tr" />
				<div class="dev_team_table">
	    		<table id="complete_requirement_table_id" class="dev_team__table">
	    			<tbody>
	    			<tr>
	    				<th><fmt:message key="table_requirement_header_number" /></th>
	    				<th><fmt:message key="table_requirement_header_qualification" /></th>
	    				<th><fmt:message key="table_requirement_header_amount_of_workers" /></th>
	    				<th><fmt:message key="table_requirement_header_work_description" /></th>
	    			</tr>
    				<c:forEach var="elem" items="${current_requirement.items}" varStatus="status">
  						<tr>
  							<td><c:out value="${ status.count }" /></td>
						    <td><c:out value="${ elem.qualification }" /></td>
						    <td><c:out value="${ elem.requiredDevAmount }" /></td>
						    <td><c:out value="${ elem.itemDescription }" /></td>
  						</tr>
    				</c:forEach>
    				</tbody>
				</table>
				</div>
				<div class="btn"><input type="submit" value="<fmt:message key="tr_complete_btn" />" /></div>
			</form>
			</c:if>
		</div>
	</div>
	<c:import url="../../common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>