<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" prefix="customer.">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<head>
<title><fmt:message key="creation_requirement_label" /></title>
</head>
<body>
	<c:import url="../../common/header.jsp" charEncoding="UTF-8" />
	<div id="container">
		<div id="menu">
			<h3><fmt:message key="creation_requirement_label" /></h3>
		</div>
		<div id="main_panel">
			<form name="viewAllItemsForm" method="post" action="controller">
				<input type="hidden" name="command" value="view_complete_tr" />
				<div class="btn"><input type="submit" value="<fmt:message key="item_view_btn" />" /></div>
			</form>
			<form name="createItemForm" method="post" action="controller">
				<input type="hidden" name="command" value="create_item" />
				<hr>
				<span id="bolt_text"><label><fmt:message key="technical_requirement_title" /></label></span> ${current_requirement.title}<br>
				<span id="bolt_text"><label><fmt:message key="technical_requirement_time" /></label></span> ${current_requirement.time}<br/>
				<span id="bolt_text"><label><fmt:message key="technical_requirement_description" /></label></span> ${current_requirement.description}<br/>
				<hr>
				<label class="required-label"><fmt:message key="item_qualification_label" /></label><br>
				<select name="required_qualification">
					<c:forEach var="elem" items="${qualifications}" varStatus="status">
	  					<option value="${ elem }">${ elem }</option>
	    			</c:forEach>
				</select><br/>
				<label class="required-label"><fmt:message key="item_dev_number_label" /></label><br>
				<input type="text" name="item_dev_number" value="" autocomplete="off" size="10" 
						title="<fmt:message key="error_item_dev_number_format" />" 
						pattern="^[\d]{1,10}$" required="required"/> <br/>
				<label class="required-label"><fmt:message key="item_description_label" /></label><br>
				<textarea rows="15" cols="100" name="item_description" maxlength="10000"></textarea> <br/>
				<br/>
				<div class="btn"><input type="submit" value="<fmt:message key="item_add_btn" />" /></div>
			</form>
		</div>
	</div>
	<c:import url="../../common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>