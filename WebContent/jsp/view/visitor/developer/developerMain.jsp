<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" prefix="developer.">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<head>
<title><fmt:message key="main_label" /></title>
</head>
<body>
	<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<c:import url="../../common/header.jsp" charEncoding="UTF-8" />
	<div id="container">
		<div id="menu">
			<br>
			<h3><fmt:message key="main_label" /></h3>
		</div>
		<div id="main_panel">
			<form name="acceptTaskForm" method="post" action="controller">
				<input type="hidden" name="command" value="accept_task" /> 
				<input type="hidden" name="task_id" id="task_id" readonly="readonly">
				<input type="hidden" name="task_project_time" id="task_project_time_id" readonly="readonly">
				<input type="hidden" name="task_status" id="task_status_id" readonly="readonly"><br>
				<input type="text" name="task_time" title="<fmt:message key="error_task_time_format" />" pattern="^[\d]{1,10}$">
				<div class="btn"><input type="submit" value="<fmt:message key="accept_task_btn" />" /></div> 
			</form>	
			<form name="completeTaskForm" method="post" action="controller">
				<input type="hidden" name="command" value="complete_task" /> 
				<input type="hidden" name="complete_task" id="complete_task_id" readonly="readonly">
				<input type="hidden" name="complete_task_status" id="complete_task_status_id" readonly="readonly"><br>
				<div class="btn"><input type="submit" value="<fmt:message key="complete_task_btn" />" /></div> 
			</form>	
			
			<c:if test="${ tasks.size() > 0 }">
					<div class="dev_team_table">
						<table id="all_task_table_id" class="dev_team_table">
							<tbody>
								<tr>
									<th><fmt:message key="table_header_number" /></th>
									<th><fmt:message key="table_header_identifier" /></th>
		    						<th><fmt:message key="table_header_status" /></th>
		    						<th><fmt:message key="table_header_description" /></th>
		    						<th><fmt:message key="table_header_time_project" /></th>
		    						<th><fmt:message key="table_header_time" /></th>
		    						<th><fmt:message key="table_header_beginning" /></th>
		    						<th><fmt:message key="table_header_ending" /></th>
		    					</tr>
		    					<c:forEach var="elem" items="${ tasks }" varStatus="status">
		    						<tr>
			  							<td><c:out value="${ status.count }" /></td>
			  							<td><c:out value="${ elem.taskId }" /></td>
			  							<td><c:out value="${ elem.status.typeName }" /></td>
									    <td><c:out value="${ elem.requirement }" /></td>
									    <td><c:out value="${ elem.projectTime }" /></td>
									    <td><c:out value="${ elem.time }" /></td>
									    <fmt:parseDate value="${elem.beginning}" pattern="yyyy-MM-dd" var="beginningDate" type="date" />
										<fmt:formatDate value="${beginningDate}" var="stdBegining" type="date" dateStyle="long"/>
										<td>${stdBegining}</td>
										<fmt:parseDate value="${elem.ending}" pattern="yyyy-MM-dd" var="endingDate" type="date" />
										<fmt:formatDate value="${endingDate}" var="stdEnding" type="date" dateStyle="long"/>
										<td>${stdEnding}</td>
	  								</tr>
		    					</c:forEach>
							</tbody>
						</table>
					</div>
				</c:if>
		</div>
	</div>
	<c:import url="../../common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>