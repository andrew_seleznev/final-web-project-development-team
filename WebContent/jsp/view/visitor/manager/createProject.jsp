<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" prefix="manager.">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<head>
<title><fmt:message key="creation_project_label" /></title>
</head>
<body>
	<c:import url="../../common/header.jsp" charEncoding="UTF-8" />
	<div id="container">
		<div id="menu">
			<br>
			<h3><fmt:message key="creation_project_label" /></h3>
		</div>
		<div id="main_panel">
			<hr>
			<span id="bolt_text"><label><fmt:message key="technical_requirement_title" /></label></span> ${current_requirement.title}<br>
			<span id="bolt_text"><label><fmt:message key="technical_requirement_time" /></label></span> ${current_requirement.time}<br/>
			<span id="bolt_text"><label><fmt:message key="technical_requirement_description" /></label></span> ${current_requirement.description}<br/>
			<hr>
			<form name="referenceForm" method="get" action="controller">
				<input type="hidden" name="command" value="move_to_main_manager" /><br />
				<div class="btn"><input type="submit" value="<fmt:message key="move_to_main_btn" />" /></div>
			</form>
			<form name="createProjectForm" method="post" action="controller">
				<input type="hidden" name="command" value="create_project" />
				<span id="bolt_text"><label><fmt:message key="create_new_project_header" /></label></span><br>
				<br/>
				<label class="required-label"><fmt:message key="project_title" /></label><br>
				<input type="text" name="project_title" value="" autocomplete="off" size="50"
					title="<fmt:message key="error_tr_title_format" />" 
					pattern="^[а-яА-ЯёЁa-zA-Z0-9\s-]{1,100}$" required="required"/> <br/>
				<label class="required-label"><fmt:message key="project_description" /></label><br>
				<textarea rows="15" cols="100" name="project_description" maxlength="10000"></textarea> <br/>
				<br/>
				<div class="btn"><input type="submit" value="<fmt:message key="create_project_btn" />" /></div>
			</form>
		</div>
	</div>
	<c:import url="../../common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>