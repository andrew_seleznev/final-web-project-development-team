<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" prefix="manager.">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<head>
<title><fmt:message key="developers_label" /></title>
</head>
<body>
	<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<c:import url="../../common/header.jsp" charEncoding="UTF-8" />
	<div id="container">
		<div id="menu">
			<br>
			<h3><fmt:message key="developers_label" /></h3>
		</div>
		<div id="main_panel">
			<c:if test="${ developers.size() > 0 }">
				<form name="createTaskForm" method="post" action="controller">
					<input type="hidden" name="command" value="create_task" />
					<span id="bolt_text"><label><fmt:message key="create_new_task_header" /></label></span><br>
					<br/>
					<label><fmt:message key="dev_first_name" /></label><br>
					<input type="text" id="selected_dev_first_name_id" readonly="readonly"><br />
					<label><fmt:message key="dev_last_name" /></label><br>
					<input type="text" id="selected_dev_last_name_id" readonly="readonly"><br />
					<input type="hidden" id="selected_dev_id" name="developer_id" readonly="readonly"><br />
					<label class="required-label"><fmt:message key="task_description" /></label><br>
					<textarea rows="15" cols="100" name="task_requirement" maxlength="10000"></textarea> <br/>
					<br/>
					<div class="btn"><input type="submit" value="<fmt:message key="create_task_btn" />" /></div>
				</form>
				<form name="referenceForm" method="get" action="controller">
					<input type="hidden" name="command" value="move_to_assign_developer" />
					<div class="btn"><input type="submit" value="<fmt:message key="move_back_btn" />" /></div>
				</form>
				<div class="dev_team_table">
					<table id="developer_table_id" class="dev_team_table">
						<tbody>
							<tr>
								<th><fmt:message key="table_header_number" /></th>
		    					<th><fmt:message key="table_header_identifier" /></th>
		    					<th><fmt:message key="table_header_login" /></th>
		    					<th><fmt:message key="table_header_first_name" /></th>
		    					<th><fmt:message key="table_header_last_name" /></th>
		    					<th><fmt:message key="table_header_employment_type" /></th>
		    					<th><fmt:message key="table_header_qualification" /></th>
		    				</tr>
		    				<c:forEach var="elem" items="${ developers }" varStatus="status">
		    					<tr>
			  						<td><c:out value="${ status.count }" /></td>
			  						<td><c:out value="${ elem.visitorID }" /></td>
								    <td><c:out value="${ elem.login }" /></td>
								    <td><c:out value="${ elem.firstName }" /></td>
								    <td><c:out value="${ elem.lastName }" /></td>
								    <td><c:out value="${ elem.employmentType.typeName }" /></td>
								    <td><c:out value="${ elem.qualification.typeName }" /></td>
	  							</tr>
		    				</c:forEach>
						</tbody>
					</table>
				</div>
			</c:if>
		</div>
	</div>
	<c:import url="../../common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>