<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" prefix="manager.">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<head>
<title><fmt:message key="requirement_details_label" /></title>
</head>
<body>
	<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<c:import url="../../common/header.jsp" charEncoding="UTF-8" />
	<div id="container">
		<div id="menu">
			<br>
			<h3><fmt:message key="requirement_details_label" /></h3>
		</div>
		<div id="main_panel">
			<form name="referenceForm" method="get" action="controller">
				<input type="hidden" name="command" value="move_to_main_manager" />
				<div class="btn"><input type="submit" value="<fmt:message key="move_to_main_btn" />" /></div>
			</form>
			<hr>
			<c:if test="${ current_requirement.items.size() > 0 }">
				<div class="dev_team_table">
					<table id="tr_details_table_id" class="dev_team_table">
						<tbody>
							<tr>
		    					<th><fmt:message key="table_header_number" /></th>
		    					<th><fmt:message key="table_header_qualification" /></th>
		    					<th><fmt:message key="table_header_amount_of_workers" /></th>
		    					<th><fmt:message key="table_header_description" /></th>
		    				</tr>
		    				<c:forEach var="elem" items="${ current_requirement.items }" varStatus="status">
		    					<tr>
									<td><c:out value="${ status.count }" /></td>
									<td><c:out value="${ elem.qualification.typeName }" /></td>
								    <td><c:out value="${ elem.requiredDevAmount }" /></td>
								    <td><c:out value="${ elem.itemDescription }" /></td>
	  							</tr>
		    				</c:forEach>
						</tbody>
					</table>
				</div>
				<hr>
				<form name="createTrForm" method="get" action="controller">
					<input type="hidden" name="command" value="move_to_create_project" />
					<div class="btn"><input type="submit" value="<fmt:message key="move_to_create_project_btn" />" /></div>
				</form>
			</c:if>
		</div>
	</div>
	<c:import url="../../common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>