<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" prefix="manager.">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<head>
<title><fmt:message key="bill_details_label" /></title>
</head>
<body>
	<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<c:import url="../../common/header.jsp" charEncoding="UTF-8" />
	<div id="container">
		<div id="menu">
			<br>
			<h3><fmt:message key="bill_details_label" /></h3>
		</div>
		<div id="main_panel">
			<form name="referenceForm" method="get" action="controller">
				<input type="hidden" name="command" value="move_to_main_manager" />
				<div class="btn"><input type="submit" value="<fmt:message key="move_to_main_btn" />" /></div>
			</form>
			<hr>
			<c:if test="${ bill.size() > 0 }">
				<div class="dev_team_table">
					<table id="tr_details_table_id" class="dev_team_table">
						<tbody>
							<tr>
		    					<th><fmt:message key="table_header_number" /></th>
		    					<th><fmt:message key="table_header_qualification" /></th>
		    					<th><fmt:message key="table_header_time" /></th>
		    					<th><fmt:message key="table_header_rate" /></th>
		    					<th><fmt:message key="table_header_description" /></th>
		    				</tr>
		    				<c:forEach var="elem" items="${ bill }" varStatus="status">
		    					<tr>
									<td><c:out value="${ status.count }" /></td>
									<td><c:out value="${ elem.qualification.typeName }" /></td>
								    <td><c:out value="${ elem.time }" /></td>
								    <td><fmt:formatNumber value="${ elem.rate }" type="currency" currencySymbol="$"/></td>
								    <td><c:out value="${ elem.description }" /></td>
	  							</tr>
		    				</c:forEach>
						</tbody>
					</table>
				</div>
				<label><fmt:message key="total_price_label" /></label><br>
				<input type="text" value="${ total_price }" readonly="readonly"><br>
				<br>
				<form name="confirmBillForm" method="post" action="controller">
					<input type="hidden" name="command" value="confirm_bill" />
					<div class="btn"><input type="submit" value="<fmt:message key="confirm_btn" />" /></div>
				</form>
			</c:if>
		</div>
	</div>
	<c:import url="../../common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>