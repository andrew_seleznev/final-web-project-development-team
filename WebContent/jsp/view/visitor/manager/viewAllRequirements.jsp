<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" prefix="manager.">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<head>
<title><fmt:message key="all_requirements_label" /></title>
</head>
<body>
	<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<c:import url="../../common/header.jsp" charEncoding="UTF-8" />
	<div id="container">
		<div id="menu">
			<br>
			<h3><fmt:message key="all_requirements_label" /></h3>
		</div>
		<div id="main_panel">
			<form name="requirementDetailsForm" method="post" action="controller">
				<input type="hidden" name="command" value="view_requirement_details_manager" />
				<input type="hidden" id="requirement_id" name="view_requirement_id" />
				<div class="btn"><input type="submit" value="<fmt:message key="view_requirement_details_btn" />" /></div>
			</form>
			<hr>
				<c:if test="${ requirements.size() > 0 }">
					<div class="dev_team_table">
						<table id="all_requirement_manager_table_id" class="dev_team_table">
							<tbody>
								<tr>
		    						<th><fmt:message key="table_header_number" /></th>
		    						<th><fmt:message key="table_header_identifier" /></th>
		    						<th><fmt:message key="table_header_status" /></th>
		    						<th><fmt:message key="table_header_title" /></th>
		    						<th><fmt:message key="table_header_time" /></th>
		    						<th><fmt:message key="table_header_cost" /></th>
		    						<th><fmt:message key="table_header_description" /></th>
		    					</tr>
		    					<c:forEach var="elem" items="${ requirements }" varStatus="status">
		    						<tr>
			  							<td><c:out value="${ status.count }" /></td>
			  							<td><c:out value="${ elem.requirementId }" /></td>
									    <td><c:out value="${ elem.status.typeName }" /></td>
									    <td><c:out value="${ elem.title }" /></td>
									    <td><c:out value="${ elem.time }" /></td>
									    <td><fmt:formatNumber value="${ elem.price }" type="currency" currencySymbol="$" /></td>
									    <td><c:out value="${ elem.description }" /></td>
	  								</tr>
		    					</c:forEach>
							</tbody>
						</table>
					</div>
				</c:if>
			<hr>
			<form name="referenceForm" method="get" action="controller">
				<input type="hidden" name="command" value="move_to_main_manager" />
				<div class="btn"><input type="submit" value="<fmt:message key="move_to_main_btn" />" /><br /></div>
			</form>
		</div>
	</div>
	<c:import url="../../common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>