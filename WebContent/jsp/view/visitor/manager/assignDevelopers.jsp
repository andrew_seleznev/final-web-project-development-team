<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" prefix="manager.">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<head>
<title><fmt:message key="assign_developer_label" /></title>
</head>
<body>
	<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<c:import url="../../common/header.jsp" charEncoding="UTF-8" />
	<c:set var="qualification" value="${ current_requirement.items.get(item_index - 1).qualification }" scope="page"/>
	<c:set var="requiredDevAmount" value="${ current_requirement.items.get(item_index - 1).requiredDevAmount }" scope="page"/>
	<c:set var="assignedDevAmount" value="${ current_requirement.items.get(item_index - 1).assignedDevAmount }" scope="page"/>
	<c:set var="itemDescription" value="${ current_requirement.items.get(item_index - 1).itemDescription }" scope="page"/>
	<div id="container">
		<div id="menu">
			<br>
			<h3><fmt:message key="assign_developer_label" /></h3>
		</div>
		<div id="main_panel">
			<hr>
			<span id="bolt_text"><label><fmt:message key="technical_requirement_title" /></label></span> ${current_requirement.title}<br>
			<span id="bolt_text"><label><fmt:message key="technical_requirement_time" /></label></span> ${current_requirement.time}<br/>
			<span id="bolt_text"><label><fmt:message key="technical_requirement_description" /></label></span> ${current_requirement.description}<br/>
			<hr>
			<span id="bolt_text"><label><fmt:message key="project_title" /></label></span> ${current_project.title}<br>
			<span id="bolt_text"><label><fmt:message key="project_time" /></label></span> ${current_project.maxTime}<br/>
			<span id="bolt_text"><label><fmt:message key="project_description" /></label></span> ${current_project.description}<br/>
			<hr>
			<c:if test="${ current_requirement.items.size() > 0 }">
				<div class="dev_team_table">
					<table id="tr_details_table_id" class="dev_team_table">
						<tbody>
							<tr>
		    					<th><fmt:message key="table_header_qualification" /></th>
		    					<th><fmt:message key="table_header_required" /></th>
		    					<th><fmt:message key="table_header_assigned" /></th>
		    					<th><fmt:message key="table_header_description" /></th>
		    				</tr>
		    				<tr>
								<td><c:out value="${ qualification.typeName }" /></td>
							    <td><c:out value="${ requiredDevAmount }" /></td>
							    <td><c:out value="${ assignedDevAmount }" /></td>
							    <td><c:out value="${ itemDescription }" /></td>
	  						</tr>
						</tbody>
					</table>
				</div>
				
				<c:if test="${ current_assigned_developers.size() > 0 }">
				<div class="dev_team_table">
					<table id="developer_table_id" class="dev_team_table">
						<tbody>
							<tr>
								<th><fmt:message key="table_header_number" /></th>
		    					<th><fmt:message key="table_header_identifier" /></th>
		    					<th><fmt:message key="table_header_login" /></th>
		    					<th><fmt:message key="table_header_first_name" /></th>
		    					<th><fmt:message key="table_header_last_name" /></th>
		    					<th><fmt:message key="table_header_employment_type" /></th>
		    					<th><fmt:message key="table_header_qualification" /></th>
		    				</tr>
		    				<c:forEach var="elem" items="${ current_assigned_developers }" varStatus="status">
		    					<tr>
			  						<td><c:out value="${ status.count }" /></td>
			  						<td><c:out value="${ elem.visitorID }" /></td>
								    <td><c:out value="${ elem.login }" /></td>
								    <td><c:out value="${ elem.firstName }" /></td>
								    <td><c:out value="${ elem.lastName }" /></td>
								    <td><c:out value="${ elem.employmentType.typeName }" /></td>
								    <td><c:out value="${ elem.qualification.typeName }" /></td>
	  							</tr>
		    				</c:forEach>
						</tbody>
					</table>
				</div>
				</c:if>
				<br>
				<c:if test="${ requiredDevAmount > assignedDevAmount }">
					<form name="assignDeveloperForm" method="get" action="controller">
						<input type="hidden" name="command" value="assign_developer" />
						<div class="btn"><input type="submit" value="<fmt:message key="assign_developer_btn" />" /></div>
					</form>
				</c:if>
				<c:if test="${ requiredDevAmount eq assignedDevAmount and item_index > 1 }">
					<form name="referenceForm" method="get" action="controller">
						<input type="hidden" name="command" value="view_next_item" />
						<div class="btn"><input type="submit" value="<fmt:message key="move_to_next_item_btn" />" /></div>
					</form>
				</c:if>
				<c:if test="${ (requiredDevAmount eq assignedDevAmount) and (item_index eq 1)}">
				<form name="confirmProjectForm" method="get" action="controller">
					<input type="hidden" name="command" value="confirm_project" />
					<div class="btn"><input type="submit" value="<fmt:message key="confirm_project_btn" />" /></div>
				</form>
				<form name="referenceForm" method="get" action="controller">
					<input type="hidden" name="command" value="move_to_main_manager" />
					<div class="btn"><input type="submit" value="<fmt:message key="move_to_main_btn" />" /><br /></div>
				</form>
				</c:if>
				<form name="canceledRequirementForm" method="post" action="controller">
					<input type="hidden" name="command" value="cancel_requirement" />
					<div class="btn"><input type="submit" value="<fmt:message key="canceled_requirement_btn" />" /></div>
				</form>
			</c:if>
		</div>
	</div>
	<c:import url="../../common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>