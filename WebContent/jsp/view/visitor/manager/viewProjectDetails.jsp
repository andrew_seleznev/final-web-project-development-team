<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" prefix="manager.">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<head>
<title><fmt:message key="project_details_label" /></title>
</head>
<body>
	<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<c:import url="../../common/header.jsp" charEncoding="UTF-8" />
	<div id="container">
		<div id="menu">
			<br>
			<h3><fmt:message key="project_details_label" /></h3>
		</div>
		<div id="main_panel">
			<form name="referenceForm" method="get" action="controller">
				<input type="hidden" name="command" value="move_to_main_manager" />
				<div class="btn"><input type="submit" value="<fmt:message key="move_to_main_btn" />" /><br /></div>
			</form>
			<hr>
			<c:if test="${ work_project.tasks.size() > 0 }">
				<div class="dev_team_table">
					<table id="tr_details_table_id" class="dev_team_table">
						<tbody>
							<tr>
		    					<th><fmt:message key="table_header_number" /></th>
		    					<th><fmt:message key="table_header_task_identifier" /></th>
		    					<th><fmt:message key="table_header_developer_identifier" /></th>
		    					<th><fmt:message key="table_header_requirement" /></th>
		    					<th><fmt:message key="table_header_status" /></th>
		    					<th><fmt:message key="table_header_time" /></th>
		    					<th><fmt:message key="table_header_beginning" /></th>
		    					<th><fmt:message key="table_header_ending" /></th>
		    				</tr>
		    				<c:forEach var="elem" items="${ work_project.tasks }" varStatus="status">
		    					<tr>
									<td><c:out value="${ status.count }" /></td>
									<td><c:out value="${ elem.taskId }" /></td>
								    <td><c:out value="${ elem.developerId }" /></td>
								    <td><c:out value="${ elem.requirement }" /></td>
								    <td><c:out value="${ elem.status.typeName }" /></td>
								    <td><c:out value="${ elem.time }" /></td>
								    <fmt:parseDate value="${elem.beginning}" pattern="yyyy-MM-dd" var="beginningDate" type="date" />
									<fmt:formatDate value="${beginningDate}" var="stdBegining" type="date" dateStyle="long"/>
									<td>${stdBegining}</td>
									<fmt:parseDate value="${elem.ending}" pattern="yyyy-MM-dd" var="endingDate" type="date" />
									<fmt:formatDate value="${endingDate}" var="stdEnding" type="date" dateStyle="long"/>
									<td>${stdEnding}</td>
	  							</tr>
		    				</c:forEach>
						</tbody>
					</table>
				</div>
				<hr>
				<form name="createTrForm" method="get" action="controller">
					<input type="hidden" name="command" value="create_bill" />
					<div class="btn"><input type="submit" value="<fmt:message key="create_bill_btn" />" /></div>
				</form>
				<form name="finishProjectForm" method="get" action="controller">
					<input type="hidden" name="command" value="finish_project" />
					<div class="btn"><input type="submit" value="<fmt:message key="finish_project_btn" />" /></div>
				</form>
			</c:if>
		</div>
	</div>
	<c:import url="../../common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>