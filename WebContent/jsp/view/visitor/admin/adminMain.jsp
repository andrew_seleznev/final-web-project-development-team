<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" prefix="admin.">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<head>
<title><fmt:message key="main_label" /></title>
</head>
<body>
	<c:import url="../../common/header.jsp" charEncoding="UTF-8" />
	<div id="container">
		<div id="menu">
			<br>
			<h3><fmt:message key="main_label" /></h3>
		</div>
		<div id="main_panel">
			<form name="viewTrForm" method="post" action="controller">
				<input type="hidden" name="command" value="view_all_requirements_admin" /> 
				<div class="btn"><input type="submit" value="<fmt:message key="view_all_requirements_btn" />" /></div>
			</form>
		</div>
	</div>
	<c:import url="../../common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>