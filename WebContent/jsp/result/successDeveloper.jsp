<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" prefix="manager.">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<head>
<title><fmt:message key="confirm_label" /></title>
</head>
<body>
	<c:import url="../view/common/header.jsp" charEncoding="UTF-8" />
	<div id="container">
		<div id="menu">
			<br>
			<h3><fmt:message key="confirm_label" /></h3>
		</div>
		<div id="main_panel">
			<span id="bolt_text"><label><fmt:message key="success_info" /></label></span>
			<form name="createTrForm" method="post" action="<c:url value="/controller"/>">
				<input type="hidden" name="command" value="move_to_main_developer" /><br />
				<div class="btn"><input type="submit" value="<fmt:message key="move_to_main_btn" />" /></div>
			</form>
		</div>
	</div>
	<c:import url="../view/common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>