<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="properties.text" >
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/style.css" />
<head>
<title><fmt:message key="index.welcome_label" /></title>
</head>
<body>
	<c:import url="../view/common/header.jsp" charEncoding="UTF-8" />
	<div id="container">
		<div id="menu">
		<br>
			<h3><fmt:message key="index.welcome_label" /><br /></h3>
		</div>
		<div id="login_panel">
			<form name="loginForm" method="post" action="controller">
				<input type="hidden" name="command" value="login" /> 
				<label class="required-label"><fmt:message key="index.register_login" /></label><br>
					<input type="text" name="login" value="" 
					title="<fmt:message key="messages.error_reg_login_format" />" 
					pattern="^[a-zA-Z][\w-_\.]{1,20}$" autocomplete="off" /> <br />
				<label class="required-label"><fmt:message key="index.register_password" /></label><br>
					<input type="password" name="password" value="" 
					title="<fmt:message key="messages.error_reg_password_format" />"
					pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,20}$" autocomplete="off" /> <br />
				<p><fmt:message key="index.guest" />
					<input type="checkbox" name="guest" id="guest" />
				</p><br />
  					<div class="btn"><input type="submit" value="<fmt:message key="index.log_in" />" /></div><br />
			</form>
		</div>
		<div id="register_panel">
			<form name="registerForm" method="post" action="controller">
				<input type="hidden" name="command" value="register" />
				<label class="required-label"><fmt:message key="index.register_first_name" /></label><br>  
					<input type="text" name="reg_first_name" value="" 
						title="<fmt:message key="messages.error_reg_first_name_format" />" 
						pattern="^[А-ЯЁA-Z][а-яА-ЯёЁa-zA-Z]{1,20}$" autocomplete="off" required="required"/> <br />
				<label class="required-label"><fmt:message key="index.register_last_name" /></label><br>
					<input type="text" name="reg_last_name" value="" 
						title="<fmt:message key="messages.error_reg_last_name_format" />" 
						pattern="^[А-ЯЁA-Z][а-яА-ЯёЁa-zA-Z]{1,20}$" autocomplete="off"/> <br />
				<label class="required-label"><fmt:message key="index.register_company" /></label><br>
					<input type="text" name="reg_company" value="" 
						title="<fmt:message key="messages.error_reg_company_format" />" 
						pattern="^[^<>].[^<>]{1,20}$" autocomplete="off"/> <br />
				<label class="required-label"><fmt:message key="index.register_login" /></label><br>
					<input type="text" name="reg_login" value="" 
						title="<fmt:message key="messages.error_reg_login_format" />" 
						pattern="^[a-zA-Z][\w-_\.]{1,20}$" autocomplete="off"/> <br />
				<label class="required-label"><fmt:message key="index.register_password" /></label><br>
					<input type="password" name="reg_password" value="" 
						title="<fmt:message key="messages.error_reg_password_format" />" 
						pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,20}$" autocomplete="off"/> <br />
				<label class="required-label"><fmt:message key="index.register_email" /></label><br>
					<input type="text" name="reg_email" value="" 
						title="<fmt:message key="messages.error_reg_email_format" />" 
						pattern="^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$" autocomplete="off"/> <br />
				<label class="required-label"><fmt:message key="index.register_mobile" /></label><br>
					<input type="text" name="reg_mobile" value="" 
						title="<fmt:message key="messages.error_reg_mobile_format" />" 
						pattern="^[\d]{7,16}$"autocomplete="off"/> <br>
						<br>
  				<div class="btn"><input type="submit" value="<fmt:message key="index.register" />" /></div><br />
			</form>
		</div>
	</div>
	<c:import url="../view/common/footer.jsp" charEncoding="UTF-8" />
</body>
</fmt:bundle>
</html>