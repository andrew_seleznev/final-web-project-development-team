function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tab1").deleteRow(i);
}

function insertRow() {
    var table = document.getElementById("tab1");
    var row = table.insertRow(0);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    cell1.innerHTML = "new row";
    cell2.innerHTML = '<input type="button" value="Delete" onclick="deleteRow(this)">';
}

function addRow()
{	
	var d = document;

	var name;
	var initials;
	var posada;
    // Считываем значения с формы
    name = d.getElementById('name').value;
    initials = d.getElementById('initials').value;
    posada = d.getElementById('posada').value;

    // Находим нужную таблицу
    var tbody = d.getElementById('tab1').getElementsByTagName('TBODY')[0];

    // Создаем строку таблицы и добавляем ее
    var row = d.createElement("TR");
    tbody.appendChild(row);

    // Создаем ячейки в вышесозданной строке
    // и добавляем тх
    var td1 = d.createElement("TD");
    var td2 = d.createElement("TD");

    row.appendChild(td1);
    row.appendChild(td2);

    // Наполняем ячейки
    td1.innerHTML = name+' '+initials;
    td2.innerHTML = posada;
}

(function () {
    if (window.addEventListener) {
        window.addEventListener('load', run, false);
    } else if (window.attachEvent) {
        window.attachEvent('onload', run);
    }

    function run() {
		var t = document.getElementById('complete_requirement_table_id');
        var rows = t.rows; //rows collection - https://developer.mozilla.org/en-US/docs/Web/API/HTMLTableElement
		for (var i=0; i<rows.length; i++) {
			rows[i].onclick = function (event) {
				//event = event || window.event; // for IE8 backward compatibility
				//console.log(event, this, this.outerHTML);
				if (this.parentNode.nodeName == 'THEAD') {
					return;
				}
				var cells = this.cells; //cells collection
				var f1 = document.getElementById('item_number');
				f1.value = cells[0].innerHTML;
			};
		}
    }
})();

(function () {
    if (window.addEventListener) {
        window.addEventListener('load', run, false);
    } else if (window.attachEvent) {
        window.attachEvent('onload', run);
    }
    
    function run() {
		var t = document.getElementById('all_requirement_table_id');
        var rows = t.rows;
		for (var i=0; i<rows.length; i++) {
			rows[i].onclick = function (event) {
				if (this.parentNode.nodeName == 'THEAD') {
					return;
				}
				var cells = this.cells;
				var f1 = document.getElementById('tr_id');
				f1.value = cells[1].innerHTML;
			};
		}
    }

})();

(function () {
    if (window.addEventListener) {
        window.addEventListener('load', run, false);
    } else if (window.attachEvent) {
        window.attachEvent('onload', run);
    }
    
    function run() {
		var t = document.getElementById('all_requirement_manager_table_id');
        var rows = t.rows; //rows collection - https://developer.mozilla.org/en-US/docs/Web/API/HTMLTableElement
		for (var i=0; i<rows.length; i++) {
			rows[i].onclick = function (event) {
				//event = event || window.event; // for IE8 backward compatibility
				//console.log(event, this, this.outerHTML);
				if (this.parentNode.nodeName == 'THEAD') {
					return;
				}
				var cells = this.cells; //cells collection
				var f1 = document.getElementById('requirement_id');
				f1.value = cells[1].innerHTML;
			};
		}
    }

})();

(function () {
    if (window.addEventListener) {
        window.addEventListener('load', run, false);
    } else if (window.attachEvent) {
        window.attachEvent('onload', run);
    }
    
    function run() {
		var t = document.getElementById('developer_table_id');
        var rows = t.rows; //rows collection - https://developer.mozilla.org/en-US/docs/Web/API/HTMLTableElement
		for (var i=0; i<rows.length; i++) {
			rows[i].onclick = function (event) {
				//event = event || window.event; // for IE8 backward compatibility
				//console.log(event, this, this.outerHTML);
				if (this.parentNode.nodeName == 'THEAD') {
					return;
				}
				var cells = this.cells; //cells collection
				var f1 = document.getElementById('selected_dev_first_name_id');
				var f2 = document.getElementById('selected_dev_last_name_id');
				var f3 = document.getElementById('selected_dev_id');
				f1.value = cells[3].innerHTML;
				f2.value = cells[4].innerHTML;
				f3.value = cells[1].innerHTML;
			};
		}
    }

})();

(function () {
    if (window.addEventListener) {
        window.addEventListener('load', run, false);
    } else if (window.attachEvent) {
        window.attachEvent('onload', run);
    }
    
    function run() {
		var t = document.getElementById('all_projects_table_id');
        var rows = t.rows; //rows collection - https://developer.mozilla.org/en-US/docs/Web/API/HTMLTableElement
		for (var i=0; i<rows.length; i++) {
			rows[i].onclick = function (event) {
				//event = event || window.event; // for IE8 backward compatibility
				//console.log(event, this, this.outerHTML);
				if (this.parentNode.nodeName == 'THEAD') {
					return;
				}
				var cells = this.cells; //cells collection
				var f1 = document.getElementById('project_id');
				f1.value = cells[1].innerHTML;
			};
		}
    }

})();

(function () {
    if (window.addEventListener) {
        window.addEventListener('load', run, false);
    } else if (window.attachEvent) {
        window.attachEvent('onload', run);
    }
    
    function run() {
		var t = document.getElementById('all_task_table_id');
        var rows = t.rows; //rows collection - https://developer.mozilla.org/en-US/docs/Web/API/HTMLTableElement
		for (var i=0; i<rows.length; i++) {
			rows[i].onclick = function (event) {
				//event = event || window.event; // for IE8 backward compatibility
				//console.log(event, this, this.outerHTML);
				if (this.parentNode.nodeName == 'THEAD') {
					return;
				}
				var cells = this.cells; //cells collection
				var f1 = document.getElementById('task_id');
				var f2 = document.getElementById('task_project_time_id');
				var f3 = document.getElementById('task_status_id');
				var f4 = document.getElementById('complete_task_id');
				var f5 = document.getElementById('complete_task_status_id');
				f1.value = cells[1].innerHTML;
				f2.value = cells[4].innerHTML;
				f3.value = cells[2].innerHTML;
				f4.value = cells[1].innerHTML;
				f5.value = cells[2].innerHTML;
			};
		}
    }

})();

(function () {
    if (window.addEventListener) {
        window.addEventListener('load', run, false);
    } else if (window.attachEvent) {
        window.attachEvent('onload', run);
    }
    
    function run() {
		var t = document.getElementById('delete_requirement_table_id');
        var rows = t.rows;
		for (var i=0; i<rows.length; i++) {
			rows[i].onclick = function (event) {
				if (this.parentNode.nodeName == 'THEAD') {
					return;
				}
				var cells = this.cells;
				var f1 = document.getElementById('tr_id');
				f1.value = cells[1].innerHTML;
			};
		}
    }

})();

$(document).ready(function() {
	$('#result_requirement_table_id').on('click', 'tbody tr', function(event) {
		$(this).addClass('highlight').siblings().removeClass('highlight');
	});
});

$(document).ready(function() {
	$('#complete_requirement_table_id').on('click', 'tbody tr', function(event) {
		$(this).addClass('highlight').siblings().removeClass('highlight');
	});
});

$(document).ready(function() {
	$('#tr_details_table_id').on('click', 'tbody tr', function(event) {
		$(this).addClass('highlight').siblings().removeClass('highlight');
	});
});

$(document).ready(function() {
	$('#all_requirement_table_id').on('click', 'tbody tr', function(event) {
		$(this).addClass('highlight').siblings().removeClass('highlight');
	});
});

$(document).ready(function() {
	$('#all_requirement_manager_table_id').on('click', 'tbody tr', function(event) {
		$(this).addClass('highlight').siblings().removeClass('highlight');
	});
});

$(document).ready(function() {
	$('#developer_table_id').on('click', 'tbody tr', function(event) {
		$(this).addClass('highlight').siblings().removeClass('highlight');
	});
});

$(document).ready(function() {
	$('#all_projects_table_id, #all_task_table_id, #delete_requirement_table_id').on('click', 'tbody tr', function(event) {
		$(this).addClass('highlight').siblings().removeClass('highlight');
	});
});