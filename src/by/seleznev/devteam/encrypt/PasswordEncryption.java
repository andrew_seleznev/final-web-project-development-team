package by.seleznev.devteam.encrypt;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import by.seleznev.devteam.exception.DevTeamEncryptException;

public class PasswordEncryption {

	private static final String ENCODING_TYPE = "UTF-8";
	private static final String ENCRYPT_TYPE = "AES";
	private static final String CIPHER = "AES/CBC/PKCS5PADDING";
	private static final String ENCRYPTION_KEY = "Bar12345Bar12345";
	private static final String ENCRYPTION_VECTOR = "RandomInitVector";

	public static String encrypt(String value) throws DevTeamEncryptException {
		byte[] encrypted = null;
		try {
			IvParameterSpec iv = new IvParameterSpec(ENCRYPTION_VECTOR.getBytes(ENCODING_TYPE));
			SecretKeySpec skeySpec = new SecretKeySpec(ENCRYPTION_KEY.getBytes(ENCODING_TYPE), ENCRYPT_TYPE);
			Cipher cipher = Cipher.getInstance(CIPHER);
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
			encrypted = cipher.doFinal(value.getBytes());
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
				| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
			throw new DevTeamEncryptException(e);
		}
		return Base64.encodeBase64String(encrypted);
	}

	public static String decrypt(String encrypted) throws DevTeamEncryptException {
		byte[] original = null;
		try {
			IvParameterSpec iv = new IvParameterSpec(ENCRYPTION_VECTOR.getBytes(ENCODING_TYPE));
			SecretKeySpec skeySpec = new SecretKeySpec(ENCRYPTION_KEY.getBytes(ENCODING_TYPE), ENCRYPT_TYPE);
			Cipher cipher = Cipher.getInstance(CIPHER);
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
			original = cipher.doFinal(Base64.decodeBase64(encrypted));
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
				| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
			throw new DevTeamEncryptException(e);
		}
		return new String(original);
	}

//	public static void main(String[] args) {
//		String after = null;
//		String en = null;
//		try {
//			String before = "acrux" + "NV10";
//			System.out.println("Before encrypt: " + before);
//			en = encrypt(before);
//			System.out.println("Encrypt value: " + en);
//			after = decrypt(en);
//		} catch (DevTeamEncryptException e) {
//
//		}
//		System.out.println("After decrypt: " + after);
//	}
}
