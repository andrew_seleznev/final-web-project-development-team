package by.seleznev.devteam.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.service.impl.AdminService;

public class ViewAllTRequirementsCommand implements ActionCommand {
	
	private static final String ATTR_REQUIREMENTS = "requirements";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	
	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		List<TechnicalRequirement> requirements = null;
		AdminService adminService = new AdminService();
		try {
			requirements = adminService.defineAllTr();
			request.getSession().setAttribute(ATTR_REQUIREMENTS, requirements);
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_ADMIN_VIEW_ALL_REQUIREMENTS);
		} catch (DevTeamServiceException e) {
			throw new DevTeamApplicationException(e);
		}

	}

	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
