package by.seleznev.devteam.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.AdminService;
import by.seleznev.devteam.validator.Validator;

public class RemoveCompleteRequirementCommand implements ActionCommand {

	private static final String PARAM_REQUIREMENT_ID = "requirement_id";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	private static final String ATTR_REQUIREMENTS = "requirements";
	private static final String ATTR_MESSAGE = "message";
	private boolean isSuccess;

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		String requirementId = request.getParameter(PARAM_REQUIREMENT_ID);
		List<TechnicalRequirement> requirements = (List<TechnicalRequirement>) request.getSession()
				.getAttribute(ATTR_REQUIREMENTS);
		if (Validator.isValidId(requirementId) && Validator.isReadyToDeleteRequirement(requirements, requirementId)) {
			try {
				AdminService adminService = (AdminService) FactoryService.getInstance().getService(ServiceType.ADMIN);
				if (adminService.removeCompleteRequirement(requirementId)) {
					request.getSession().removeAttribute(ATTR_REQUIREMENTS);
					isSuccess = true;
					return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_SUCCESS_ADMIN);
				} else {
					isSuccess = false;
					request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.ERROR_REMOVE_COMPLETE_REQUIREMENT));
					return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_ADMIN_VIEW_ALL_REQUIREMENTS);
				}
			} catch (DevTeamServiceException e) {
				throw new DevTeamApplicationException(e);
			}
		} else {
			isSuccess = false;
			request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.ERROR_REMOVE_COMPLETE_REQUIREMENT));
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_ADMIN_VIEW_ALL_REQUIREMENTS);
		}
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return (isSuccess) ? RedirectionType.SEND_REDIRECT : RedirectionType.FORWARD;
	}

}
