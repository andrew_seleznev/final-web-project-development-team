package by.seleznev.devteam.command.manager;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.manager.ConfigurationManager;

public class ViewNextRequirementItemCommand implements ActionCommand {
	
	private static final String ATTR_ITEM_INDEX = "item_index";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	
	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		Integer index = (Integer) request.getSession().getAttribute(ATTR_ITEM_INDEX);
		index--;
		request.getSession().setAttribute(ATTR_ITEM_INDEX, index);
		return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_ASSIGN_DEVELOPERS);
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
