package by.seleznev.devteam.command.manager;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.manager.BillItem;
import by.seleznev.devteam.entity.manager.Project;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.ManagerService;

public class CreateBillCommand implements ActionCommand {

	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_WORK_PROJECT = "work_project";
	private static final String ATTR_BILL = "bill";
	private static final String ATTR_TOTAL_PRICE = "total_price";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		Project project = (Project) request.getSession().getAttribute(ATTR_WORK_PROJECT);
		try {
			ManagerService managerService = (ManagerService) FactoryService.getInstance()
					.getService(ServiceType.MANAGER);
			if (managerService.isPossibleCreateBill(project)) {
				List<BillItem> bill = null;
				double price = 0;
				try {
					bill = managerService.defineBillInfo(project);
					price = managerService.calculateTotalPrice(bill);
				} catch (DevTeamServiceException e) {
					throw new DevTeamApplicationException(e);
				}
				request.getSession().setAttribute(ATTR_BILL, bill);
				request.getSession().setAttribute(ATTR_TOTAL_PRICE, price);
				return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_BILL_DETAILS);
			} else {
				request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.ERROR_CREATE_BILL));
				return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_PROJECT_DETAILS);
			}
		} catch (DevTeamServiceException e) {
			throw new DevTeamApplicationException(e);
		}
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
