package by.seleznev.devteam.command.manager;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.manager.Project;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.ManagerService;
import by.seleznev.devteam.validator.Validator;

public class FinishProjectCommand implements ActionCommand {
	
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_WORK_PROJECT = "work_project";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	private boolean isSuccess;
	
	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		Project project = (Project) request.getSession().getAttribute(ATTR_WORK_PROJECT);
		if (Validator.isPossibleCompleteProject(project)) {
			try {
				ManagerService managerService = (ManagerService) FactoryService.getInstance()
						.getService(ServiceType.MANAGER);
				if (managerService.finishProject(project)) {
					isSuccess = true;
					request.getSession().removeAttribute(ATTR_WORK_PROJECT);
					return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_SUCCESS_MANAGER);
				} else {
					isSuccess = false;
					request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.ERROR_COMPLETE_PROJECT));
					return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_PROJECT_DETAILS);
				}
			} catch (DevTeamServiceException e) {
				throw new DevTeamApplicationException(e);
			}
		} else {
			isSuccess = false;
			request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.ERROR_COMPLETE_PROJECT));
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_PROJECT_DETAILS);
		}
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return (isSuccess) ? RedirectionType.SEND_REDIRECT : RedirectionType.FORWARD;
	}

}
