package by.seleznev.devteam.command.manager;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.ManagerService;
import by.seleznev.devteam.validator.Validator;

public class ViewRequirementDetailsManagerCommand implements ActionCommand {

	private static final String PARAM_VIEW_REQUIREMENT_ID = "view_requirement_id";
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_CURRENT_REQUIREMENT = "current_requirement";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		String requirementId = request.getParameter(PARAM_VIEW_REQUIREMENT_ID);
		if (Validator.isValidId(requirementId)) {
			try {
				ManagerService managerService = (ManagerService) FactoryService.getInstance()
						.getService(ServiceType.MANAGER);
				TechnicalRequirement requirement = managerService.defineRequirementById(requirementId);
				request.getSession().setAttribute(ATTR_CURRENT_REQUIREMENT, requirement);
			} catch (DevTeamServiceException e) {
				throw new DevTeamApplicationException(e);
			}
		} else {
			request.setAttribute(ATTR_MESSAGE,
					MessageManager.getProperty(MessageManager.NULL_SHOW_TR));
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_ALL_REQUIREMENTS);
		}
		return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_REQUIREMENT_DETAILS);
	}
	
	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
