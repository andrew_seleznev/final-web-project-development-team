package by.seleznev.devteam.command.manager;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.entity.visitor.Developer;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.ManagerService;

public class AssignDeveloperCommand implements ActionCommand {

	private static final String ATTR_CURRENT_REQUIREMENT = "current_requirement";
	private static final String ATTR_DEVELOPERS = "developers";
	private static final String ATTR_ITEM_INDEX = "item_index";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		TechnicalRequirement requirement = (TechnicalRequirement) request.getSession()
				.getAttribute(ATTR_CURRENT_REQUIREMENT);
		Integer index = (Integer) request.getSession().getAttribute(ATTR_ITEM_INDEX);
		try {
			ManagerService managerService = (ManagerService) FactoryService.getInstance()
					.getService(ServiceType.MANAGER);
			List<Developer> developers = managerService.defineDevelopers(requirement, index);
			request.getSession().setAttribute(ATTR_DEVELOPERS, developers);
		} catch (DevTeamServiceException e) {
			throw new DevTeamApplicationException(e);
		}
		return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_DEVELOPERS);
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
