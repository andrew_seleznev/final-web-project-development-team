package by.seleznev.devteam.command.manager;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.ManagerService;

public class ViewAllRequirementsManagerCommand implements ActionCommand {
	
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_REQUIREMENTS = "requirements";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	
	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		List<TechnicalRequirement> requirements = null;
		try {
			ManagerService managerService = (ManagerService) FactoryService.getInstance()
					.getService(ServiceType.MANAGER);
			requirements = managerService.defineAllRequirements();
			request.getSession().setAttribute(ATTR_REQUIREMENTS, requirements);
		} catch (DevTeamServiceException e) {
			request.setAttribute(ATTR_MESSAGE, e.getMessage());
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER);
		}
		return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_ALL_REQUIREMENTS);
	}
	
	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
