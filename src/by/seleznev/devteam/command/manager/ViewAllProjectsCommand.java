package by.seleznev.devteam.command.manager;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.manager.Project;
import by.seleznev.devteam.entity.visitor.Visitor;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.ManagerService;

public class ViewAllProjectsCommand implements ActionCommand {
	
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	private static final String ATTR_PROJECTS = "projects";
	private static final String ATTR_MESSAGE = "message";
	
	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		Visitor visitor = (Visitor) request.getSession().getAttribute(ATTR_LOGGED_VISITOR);
		try {
			ManagerService managerService = (ManagerService) FactoryService.getInstance()
					.getService(ServiceType.MANAGER);
			List<Project> projects = managerService.defineProjects(visitor);
			if (projects != null) {
				request.getSession().setAttribute(ATTR_PROJECTS, projects);
				return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_ALL_PROJECTS);
			} else {
				request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.ERROR_VIEW_PROJECTS));
				return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER);
			}
		} catch (DevTeamServiceException e) {
			throw new DevTeamApplicationException(e);
		}
	}
	
	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
