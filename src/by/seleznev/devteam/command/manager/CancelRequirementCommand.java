package by.seleznev.devteam.command.manager;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.ManagerService;

public class CancelRequirementCommand implements ActionCommand {

	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	private static final String ATTR_CURRENT_REQUIREMENT = "current_requirement";
	private static final String ATTR_CURRENT_PROJECT = "current_project";
	private static final String ATTR_ITEM_INDEX = "item_index";
	private static final String ATTR_CURRENT_ASSIGNED_DEVELOPERS = "current_assigned_developers";
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_DEVELOPERS = "developers";
	private boolean isSuccess;

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		TechnicalRequirement technicalRequirement = (TechnicalRequirement) request.getSession()
				.getAttribute(ATTR_CURRENT_REQUIREMENT);
		try {
			ManagerService managerService = (ManagerService) FactoryService.getInstance()
					.getService(ServiceType.MANAGER);
			boolean isCancelRequirementSuccess = managerService.cancelRequirement(technicalRequirement);
			if (isCancelRequirementSuccess) {
				request.getSession().removeAttribute(ATTR_CURRENT_REQUIREMENT);
				request.getSession().removeAttribute(ATTR_CURRENT_PROJECT);
				request.getSession().removeAttribute(ATTR_ITEM_INDEX);
				request.getSession().removeAttribute(ATTR_CURRENT_ASSIGNED_DEVELOPERS);
				request.getSession().removeAttribute(ATTR_DEVELOPERS);
				isSuccess = true;
				return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_SUCCESS_MANAGER);
			} else {
				isSuccess = false;
				request.getSession().setAttribute(ATTR_MESSAGE,
						MessageManager.getProperty(MessageManager.ERROR_DURING_CANCEL_REQUIREMENT));
				return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_ASSIGN_DEVELOPERS);
			}
		} catch (DevTeamServiceException e) {
			throw new DevTeamApplicationException(e);
		}
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return (isSuccess) ? RedirectionType.SEND_REDIRECT : RedirectionType.FORWARD;
	}

}
