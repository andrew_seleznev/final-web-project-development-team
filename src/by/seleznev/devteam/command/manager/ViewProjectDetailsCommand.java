package by.seleznev.devteam.command.manager;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.manager.Project;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.ManagerService;
import by.seleznev.devteam.validator.Validator;

public class ViewProjectDetailsCommand implements ActionCommand {

	private static final String PARAM_PROJECT_ID = "project_id";
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_WORK_PROJECT = "work_project";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		String projectId = request.getParameter(PARAM_PROJECT_ID);
		if (Validator.isValidId(projectId)) {
			Project project = null;
			try {
				ManagerService managerService = (ManagerService) FactoryService.getInstance()
						.getService(ServiceType.MANAGER);
				project = managerService.defineProjectById(projectId);
				request.getSession().setAttribute(ATTR_WORK_PROJECT, project);
			} catch (DevTeamServiceException e) {
				throw new DevTeamApplicationException(e);
			}
		} else {
			request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.ERROR_PROJECT_NOT_SELECTED));
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_ALL_PROJECTS);
		}
		return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_PROJECT_DETAILS);
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
