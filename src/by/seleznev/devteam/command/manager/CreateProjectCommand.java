package by.seleznev.devteam.command.manager;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.entity.manager.Project;
import by.seleznev.devteam.entity.visitor.Developer;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.ManagerService;
import by.seleznev.devteam.validator.Validator;

public class CreateProjectCommand implements ActionCommand {

	private static final String PARAM_PROJECT_TITLE = "project_title";
	private static final String PARAM_PROJECT_DESCRIPTION = "project_description";
	private static final String ATTR_CURRENT_ASSIGNED_DEVELOPERS = "current_assigned_developers";
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_CURRENT_REQUIREMENT = "current_requirement";
	private static final String ATTR_CURRENT_PROJECT = "current_project";
	private static final String ATTR_ITEM_INDEX = "item_index";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		String projectTitle = request.getParameter(PARAM_PROJECT_TITLE);
		String projectDescription = request.getParameter(PARAM_PROJECT_DESCRIPTION);
		if (Validator.isValidProjectCreationData(projectTitle, projectDescription)) {
			TechnicalRequirement requirement = (TechnicalRequirement) request.getSession()
					.getAttribute(ATTR_CURRENT_REQUIREMENT);
			try {
				ManagerService managerService = (ManagerService) FactoryService.getInstance()
						.getService(ServiceType.MANAGER);
				List<Developer> assignedDevelopers = new ArrayList<>();
				Project project = managerService.createProject(requirement, projectTitle, projectDescription);
				request.getSession().setAttribute(ATTR_ITEM_INDEX, requirement.getItems().size());
				request.getSession().setAttribute(ATTR_CURRENT_PROJECT, project);
				request.getSession().setAttribute(ATTR_CURRENT_ASSIGNED_DEVELOPERS, assignedDevelopers);
			} catch (DevTeamServiceException e) {
				throw new DevTeamApplicationException(e);
			}
		} else {
			request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.CHECK_ALL_REQUIRED_FIELDS));
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_CREATE_PROJECT);
		}
		return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_ASSIGN_DEVELOPERS);
	}
	
	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
