package by.seleznev.devteam.command.manager;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.entity.manager.Project;
import by.seleznev.devteam.entity.manager.Task;
import by.seleznev.devteam.entity.visitor.Developer;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.ManagerService;
import by.seleznev.devteam.validator.Validator;

public class CreateTaskCommand implements ActionCommand {

	private static final String PARAM_DEVELOPER_ID = "developer_id";
	private static final String PARAM_TASK_REQUIREMENT = "task_requirement";
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_DEVELOPERS = "developers";
	private static final String ATTR_CURRENT_PROJECT = "current_project";
	private static final String ATTR_CURRENT_ASSIGNED_DEVELOPERS = "current_assigned_developers";
	private static final String ATTR_CURRENT_REQUIREMENT = "current_requirement";
	private static final String ATTR_ITEM_INDEX = "item_index";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		String developerId = request.getParameter(PARAM_DEVELOPER_ID);
		String requirement = request.getParameter(PARAM_TASK_REQUIREMENT);
		Project project = (Project) request.getSession().getAttribute(ATTR_CURRENT_PROJECT);
		if (Validator.isValidTaskCreationData(developerId, requirement)) {
			try {
				ManagerService managerService = (ManagerService) FactoryService.getInstance()
						.getService(ServiceType.MANAGER);
				List<Developer> developers = (List<Developer>) request.getSession().getAttribute(ATTR_DEVELOPERS);
				List<Developer> assignedDevelopers = (List<Developer>) request.getSession()
						.getAttribute(ATTR_CURRENT_ASSIGNED_DEVELOPERS);
				Developer developer = managerService.defineAssignedDeveloper(developers, developerId);
				Task task = managerService.createTask(developer, requirement, project);
				if (task != null) {
					TechnicalRequirement technicalRequirement = (TechnicalRequirement) request.getSession()
							.getAttribute(ATTR_CURRENT_REQUIREMENT);
					Integer index = (Integer) request.getSession().getAttribute(ATTR_ITEM_INDEX);
					managerService.addAssignedDeveloper(technicalRequirement, index);
					project.getTasks().add(task);
					assignedDevelopers.add(developer);
					request.getSession().setAttribute(ATTR_CURRENT_PROJECT, project);
					request.getSession().setAttribute(ATTR_CURRENT_ASSIGNED_DEVELOPERS, assignedDevelopers);
					request.removeAttribute(PARAM_DEVELOPER_ID);
					request.removeAttribute(PARAM_TASK_REQUIREMENT);
				} else {
					request.setAttribute(ATTR_MESSAGE,
							MessageManager.getProperty(MessageManager.CHECK_ALL_REQUIRED_FIELDS));
					return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_DEVELOPERS);
				}
			} catch (DevTeamServiceException e) {
				throw new DevTeamApplicationException(e);
			}
		} else {
			request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.CHECK_ALL_REQUIRED_FIELDS));
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_DEVELOPERS);
		}
		return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_ASSIGN_DEVELOPERS);
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
