package by.seleznev.devteam.command.manager;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.manager.BillItem;
import by.seleznev.devteam.entity.manager.Project;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.ManagerService;

public class ConfirmBillCommand implements ActionCommand {

	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_WORK_PROJECT = "work_project";
	private static final String ATTR_BILL = "bill";
	private static final String ATTR_TOTAL_PRICE = "total_price";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	private boolean isSuccess;

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		Project project = (Project) request.getSession().getAttribute(ATTR_WORK_PROJECT);
		List<BillItem> bill = (List<BillItem>) request.getSession().getAttribute(ATTR_BILL);
		Double price = (Double) request.getSession().getAttribute(ATTR_TOTAL_PRICE);
		try {
			ManagerService managerService = (ManagerService) FactoryService.getInstance()
					.getService(ServiceType.MANAGER);
			if (managerService.addBill(project, bill, price)) {
				request.getSession().removeAttribute(ATTR_BILL);
				request.getSession().removeAttribute(ATTR_WORK_PROJECT);
				request.getSession().removeAttribute(ATTR_TOTAL_PRICE);
				isSuccess = true;
				return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_SUCCESS_MANAGER);
			} else {
				request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.ERROR_CONFIRM_BILL));
				return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_BILL_DETAILS);
			}
		} catch (DevTeamServiceException e) {
			throw new DevTeamApplicationException(e);
		}
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return (isSuccess) ? RedirectionType.SEND_REDIRECT : RedirectionType.FORWARD;
	}

}
