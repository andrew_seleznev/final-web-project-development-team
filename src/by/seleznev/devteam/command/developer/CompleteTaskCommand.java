package by.seleznev.devteam.command.developer;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.DeveloperService;
import by.seleznev.devteam.validator.Validator;

public class CompleteTaskCommand implements ActionCommand {
	
	private static final String PARAM_COMPLETE_TASK_ID = "complete_task";
	private static final String PARAM_COMPLETE_TASK_STATUS = "complete_task_status";
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	private boolean isSuccess;
	
	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		String taskId = request.getParameter(PARAM_COMPLETE_TASK_ID);
		String taskStatus = request.getParameter(PARAM_COMPLETE_TASK_STATUS);
		if (Validator.isValidId(taskId) && Validator.isPossibleCompleteTask(taskStatus)) {
			try {
				DeveloperService developerService = (DeveloperService) FactoryService.getInstance()
						.getService(ServiceType.DEVELOPER);
				if (developerService.finishTask(taskId)) {
					isSuccess = true;
					return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_SUCCESS_DEVELOPER);
				} else {
					request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.ERROR_COMPLETE_TASK));
					return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_DEVELOPER);
				}
			} catch (DevTeamServiceException e) {
				throw new DevTeamApplicationException(e);
			}
		} else {
			request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.ERROR_COMPLETE_TASK));
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_DEVELOPER);
		}
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return (isSuccess) ? RedirectionType.SEND_REDIRECT : RedirectionType.FORWARD;
	}

}
