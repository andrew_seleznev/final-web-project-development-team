package by.seleznev.devteam.command.client;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.command.admin.RemoveCompleteRequirementCommand;
import by.seleznev.devteam.command.admin.ViewAllTRequirementsCommand;
import by.seleznev.devteam.command.customer.ConfirmTechnicalRequirementCommand;
import by.seleznev.devteam.command.customer.CreateItemCommand;
import by.seleznev.devteam.command.customer.CreateTechnicalRequirementCommand;
import by.seleznev.devteam.command.customer.PayBillCommand;
import by.seleznev.devteam.command.customer.RemoveItemCommand;
import by.seleznev.devteam.command.customer.ViewAllTechnicalRequirementCommand;
import by.seleznev.devteam.command.customer.ViewBillCommand;
import by.seleznev.devteam.command.customer.ViewTechnicalRequirementDetailsCommand;
import by.seleznev.devteam.command.developer.AcceptTaskCommand;
import by.seleznev.devteam.command.developer.CompleteTaskCommand;
import by.seleznev.devteam.command.manager.AssignDeveloperCommand;
import by.seleznev.devteam.command.manager.CancelRequirementCommand;
import by.seleznev.devteam.command.manager.ConfirmBillCommand;
import by.seleznev.devteam.command.manager.ConfirmProjectCommand;
import by.seleznev.devteam.command.manager.CreateBillCommand;
import by.seleznev.devteam.command.manager.CreateProjectCommand;
import by.seleznev.devteam.command.manager.CreateTaskCommand;
import by.seleznev.devteam.command.manager.FinishProjectCommand;
import by.seleznev.devteam.command.manager.ViewAllProjectsCommand;
import by.seleznev.devteam.command.manager.ViewAllRequirementsManagerCommand;
import by.seleznev.devteam.command.manager.ViewNextRequirementItemCommand;
import by.seleznev.devteam.command.manager.ViewProjectDetailsCommand;
import by.seleznev.devteam.command.manager.ViewRequirementDetailsManagerCommand;
import by.seleznev.devteam.command.reference.MoveToAssignDeveloper;
import by.seleznev.devteam.command.reference.MoveToCreateItemCommand;
import by.seleznev.devteam.command.reference.MoveToCreateProject;
import by.seleznev.devteam.command.reference.MoveToLoginCommand;
import by.seleznev.devteam.command.reference.MoveToMainAdmin;
import by.seleznev.devteam.command.reference.MoveToMainCustomerCommand;
import by.seleznev.devteam.command.reference.MoveToMainDeveloperCommand;
import by.seleznev.devteam.command.reference.MoveToMainManagerCommand;
import by.seleznev.devteam.command.reference.MoveToTechnicalRequirementListCommand;
import by.seleznev.devteam.command.visitor.ChangeLocaleCommand;
import by.seleznev.devteam.command.visitor.LoginCommand;
import by.seleznev.devteam.command.visitor.LogoutCommand;
import by.seleznev.devteam.command.visitor.RegisterCommand;

public enum CommandEnum {
	LOGIN {
		{
			this.command = new LoginCommand();
		}
	},
	LOGOUT {
		{
			this.command = new LogoutCommand();
		}
	},
	REGISTER {
		{
			this.command = new RegisterCommand();
		}
	},
	CHANGE_LOCALE {
		{
			this.command = new ChangeLocaleCommand();
		}
	},
	CREATE_TR {
		{
			this.command = new CreateTechnicalRequirementCommand();
		}
	},
	VIEW_COMPLETE_TR {
		{
			this.command = new MoveToTechnicalRequirementListCommand();
		}
	},
	CREATE_ITEM {
		{
			this.command = new CreateItemCommand();
		}
	},
	REMOVE_ITEM {
		{
			this.command = new RemoveItemCommand();
		}
	},
	MOVE_TO_CREATE_ITEM {
		{
			this.command = new MoveToCreateItemCommand();
		}
	},
	CONFIRM_TR {
		{
			this.command = new ConfirmTechnicalRequirementCommand();
		}
	},
	VIEW_ALL_TR {
		{
			this.command = new ViewAllTechnicalRequirementCommand();
		}
	},
	VIEW_TR_DETAILS {
		{
			this.command = new ViewTechnicalRequirementDetailsCommand();
		}
	},
	MOVE_TO_MAIN_CUSTOMER {
		{
			this.command = new MoveToMainCustomerCommand();
		}
	},
	VIEW_ALL_REQUIREMENTS_MANAGER {
		{
			this.command = new ViewAllRequirementsManagerCommand();
		}
	},
	VIEW_ALL_PROJECTS {
		{
			this.command = new ViewAllProjectsCommand();
		}
	},
	MOVE_TO_MAIN_MANAGER {
		{
			this.command = new MoveToMainManagerCommand();
		}
	},
	VIEW_REQUIREMENT_DETAILS_MANAGER {
		{
			this.command = new ViewRequirementDetailsManagerCommand();
		}
	},
	CREATE_PROJECT {
		{
			this.command = new CreateProjectCommand();
		}
	},
	MOVE_TO_CREATE_PROJECT {
		{
			this.command = new MoveToCreateProject();
		}
	},
	ASSIGN_DEVELOPER {
		{
			this.command = new AssignDeveloperCommand();
		}
	},
	CREATE_TASK {
		{
			this.command = new CreateTaskCommand();
		}
	},
	MOVE_TO_ASSIGN_DEVELOPER {
		{
			this.command = new MoveToAssignDeveloper();
		}
	},
	VIEW_NEXT_ITEM {
		{
			this.command = new ViewNextRequirementItemCommand();
		}
	},
	CONFIRM_PROJECT {
		{
			this.command = new ConfirmProjectCommand();
		}
	},
	VIEW_PROJECT_DETAILS {
		{
			this.command = new ViewProjectDetailsCommand();
		}
	},
	CREATE_BILL {
		{
			this.command = new CreateBillCommand();
		}
	},
	CONFIRM_BILL {
		{
			this.command = new ConfirmBillCommand();
		}
	},
	ACCEPT_TASK {
		{
			this.command = new AcceptTaskCommand();
		}
	},
	MOVE_TO_MAIN_DEVELOPER {
		{
			this.command = new MoveToMainDeveloperCommand();
		}
	},
	VIEW_BILL {
		{
			this.command = new ViewBillCommand();
		}
	},
	PAY_BILL {
		{
			this.command = new PayBillCommand();
		}
	},
	FINISH_PROJECT {
		{
			this.command = new FinishProjectCommand();
		}
	},
	COMPLETE_TASK {
		{
			this.command = new CompleteTaskCommand();
		}
	},
	VIEW_ALL_REQUIREMENTS_ADMIN {
		{
			this.command = new ViewAllTRequirementsCommand();
		}
	},
	MOVE_TO_MAIN_ADMIN {
		{
			this.command = new MoveToMainAdmin();
		}
	},
	REMOVE_COMPLETE_REQUIREMENT {
		{
			this.command = new RemoveCompleteRequirementCommand();
		}
	},
	MOVE_TO_LOGIN {
		{
			this.command = new MoveToLoginCommand();
		}
	},
	CANCEL_REQUIREMENT {
		{
			this.command = new CancelRequirementCommand();
		}
	};
	
	ActionCommand command;

	public ActionCommand getCurrentCommand() {
		return command;
	}
}
