package by.seleznev.devteam.command.factory;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.command.client.CommandEnum;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.manager.MessageManager;

public class ActionFactory {
	public ActionCommand defineCommand(HttpServletRequest request) throws DevTeamApplicationException {
		ActionCommand current = null;
		String action = request.getParameter("command");
		if (action == null || action.isEmpty()) {
			throw new DevTeamApplicationException("Null command has been called!");
		}
		try {
			CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
			current = currentEnum.getCurrentCommand();
		} catch (IllegalArgumentException e) {
			request.setAttribute("wrongAction", action + MessageManager.getProperty("message.wrongaction"));
			throw new DevTeamApplicationException("Unsupported command has been called!");
		}
		return current;
	}
}
