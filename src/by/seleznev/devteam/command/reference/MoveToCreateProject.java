package by.seleznev.devteam.command.reference;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.validator.Validator;

public class MoveToCreateProject implements ActionCommand {

	private static final String ATTR_CURRENT_REQUIREMENT = "current_requirement";
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		TechnicalRequirement technicalRequirement = (TechnicalRequirement) request.getSession()
				.getAttribute(ATTR_CURRENT_REQUIREMENT);
		if (Validator.isPossibleProjectCreation(technicalRequirement)) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_CREATE_PROJECT);
		} else {
			request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.ERROR_IMPOSSIBLE_PROJECT_CREATION));
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER_VIEW_REQUIREMENT_DETAILS);
		}
		
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
