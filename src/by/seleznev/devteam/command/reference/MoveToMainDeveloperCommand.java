package by.seleznev.devteam.command.reference;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.manager.Task;
import by.seleznev.devteam.entity.visitor.Visitor;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.service.impl.DeveloperService;

public class MoveToMainDeveloperCommand implements ActionCommand {
	
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	private static final String ATTR_TASKS = "tasks";

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		Visitor visitor = (Visitor) request.getSession().getAttribute(ATTR_LOGGED_VISITOR);
		DeveloperService developerService = new DeveloperService();
		List<Task> tasks = null;
		try {
			tasks = developerService.defineTasks(visitor);
		} catch (DevTeamServiceException e) {
			throw new DevTeamApplicationException(e);
		}
		request.getSession().setAttribute(ATTR_TASKS, tasks);
		return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_DEVELOPER);
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
