package by.seleznev.devteam.command.customer;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.CustomerService;
import by.seleznev.devteam.validator.Validator;

public class CreateTechnicalRequirementCommand implements ActionCommand {

	private static final String PARAM_TR_TITLE = "tr_title";
	private static final String PARAM_TR_TIME = "tr_time";
	private static final String PARAM_TR_DESCRIPTION = "tr_description";
	private static final String ATTR_CURRENT_REQUIREMENT = "current_requirement";
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_QUALIFICATIONS = "qualifications";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		String title = request.getParameter(PARAM_TR_TITLE);
		String time = request.getParameter(PARAM_TR_TIME);
		String description = request.getParameter(PARAM_TR_DESCRIPTION);
		if (Validator.isVilidTechnicalRequirementParameters(title, time, description)) {
			try {
				CustomerService customerService = (CustomerService) FactoryService.getInstance()
						.getService(ServiceType.CUSTOMER);
				TechnicalRequirement requirement = customerService.createRequirement(title, description, time);
				List<String> qualifications = customerService.defineAllQualifications();
				request.getSession().setAttribute(ATTR_QUALIFICATIONS, qualifications);
				request.getSession().setAttribute(ATTR_CURRENT_REQUIREMENT, requirement);
				return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_CUSTOMER_CREATE_TR);
			} catch (DevTeamServiceException e) {
				throw new DevTeamApplicationException(e);
			}
		} else {
			request.setAttribute(ATTR_MESSAGE, MessageManager.CHECK_ALL_REQUIRED_FIELDS);
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_CUSTOMER);
		}
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
