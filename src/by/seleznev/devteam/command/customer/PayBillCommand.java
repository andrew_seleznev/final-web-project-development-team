package by.seleznev.devteam.command.customer;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.CustomerService;

public class PayBillCommand implements ActionCommand {

	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_EXISTING_REQUIREMENT = "existing_requirement";
	private static final String ATTR_BILL = "bill";
	private static final String ATTR_TOTAL_PRICE = "total_price";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	private boolean isSuccess;

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		TechnicalRequirement requirement = (TechnicalRequirement) request.getSession()
				.getAttribute(ATTR_EXISTING_REQUIREMENT);
		Double price = (Double) request.getSession().getAttribute(ATTR_TOTAL_PRICE);
		try {
			CustomerService customerService = (CustomerService) FactoryService.getInstance()
					.getService(ServiceType.CUSTOMER);
			if (customerService.payBill(requirement, price)) {
				request.getSession().removeAttribute(ATTR_EXISTING_REQUIREMENT);
				request.getSession().removeAttribute(ATTR_BILL);
				request.getSession().removeAttribute(ATTR_TOTAL_PRICE);
				isSuccess = true;
				return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_SUCCESS_CUSTOMER);
			} else {
				request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.ERROR_MAKE_PAYMENT));
				return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_CUSTOMER_VIEW_BILL_DETAILS);
			}
		} catch (DevTeamServiceException e) {
			throw new DevTeamApplicationException(e);
		}
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return (isSuccess) ? RedirectionType.SEND_REDIRECT : RedirectionType.FORWARD;
	}

}
