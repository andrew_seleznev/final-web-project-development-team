package by.seleznev.devteam.command.customer;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.entity.visitor.Visitor;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.CustomerService;
import by.seleznev.devteam.validator.Validator;

public class ViewTechnicalRequirementDetailsCommand implements ActionCommand {
	
	private static final String PARAM_SHOW_TR_ID = "show_tr_id";
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	private static final String ATTR_EXISTING_REQUIREMENT = "existing_requirement";

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		String trId = request.getParameter(PARAM_SHOW_TR_ID);
		if (Validator.isValidId(trId)) {
			Visitor visitor = (Visitor) request.getSession().getAttribute(ATTR_LOGGED_VISITOR);
			Long customerId = visitor.getVisitorID();
			try {
				CustomerService customerService = (CustomerService) FactoryService.getInstance()
						.getService(ServiceType.CUSTOMER);
				TechnicalRequirement requirement = customerService.defineRequirement(customerId, trId);
				request.getSession().setAttribute(ATTR_EXISTING_REQUIREMENT, requirement);
			} catch (DevTeamServiceException e) {
				throw new DevTeamApplicationException(e);
			}
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_CUSTOMER_VIEW_TR_DETAILS);
		} else {
			request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.NULL_SHOW_TR));
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_CUSTOMER_VIEW_ALL_TR);
		}
	}
	
	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}
}
