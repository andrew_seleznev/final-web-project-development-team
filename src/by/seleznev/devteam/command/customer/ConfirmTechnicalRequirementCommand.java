package by.seleznev.devteam.command.customer;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.entity.visitor.Visitor;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.CustomerService;

public class ConfirmTechnicalRequirementCommand implements ActionCommand {

	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	private static final String ATTR_CURRENT_REQUIREMENT = "current_requirement";
	private static final String ATTR_QUALIFICATIONS = "qualifications";
	private static final String ATTR_MESSAGE = "message";
	private boolean isSuccess;

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		Visitor visitor = (Visitor) request.getSession().getAttribute(ATTR_LOGGED_VISITOR);
		TechnicalRequirement requirement = (TechnicalRequirement) request.getSession()
				.getAttribute(ATTR_CURRENT_REQUIREMENT);
		try {
			CustomerService customerService = (CustomerService) FactoryService.getInstance()
					.getService(ServiceType.CUSTOMER);
			if (customerService.addRequirement(visitor, requirement)) {
				request.getSession().removeAttribute(ATTR_CURRENT_REQUIREMENT);
				request.getSession().removeAttribute(ATTR_QUALIFICATIONS);
				isSuccess = true;
				return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_SUCCESS_CUSTOMER);
			} else {
				request.setAttribute(ATTR_MESSAGE,
						MessageManager.getProperty(MessageManager.ERROR_DURING_ADD_TECHNICAL_REQUIREMENT));
				return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_CUSTOMER_VIEW_TR);
			}

		} catch (DevTeamServiceException e) {
			throw new DevTeamApplicationException(e);
		}
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return (isSuccess) ? RedirectionType.SEND_REDIRECT : RedirectionType.FORWARD;
	}
}
