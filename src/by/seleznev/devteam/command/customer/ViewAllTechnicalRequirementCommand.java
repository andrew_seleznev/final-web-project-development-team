package by.seleznev.devteam.command.customer;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.entity.visitor.Visitor;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.CustomerService;

public class ViewAllTechnicalRequirementCommand implements ActionCommand {

	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	private static final String ATTR_REQUIREMENTS = "requirements";

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		Visitor visitor = (Visitor) request.getSession().getAttribute(ATTR_LOGGED_VISITOR);
		Long visitorId = visitor.getVisitorID();
		ArrayList<TechnicalRequirement> requirements = null;
		try {
			CustomerService customerService = (CustomerService) FactoryService.getInstance()
					.getService(ServiceType.CUSTOMER);
			requirements = customerService.defineAllTechnicalRequirements(visitorId);
			request.getSession().setAttribute(ATTR_REQUIREMENTS, requirements);
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_CUSTOMER_VIEW_ALL_TR);
		} catch (DevTeamServiceException e) {
			throw new DevTeamApplicationException(e);
		}
	}
	
	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
