package by.seleznev.devteam.command.customer;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.CustomerService;
import by.seleznev.devteam.validator.Validator;

public class RemoveItemCommand implements ActionCommand {

	private static final String PARAM_ITEM_NUMBER = "remove_item_number";
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_CURRENT_REQUIREMENT = "current_requirement";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		String item = request.getParameter(PARAM_ITEM_NUMBER);
		if (Validator.isValidId(item)) {
			TechnicalRequirement requirement = (TechnicalRequirement) request.getSession()
					.getAttribute(ATTR_CURRENT_REQUIREMENT);
			try {
				CustomerService customerService = (CustomerService) FactoryService.getInstance()
						.getService(ServiceType.CUSTOMER);
				requirement = customerService.removeItem(requirement, item);
			} catch (DevTeamServiceException e) {
				throw new DevTeamApplicationException(e);
			}
			request.getSession().setAttribute(ATTR_CURRENT_REQUIREMENT, requirement);
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_CUSTOMER_VIEW_TR);
		} else {
			request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.NULL_REMOVE_ITEM));
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_CUSTOMER_VIEW_TR);
		}
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
