package by.seleznev.devteam.command.customer;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.entity.customer.RequirementItem;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.CustomerService;
import by.seleznev.devteam.validator.Validator;

public class CreateItemCommand implements ActionCommand {

	private static final String PARAM_QUALIFICATION = "required_qualification";
	private static final String PARAM_DEV_NUMBER = "item_dev_number";
	private static final String PARAM_DESCRIPTION = "item_description";
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_CURRENT_REQUIREMENT = "current_requirement";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		String qualification = request.getParameter(PARAM_QUALIFICATION);
		String devNumber = request.getParameter(PARAM_DEV_NUMBER);
		String description = request.getParameter(PARAM_DESCRIPTION);
		if (Validator.isValidItemParameters(qualification, devNumber, description)) {
			try {
				CustomerService customerService = (CustomerService) FactoryService.getInstance()
						.getService(ServiceType.CUSTOMER);
				RequirementItem item = customerService.createItem(qualification, devNumber, description);
				TechnicalRequirement requirement = (TechnicalRequirement) request.getSession()
						.getAttribute(ATTR_CURRENT_REQUIREMENT);
				requirement.getItems().add(item);
				request.getSession().setAttribute(ATTR_CURRENT_REQUIREMENT, requirement);
				return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_CUSTOMER_CREATE_TR);
			} catch (DevTeamServiceException e) {
				throw new DevTeamApplicationException(e);
			}
		} else {
			request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.CHECK_ALL_REQUIRED_FIELDS));
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_CUSTOMER_CREATE_TR);
		}
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}
}
