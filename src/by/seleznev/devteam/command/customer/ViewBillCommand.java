package by.seleznev.devteam.command.customer;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.entity.manager.BillItem;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.CustomerService;
import by.seleznev.devteam.service.impl.ManagerService;
import by.seleznev.devteam.validator.Validator;

public class ViewBillCommand implements ActionCommand {

	private static final String ATTR_EXISTING_REQUIREMENT = "existing_requirement";
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_BILL = "bill";
	private static final String ATTR_TOTAL_PRICE = "total_price";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		if (request.getSession().getAttribute(ATTR_LOGGED_VISITOR) == null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		}
		TechnicalRequirement requirement = (TechnicalRequirement) request.getSession()
				.getAttribute(ATTR_EXISTING_REQUIREMENT);
		if (Validator.isPossibleViewBill(requirement)) {
			List<BillItem> bill = null;
			double price = 0;
			try {
				CustomerService customerService = (CustomerService) FactoryService.getInstance()
						.getService(ServiceType.CUSTOMER);
				ManagerService managerService = (ManagerService) FactoryService.getInstance()
						.getService(ServiceType.MANAGER);
				bill = customerService.defineBillInfo(requirement);
				price = managerService.calculateTotalPrice(bill);
			} catch (DevTeamServiceException e) {
				throw new DevTeamApplicationException(e);
			}
			request.getSession().setAttribute(ATTR_BILL, bill);
			request.getSession().setAttribute(ATTR_TOTAL_PRICE, price);
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_CUSTOMER_VIEW_BILL_DETAILS);
		} else {
			request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.ERROR_VIEW_BILL));
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_CUSTOMER_VIEW_TR_DETAILS);
		}
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
