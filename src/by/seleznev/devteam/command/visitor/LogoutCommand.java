package by.seleznev.devteam.command.visitor;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.manager.ConfigurationManager;

public class LogoutCommand implements ActionCommand {
	
	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		String page = ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
		request.getSession().invalidate();
		return page;
	}
	
	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}
}
