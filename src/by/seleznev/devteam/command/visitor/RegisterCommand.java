package by.seleznev.devteam.command.visitor;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.visitor.Visitor;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.impl.VisitorService;
import by.seleznev.devteam.validator.Validator;

public class RegisterCommand implements ActionCommand {

	private static final String PARAM_FIRST_NAME = "reg_first_name";
	private static final String PARAM_LAST_NAME = "reg_last_name";
	private static final String PARAM_COMPANY = "reg_company";
	private static final String PARAM_LOGIN = "reg_login";
	private static final String PARAM_PASSWORD = "reg_password";
	private static final String PARAM_EMAIL = "reg_email";
	private static final String PARAM_MOBILE = "reg_mobile";
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	private boolean isSuccess;

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		String firstName = request.getParameter(PARAM_FIRST_NAME);
		String lastName = request.getParameter(PARAM_LAST_NAME);
		String company = request.getParameter(PARAM_COMPANY);
		String login = request.getParameter(PARAM_LOGIN);
		String password = request.getParameter(PARAM_PASSWORD);
		String email = request.getParameter(PARAM_EMAIL);
		String mobile = request.getParameter(PARAM_MOBILE);
		if (Validator.isValidRegistrationParameters(firstName, lastName, company, login, password, email, mobile)) {
			VisitorService visitorService = new VisitorService();
			Visitor visitor = null;
			try {
				if (visitorService.addCustomer(firstName, lastName, company, login, password, email,
						mobile)) {
					isSuccess = true;
					visitor = visitorService.defineNewCustomer(login, password);
					request.getSession().setAttribute(ATTR_LOGGED_VISITOR, visitor);
					return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_SUCCESS_CUSTOMER);
				} else {
					isSuccess = false;
					request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.CHECK_ALL_REQUIRED_FIELDS));
					return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_LOGIN);
				}
			} catch (DevTeamServiceException e) {
				throw new DevTeamApplicationException(e);
			}
		} else {
			isSuccess = false;
			request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.CHECK_ALL_REQUIRED_FIELDS));
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_LOGIN);
		}
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return (isSuccess) ? RedirectionType.SEND_REDIRECT : RedirectionType.FORWARD;
	}
}
