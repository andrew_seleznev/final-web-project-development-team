package by.seleznev.devteam.command.visitor;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.manager.MessageManager;

public class ChangeLocaleCommand implements ActionCommand {
	
	private static final String LANGUAGE = "language";
	private static final String ATTR_CURRENT_PAGE = "currentPage";
	private static final String ATTR_LOCALE = "locale";

	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		HttpSession session = request.getSession();
		String language = request.getParameter(LANGUAGE);
		String currentPage = (String) session.getAttribute(ATTR_CURRENT_PAGE);
		Locale locale = new Locale(language);
		session.setAttribute(ATTR_LOCALE, locale);
		MessageManager.setCurrentLocale(language);
		return currentPage;
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}

}
