package by.seleznev.devteam.command.visitor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.entity.manager.Task;
import by.seleznev.devteam.entity.visitor.Visitor;
import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.enumeration.Role;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.service.impl.DeveloperService;
import by.seleznev.devteam.service.impl.VisitorService;
import by.seleznev.devteam.validator.Validator;

public class LoginCommand implements ActionCommand {
	private static final String PARAM_LOGIN = "login";
	private static final String PARAM_PASSWORD = "password";
	private static final String PARAM_GUEST = "guest";
	private static final String ATTR_MESSAGE = "message";
	private static final String ATTR_LOGGED_VISITOR = "loggedVisitor";
	private static final String ATTR_TASKS = "tasks";
	
	
	@Override
	public String execute(HttpServletRequest request) throws DevTeamApplicationException {
		String login = request.getParameter(PARAM_LOGIN);
		String pass = request.getParameter(PARAM_PASSWORD);
		if (request.getParameter(PARAM_GUEST) != null) {
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_GUEST);
		} else if (Validator.isValidLoginParameters(login, pass)) {
			VisitorService visitorService = new VisitorService();
			String role = null;
			Visitor visitor = null;
			try {
				role = visitorService.defineVisitorRole(login, pass);
				visitor = visitorService.defineVisitor(login, pass, role);
				request.getSession().setAttribute(ATTR_LOGGED_VISITOR, visitor);
				switch (Role.getRoleType(role)) {
				case ADMIN:
					return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_ADMIN);
				case CUSTOMER:
					return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_CUSTOMER);
				case DEVELOPER:
					DeveloperService developerService = new DeveloperService();
					List<Task> tasks = null;
					tasks = developerService.defineTasks(visitor);
					request.getSession().setAttribute(ATTR_TASKS, tasks);
					return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_DEVELOPER);
				case MANAGER:
					return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_MANAGER);
				default:
					return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_LOGIN);
				}
			} catch (DevTeamServiceException e) {
				throw new DevTeamApplicationException(e);
			}
		} else {
			request.setAttribute(ATTR_MESSAGE, MessageManager.getProperty(MessageManager.CHECK_ALL_REQUIRED_FIELDS));
			return ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_LOGIN);
		}
	}

	@Override
	public RedirectionType defineRedirectionType() {
		return RedirectionType.FORWARD;
	}
}
