package by.seleznev.devteam.command;

import javax.servlet.http.HttpServletRequest;

import by.seleznev.devteam.enumeration.RedirectionType;
import by.seleznev.devteam.exception.DevTeamApplicationException;

public interface ActionCommand {
	String execute(HttpServletRequest request) throws DevTeamApplicationException;

	RedirectionType defineRedirectionType();

}
