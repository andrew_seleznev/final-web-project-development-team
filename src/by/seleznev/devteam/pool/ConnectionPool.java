package by.seleznev.devteam.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.seleznev.devteam.exception.DevTeamConnectionPoolException;
import by.seleznev.devteam.manager.ConnectionManager;

/**
 * The {@code ConnectionPool} class represents an ability to connect with
 * database by means of {@code BlockingQueue} interface's possibility.
 */
public class ConnectionPool {

	static Logger logger = LogManager.getLogger(ConnectionPool.class);
	private BlockingQueue<ProxyConnection> connectionQueue;
	private AtomicBoolean isGettigConnection;
	private static AtomicBoolean instanceCreated = new AtomicBoolean(false);
	private static ReentrantLock lock = new ReentrantLock();
	private static ConnectionPool instance;
	/**
     * Simple constructor that performs the only initializing method.
     * @throws DevTeamConnectionPoolException
     */
	private ConnectionPool() throws DevTeamConnectionPoolException {
		initConnectionPool();
	}
	/**
     * Represents double-checked singleton pattern that gets the
     * only instance of {@code ConnectionPool}.
     * @return  single instance of {@code ConnectionPool}.
     */
	public static ConnectionPool getInstance() {
		if (!instanceCreated.get()) {
			lock.lock();
			try {
				if (!instanceCreated.get()) {
					instance = new ConnectionPool();
					instanceCreated.set(true);
				}
			} catch (DevTeamConnectionPoolException e) {
				logger.warn("Exception occurred during init connection pool", e);
			} finally {
				lock.unlock();
			}
		}
		return instance;
	}
	/**
     * Initializing method that sets up all starting database conditions:
     * login, password, pool size, encoding, etc. 
     * @throws DevTeamConnectionPoolException
     */
	private void initConnectionPool() throws DevTeamConnectionPoolException {
		try {
			isGettigConnection = new AtomicBoolean(true);
			int poolSize = Integer.valueOf(ConnectionManager.getProperty(ConnectionManager.DB_POOLSIZE));
			connectionQueue = new ArrayBlockingQueue<>(poolSize, true);
			Class.forName(ConnectionManager.getProperty(ConnectionManager.DB_DRIVER));
			Properties properties = new Properties();
			properties.setProperty("user", ConnectionManager.getProperty(ConnectionManager.DB_USER));
			properties.setProperty("password", ConnectionManager.getProperty(ConnectionManager.DB_PASSWORD));
			properties.setProperty("useUnicode", ConnectionManager.getProperty(ConnectionManager.DB_USE_UNICODE));
			properties.setProperty("characterEncoding", ConnectionManager.getProperty(ConnectionManager.DB_ENCODING));
			for (int i = 0; i <= poolSize; i++) {
				Connection connection = DriverManager
						.getConnection(ConnectionManager.getProperty(ConnectionManager.DB_URL), properties);
				ProxyConnection proxyConnection = new ProxyConnection(connection);
				connectionQueue.offer(proxyConnection);
			}
		} catch (SQLException | ClassNotFoundException e) {
			throw new DevTeamConnectionPoolException(e);
		}
	}
	/**
     * Gets an instance of {@code ProxyConnection} from the pool.
     * @return  an instance of {@code ProxyConnection}.
     * @throws DevTeamConnectionPoolException
     */
	public ProxyConnection getConnection() throws DevTeamConnectionPoolException {
		ProxyConnection connection = null;
		Long timeout = Long.valueOf(ConnectionManager.getProperty(ConnectionManager.DB_MAX_WAITING_TIME));
		if (isGettigConnection.get()) {
			try {
				connection = connectionQueue.poll(timeout, TimeUnit.MILLISECONDS);
			} catch (InterruptedException e) {
				throw new DevTeamConnectionPoolException("Exception occurred during getting connection", e);
			}
		}
		return connection;
	}
	/**
     * Gives back a non-null connection to the connection pool.
     * @param   connection is an instance of {@code ProxyConnection}.
     */
	public void returnConnection(ProxyConnection connection) {
		if (connection != null) {
			connectionQueue.offer(connection);
		}
	}
	 /**
     * Terminate the connection pool by closing all the connections in it.
     * Uses {@code sleep} method for compulsory delay before cleaning the pool.
     */
	public void terminateConnectionPool() throws DevTeamConnectionPoolException {
		isGettigConnection.set(false);
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			throw new DevTeamConnectionPoolException(e);
		}
		if (instance != null) {
			Iterator<ProxyConnection> iterator = connectionQueue.iterator();
			while (iterator.hasNext()) {
				ProxyConnection connection = iterator.next();
				try {
					connection.terminateConnection();
				} catch (SQLException e) {
					throw new DevTeamConnectionPoolException(e);
				}
				iterator.remove();
			}
		}
	}
}
