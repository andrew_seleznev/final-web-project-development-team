package by.seleznev.devteam.builder.manager;

import java.time.LocalDate;

import by.seleznev.devteam.entity.manager.Task;
import by.seleznev.devteam.enumeration.WorkStatus;
import by.seleznev.devteam.exception.DevTeamDAOException;

public class TaskBuilder {

	private Long taskId;
	private Long developerId;
	private String requirement;
	private WorkStatus status;
	private Integer time;
	private Integer projectTime;
	private LocalDate beginning;
	private LocalDate ending;

	private boolean isValid() {
		if (taskId == null || requirement == null || status == null || time == null || developerId == null
				|| projectTime == null) {
			return false;
		}
		return true;
	}

	public TaskBuilder taskId(Long taskId) {
		this.taskId = taskId;
		return this;
	}

	public TaskBuilder developerId(Long developerId) {
		this.developerId = developerId;
		return this;
	}

	public TaskBuilder requirement(String requirement) {
		this.requirement = requirement;
		return this;
	}

	public TaskBuilder status(WorkStatus status) {
		this.status = status;
		return this;
	}

	public TaskBuilder time(Integer time) {
		this.time = time;
		return this;
	}
	
	public TaskBuilder projectTime(Integer projectTime) {
		this.projectTime = projectTime;
		return this;
	}
	
	public TaskBuilder beginning(LocalDate beginning) {
		this.beginning = beginning;
		return this;
	}
	
	public TaskBuilder ending(LocalDate ending) {
		this.ending = ending;
		return this;
	}

	public Task build() throws DevTeamDAOException {
		if (isValid()) {
			return new Task(taskId, developerId, requirement, status, time, projectTime, beginning, ending);
		} else {
			throw new DevTeamDAOException("Illegal arguments for Task creation");
		}
	}
}
