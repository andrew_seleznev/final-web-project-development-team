package by.seleznev.devteam.builder.manager;

import java.time.LocalDate;

import by.seleznev.devteam.entity.manager.Project;
import by.seleznev.devteam.enumeration.WorkStatus;
import by.seleznev.devteam.exception.DevTeamDAOException;

public class ProjectBuilder {

	private Long projectId;
	private String title;
	private String description;
	private WorkStatus status;
	private Integer maxTime;
	private Double price;
	private LocalDate beginning;
	private LocalDate ending;

	private boolean isValid() {
		if (projectId == null || title == null || description == null || status == null || maxTime == null
				|| price == null) {
			return false;
		}
		return true;
	}

	public ProjectBuilder projectId(Long projectId) {
		this.projectId = projectId;
		return this;
	}

	public ProjectBuilder title(String title) {
		this.title = title;
		return this;
	}

	public ProjectBuilder description(String description) {
		this.description = description;
		return this;
	}

	public ProjectBuilder status(WorkStatus status) {
		this.status = status;
		return this;
	}

	public ProjectBuilder maxTime(Integer maxTime) {
		this.maxTime = maxTime;
		return this;
	}
	
	public ProjectBuilder price(Double price) {
		this.price = price;
		return this;
	}
	
	public ProjectBuilder beginning(LocalDate beginning) {
		this.beginning = beginning;
		return this;
	}
	
	public ProjectBuilder ending(LocalDate ending) {
		this.ending = ending;
		return this;
	}

	public Project build() throws DevTeamDAOException {
		if (isValid()) {
			return new Project(projectId, title, description, status, maxTime, price, beginning, ending);
		} else {
			throw new DevTeamDAOException("Illegal arguments for Project creation");
		}
	}
}
