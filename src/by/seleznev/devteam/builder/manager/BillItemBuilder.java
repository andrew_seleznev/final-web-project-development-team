package by.seleznev.devteam.builder.manager;

import by.seleznev.devteam.entity.manager.BillItem;
import by.seleznev.devteam.enumeration.Qualification;
import by.seleznev.devteam.exception.DevTeamDAOException;

public class BillItemBuilder {

	private Long requirementId;
	private Qualification qualification;
	private Integer time;
	private Integer rate;
	private String description;

	private boolean isValid() {
		if (requirementId == null || qualification == null || time == null || rate == null || description == null) {
			return false;
		}
		return true;
	}

	public BillItemBuilder requirementId(Long requirementId) {
		this.requirementId = requirementId;
		return this;
	}

	public BillItemBuilder qualification(Qualification qualification) {
		this.qualification = qualification;
		return this;
	}

	public BillItemBuilder time(Integer time) {
		this.time = time;
		return this;
	}

	public BillItemBuilder rate(Integer rate) {
		this.rate = rate;
		return this;
	}

	public BillItemBuilder description(String description) {
		this.description = description;
		return this;
	}

	public BillItem build() throws DevTeamDAOException {
		if (isValid()) {
			return new BillItem(requirementId, qualification, time, rate, description);
		} else {
			throw new DevTeamDAOException("Illegal arguments for Bill creation");
		}
	}
}
