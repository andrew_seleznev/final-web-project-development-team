package by.seleznev.devteam.builder.customer;

import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.enumeration.WorkStatus;
import by.seleznev.devteam.exception.DevTeamDAOException;

public class TechnicalRequirementBuilder {

	private Long requirementId;
	private String title;
	private String description;
	private Integer time;
	private WorkStatus status;
	private Double price;

	private boolean isValid() {
		if (requirementId == null || title == null || description == null || time == null || status == null
				|| price == null) {
			return false;
		}
		return true;
	}
	
	public TechnicalRequirementBuilder requirementId(Long requirementId) {
		this.requirementId = requirementId;
		return this;
	}

	public TechnicalRequirementBuilder title(String title) {
		this.title = title;
		return this;
	}

	public TechnicalRequirementBuilder description(String description) {
		this.description = description;
		return this;
	}

	public TechnicalRequirementBuilder time(Integer time) {
		this.time = time;
		return this;
	}

	public TechnicalRequirementBuilder status(WorkStatus status) {
		this.status = status;
		return this;
	}

	public TechnicalRequirementBuilder price(Double price) {
		this.price = price;
		return this;
	}

	public TechnicalRequirement build() throws DevTeamDAOException {
		if (isValid()) {
			return new TechnicalRequirement(requirementId, title, description, time, status, price);
		} else {
			throw new DevTeamDAOException("Illegal arguments for TechnicalRequirement creation");
		}
	}
}
