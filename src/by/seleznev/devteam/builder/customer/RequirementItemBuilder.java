package by.seleznev.devteam.builder.customer;

import by.seleznev.devteam.entity.customer.RequirementItem;
import by.seleznev.devteam.enumeration.Qualification;
import by.seleznev.devteam.exception.DevTeamDAOException;

public class RequirementItemBuilder {

	private Qualification qualification;
	private Integer requiredDevAmount;
	private Integer assignedDevAmount;
	private String itemDescription;

	private boolean isValid() {
		if (qualification == null || requiredDevAmount == null || assignedDevAmount == null
				|| itemDescription == null) {
			return false;
		}
		return true;
	}

	public RequirementItemBuilder qualification(Qualification qualification) {
		this.qualification = qualification;
		return this;
	}

	public RequirementItemBuilder requiredDevAmount(Integer requiredDevAmount) {
		this.requiredDevAmount = requiredDevAmount;
		return this;
	}

	public RequirementItemBuilder assignedDevAmount(Integer assignedDevAmount) {
		this.assignedDevAmount = assignedDevAmount;
		return this;
	}

	public RequirementItemBuilder itemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
		return this;
	}

	public RequirementItem build() throws DevTeamDAOException {
		if (isValid()) {
			return new RequirementItem(qualification, requiredDevAmount, assignedDevAmount, itemDescription);
		} else {
			throw new DevTeamDAOException("Illegal arguments for RequirementItem creation");
		}

	}

}
