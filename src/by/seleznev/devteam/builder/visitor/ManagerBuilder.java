package by.seleznev.devteam.builder.visitor;

import by.seleznev.devteam.entity.visitor.Manager;
import by.seleznev.devteam.enumeration.EmploymentType;
import by.seleznev.devteam.enumeration.Role;
import by.seleznev.devteam.exception.DevTeamDAOException;

public class ManagerBuilder {
	
	private Long visitorID;
	private Role role;
	private String login;
	private String firstName;
	private String lastName;
	private String email;
	private String mobile;
	private EmploymentType employmentType;

	private boolean isValid() {
		if (visitorID == null || role == null || login == null || firstName == null || lastName == null || email == null
				|| mobile == null || employmentType == null) {
			return false;
		}
		return true;
	}
	
	public ManagerBuilder visitorID(Long visitorID) {
		this.visitorID = visitorID;
		return this;
	}

	public ManagerBuilder role(Role role) {
		this.role = role;
		return this;
	}
	
	public ManagerBuilder login(String login) {
		this.login = login;
		return this;
	}
	
	public ManagerBuilder firstName(String firstName) {
		this.firstName = firstName;
		return this;
	}
	
	public ManagerBuilder lastName(String lastName) {
		this.lastName = lastName;
		return this;
	}
	
	public ManagerBuilder email(String email) {
		this.email = email;
		return this;
	}
	
	public ManagerBuilder mobile(String mobile) {
		this.mobile = mobile;
		return this;
	}
	
	public ManagerBuilder employmentType(EmploymentType employmentType) {
		this.employmentType = employmentType;
		return this;
	}
	
	public Manager build() throws DevTeamDAOException {
		if (isValid()) {
			return new Manager(visitorID, role, login, firstName, lastName, email, mobile, employmentType);
		} else {
			throw new DevTeamDAOException("Illegal arguments for admin creation");
		}
	}
	
}
