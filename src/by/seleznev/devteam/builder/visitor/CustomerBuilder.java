package by.seleznev.devteam.builder.visitor;

import by.seleznev.devteam.entity.visitor.Customer;
import by.seleznev.devteam.enumeration.Role;
import by.seleznev.devteam.exception.DevTeamDAOException;

public class CustomerBuilder {
	
	private Long visitorID;
	private Role role;
	private String login;
	private String firstName;
	private String lastName;
	private String email;
	private String mobile;
	private String company;
	
	private boolean isValid() {
		if (visitorID == null || role == null || login == null || firstName == null || lastName == null || email == null
				|| mobile == null || company == null) {
			return false;
		}
		return true;
	}
	
	public CustomerBuilder visitorID(Long visitorID) {
		this.visitorID = visitorID;
		return this;
	}

	public CustomerBuilder role(Role role) {
		this.role = role;
		return this;
	}
	
	public CustomerBuilder login(String login) {
		this.login = login;
		return this;
	}
	
	public CustomerBuilder firstName(String firstName) {
		this.firstName = firstName;
		return this;
	}
	
	public CustomerBuilder lastName(String lastName) {
		this.lastName = lastName;
		return this;
	}
	
	public CustomerBuilder email(String email) {
		this.email = email;
		return this;
	}
	
	public CustomerBuilder mobile(String mobile) {
		this.mobile = mobile;
		return this;
	}
	
	public CustomerBuilder company(String company) {
		this.company = company;
		return this;
	}
	
	public Customer build() throws DevTeamDAOException {
		if (isValid()) {
			return new Customer(visitorID, role, login, firstName, lastName, email, mobile, company);
		} else {
			throw new DevTeamDAOException("Illegal arguments for customer creation");
		}
	}
}
