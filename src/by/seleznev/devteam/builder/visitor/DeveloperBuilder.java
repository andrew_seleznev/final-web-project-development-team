package by.seleznev.devteam.builder.visitor;

import by.seleznev.devteam.entity.visitor.Developer;
import by.seleznev.devteam.enumeration.DevStatus;
import by.seleznev.devteam.enumeration.EmploymentType;
import by.seleznev.devteam.enumeration.Qualification;
import by.seleznev.devteam.enumeration.Role;
import by.seleznev.devteam.exception.DevTeamDAOException;

public class DeveloperBuilder {

	private Long visitorID;
	private Role role;
	private String login;
	private String firstName;
	private String lastName;
	private String email;
	private String mobile;
	private EmploymentType employmentType;
	private Qualification qualification;
	private DevStatus devStatus;

	private boolean isValid() {
		if (visitorID == null || role == null || login == null || firstName == null || lastName == null || email == null
				|| mobile == null || employmentType == null || qualification == null || devStatus == null) {
			return false;
		}
		return true;
	}

	public DeveloperBuilder visitorID(Long visitorID) {
		this.visitorID = visitorID;
		return this;
	}

	public DeveloperBuilder role(Role role) {
		this.role = role;
		return this;
	}

	public DeveloperBuilder login(String login) {
		this.login = login;
		return this;
	}

	public DeveloperBuilder firstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public DeveloperBuilder lastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public DeveloperBuilder email(String email) {
		this.email = email;
		return this;
	}

	public DeveloperBuilder mobile(String mobile) {
		this.mobile = mobile;
		return this;
	}

	public DeveloperBuilder employmentType(EmploymentType employmentType) {
		this.employmentType = employmentType;
		return this;
	}

	public DeveloperBuilder qualification(Qualification qualification) {
		this.qualification = qualification;
		return this;
	}

	public DeveloperBuilder devStatus(DevStatus devStatus) {
		this.devStatus = devStatus;
		return this;
	}

	public Developer build() throws DevTeamDAOException {
		if (isValid()) {
			return new Developer(visitorID, role, login, firstName, lastName, email, mobile, qualification,
					employmentType, devStatus);
		} else {
			throw new DevTeamDAOException("Illegal arguments for developer creation");
		}
	}
}
