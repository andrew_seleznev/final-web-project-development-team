package by.seleznev.devteam.validator;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.entity.manager.Project;
import by.seleznev.devteam.entity.manager.Task;
import by.seleznev.devteam.enumeration.WorkStatus;

public class Validator {

	private static final String REG_FIRST_NAME_PATTERN = "^[�-ߨA-Z][�-��-߸�a-zA-Z]{1,20}$";
	private static final String REG_LAST_NAME_PATTERN = "^[�-ߨA-Z][�-��-߸�a-zA-Z]{1,20}$";
	private static final String REG_COMPANY_PATTERN = "^[^<>].[^<>]{1,20}$";
	private static final String REG_LOGIN_PATTERN = "^[a-zA-Z][\\w-_\\.]{1,20}$";
	private static final String REG_PASSWORD_PATTERN = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).{4,20}$";
	private static final String REG_EMAIL_PATTERN = "^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*"
			+ "@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\\.)*"
			+ "(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$";
	private static final String REG_MOBILE_PATTERN = "^[\\d]{7,16}$";
	private static final String REG_TITLE_PATTERN = "^[�-��-߸�a-zA-Z0-9\\s-]{1,100}$";
	private static final String REG_TIME_PATTERN = "^[\\d]{1,10}$";
	private static final String REG_ITEM_DEV_NUMBER_PATTERN = "^[\\d]{1,10}$";
	private static final String REG_NUMBERS_PATTERN = "^[\\d]+$";

	public static boolean isValidId(String id) {
		if (id.isEmpty()) {
			return false;
		}
		Pattern pattern = Pattern.compile(REG_NUMBERS_PATTERN);
		Matcher matcher = pattern.matcher(id);
		return matcher.matches();
	}
	
	public static boolean isValidTaskCreationData(String developerId, String requirement) {
		if (requirement.isEmpty()) {
			return false;
		}
		return isValidId(developerId);
	}

	public static boolean isValidProjectCreationData(String projectTitle, String projectDescription) {
		if (projectTitle.isEmpty() || projectDescription.isEmpty()) {
			return false;
		}
		Pattern pattern = Pattern.compile(REG_TITLE_PATTERN);
		Matcher matcher = pattern.matcher(projectTitle);
		return matcher.matches();
	}

	public static boolean isPossibleProjectCreation(TechnicalRequirement technicalRequirement) {
		return (technicalRequirement.getStatus().equals(WorkStatus.NEW)) ? true : false;
	}

	public static boolean isPossibleSetTaskTime(String projectTime, String taskTime, String taskStatus) {
		if (taskTime.isEmpty()) {
			return false;
		} else if (Integer.valueOf(projectTime) < Integer.valueOf(taskTime)) {
			return false;
		} else if (!WorkStatus.getRoleType(taskStatus).equals(WorkStatus.NEW)) {
			return false;
		}
		return true;
	}

	public static boolean isPossibleViewBill(TechnicalRequirement requirement) {
		return (requirement.getStatus().equals(WorkStatus.WAITING_PAYMENT)) ? true : false;
	}

	public static boolean isPossibleCompleteProject(Project project) {
		List<Task> tasks = project.getTasks();
		Iterator<Task> iterator = tasks.iterator();
		if (!project.getStatus().equals(WorkStatus.ACTIVE)) {
			return false;
		}
		while (iterator.hasNext()) {
			Task task = iterator.next();
			if (!task.getStatus().equals(WorkStatus.COMPLETED)) {
				return false;
			}
		}
		return true;
	}

	public static boolean isPossibleCompleteTask(String taskStatus) {
		return (WorkStatus.getRoleType(taskStatus).equals(WorkStatus.ACTIVE)) ? true : false;
	}
	
	public static boolean isReadyToDeleteRequirement(List<TechnicalRequirement> requirements, String requirementId) {
		Long id = Long.valueOf(requirementId);
		boolean isComplete = false;
		Iterator<TechnicalRequirement> iterator = requirements.iterator();
		while (iterator.hasNext()) {
			TechnicalRequirement requirement = iterator.next();
			if (id.equals(requirement.getRequirementId())) {
				if (requirement.getStatus().equals(WorkStatus.COMPLETED) || requirement.getStatus().equals(WorkStatus.CANCELED)) {
					isComplete = true;
				}
			}
		}
		return isComplete;
	}

	public static boolean isValidRegistrationParameters(String firstName, String lastName, String company, String login,
			String password, String email, String mobile) {
		if (firstName.isEmpty() || lastName.isEmpty() || company.isEmpty() || login.isEmpty() || password.isEmpty()
				|| email.isEmpty() || mobile.isEmpty()) {
			return false;
		} else if (isValidFirstName(firstName) && isValidLastName(lastName) && isValidCompanyName(company)
				&& isValidLogin(login) && isValidPassword(password) && isValidEmail(email) && isValidMobile(mobile)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isValidLoginParameters(String login, String pass) {
		if (login.isEmpty() || pass.isEmpty()) {
			return false;
		} else if (isValidLogin(login) && isValidPassword(pass)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isVilidTechnicalRequirementParameters(String title, String time, String description) {
		if (title.isEmpty() || time.isEmpty() || description.isEmpty()) {
			return false;
		} else if (isValidTrTitle(title) && isValidTrTime(time)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isValidItemParameters(String qualification, String devNumber, String description) {
		if (qualification.isEmpty() || devNumber.isEmpty() || description.isEmpty()) {
			return false;
		} else if (isValidItemDevNumber(devNumber)) {
			return true;
		} else {
			return false;
		}
	}
	
	private static boolean isValidFirstName(String firstName) {
		Pattern pattern = Pattern.compile(REG_FIRST_NAME_PATTERN);
		Matcher matcher = pattern.matcher(firstName);
		return matcher.matches();
	}

	private static boolean isValidLastName(String lastName) {
		Pattern pattern = Pattern.compile(REG_LAST_NAME_PATTERN);
		Matcher matcher = pattern.matcher(lastName);
		return matcher.matches();
	}

	private static boolean isValidCompanyName(String company) {
		Pattern pattern = Pattern.compile(REG_COMPANY_PATTERN);
		Matcher matcher = pattern.matcher(company);
		return matcher.matches();
	}

	private static boolean isValidLogin(String login) {
		Pattern pattern = Pattern.compile(REG_LOGIN_PATTERN);
		Matcher matcher = pattern.matcher(login);
		return matcher.matches();
	}

	private static boolean isValidPassword(String password) {
		Pattern pattern = Pattern.compile(REG_PASSWORD_PATTERN);
		Matcher matcher = pattern.matcher(password);
		return matcher.matches();
	}

	private static boolean isValidEmail(String email) {
		Pattern pattern = Pattern.compile(REG_EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	private static boolean isValidMobile(String mobile) {
		Pattern pattern = Pattern.compile(REG_MOBILE_PATTERN);
		Matcher matcher = pattern.matcher(mobile);
		return matcher.matches();
	}
	
	private static boolean isValidTrTitle(String title) {
		Pattern pattern = Pattern.compile(REG_TITLE_PATTERN);
		Matcher matcher = pattern.matcher(title);
		return matcher.matches();
	}

	private static boolean isValidTrTime(String time) {
		Pattern pattern = Pattern.compile(REG_TIME_PATTERN);
		Matcher matcher = pattern.matcher(time);
		return matcher.matches();
	}
	
	private static boolean isValidItemDevNumber(String devNumber) {
		Pattern pattern = Pattern.compile(REG_ITEM_DEV_NUMBER_PATTERN);
		Matcher matcher = pattern.matcher(devNumber);
		return matcher.matches();
	}
}
