package by.seleznev.devteam.test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import by.seleznev.devteam.builder.customer.RequirementItemBuilder;
import by.seleznev.devteam.entity.customer.RequirementItem;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.exception.DevTeamDAOException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.service.factory.FactoryService;
import by.seleznev.devteam.service.factory.FactoryService.ServiceType;
import by.seleznev.devteam.service.impl.CustomerService;

public class TechnicalRequirementBuilderTest {

	private static CustomerService service;

	@BeforeClass
	public static void initCustomerService() throws DevTeamServiceException {
		service = (CustomerService) FactoryService.getInstance().getService(ServiceType.CUSTOMER);
	}

	@Test
	public void buildTechnicalRequirementTest() throws DevTeamServiceException {
		TechnicalRequirement requirement = service.createRequirement("some title", "some description", "10");
		Assert.assertNotNull(requirement);
	}

	@Test(expected = DevTeamDAOException.class)
	public void buildRequirementItemTest() throws NumberFormatException, DevTeamDAOException {
		RequirementItem item = new RequirementItemBuilder().requiredDevAmount(Integer.valueOf("10"))
				.assignedDevAmount(0).itemDescription("some description").build();
		Assert.assertNotNull(item);
	}
}
