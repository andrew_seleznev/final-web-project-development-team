package by.seleznev.devteam.test;

import org.junit.Assert;
import org.junit.Test;

import by.seleznev.devteam.encrypt.PasswordEncryption;
import by.seleznev.devteam.exception.DevTeamEncryptException;

public class PasswordEncryptionTest {
	
	@Test
	public void doPasswordEncryption() throws DevTeamEncryptException {
		String expected = "FYA+zkhpodwrfDZYqgy4SA==";
		String actual = PasswordEncryption.encrypt("szNV10");
		Assert.assertEquals("Password encryption has been failed", expected, actual);
	}
	
}
