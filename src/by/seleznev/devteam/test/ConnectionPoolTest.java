package by.seleznev.devteam.test;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import by.seleznev.devteam.exception.DevTeamConnectionPoolException;
import by.seleznev.devteam.pool.ConnectionPool;
import by.seleznev.devteam.pool.ProxyConnection;

public class ConnectionPoolTest {

	private static ConnectionPool connectionPool;

	@BeforeClass
	public static void initConnectionPool() {
		connectionPool = ConnectionPool.getInstance();
	}

	@Test
	public void getConnectionTest() throws DevTeamConnectionPoolException {
		for (int i = 0; i < 10000; i++) {
			ProxyConnection connection = connectionPool.getConnection();
			Assert.assertNotNull(connection);
			connectionPool.returnConnection(connection);
		}
	}

	@AfterClass
	public static void cleanUpConnectionPool() throws DevTeamConnectionPoolException {
		connectionPool.terminateConnectionPool();
	}

}
