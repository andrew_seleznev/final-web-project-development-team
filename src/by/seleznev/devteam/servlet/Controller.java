package by.seleznev.devteam.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.seleznev.devteam.command.ActionCommand;
import by.seleznev.devteam.command.factory.ActionFactory;
import by.seleznev.devteam.exception.DevTeamApplicationException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;

/**
 * The {@code Controller}} class represents single servlet used as
 * controller. It involves standard servlet's life-cycle methods
 * such as {@code init()}, {@code doGet()}, {@code doPost()},
 * {@code destroy()}.
 */
@WebServlet("/controller")
public class Controller extends HttpServlet {
	
	static Logger logger = LogManager.getLogger(Controller.class);
	private static final long serialVersionUID = 1L;
	private static final String ATTR_CURRENT_PAGE = "currentPage";
	private static final String ATTR_ERROR_MESSAGE = "errorMessage";
	
	/**
     * Carries out http-servlet's request in the case of {@code get} request.
     *
     * @param request  is the http-servlet's request.
     * @param response is the http-sevlet's response.
     * @throws ServletException    if servlet error occurs.
     * @throws java.io.IOException if input or output error occurs.
     */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	/**
     * Carries out http-servlet's request in the case of {@code post} request.
     *
     * @param request  is the http-servlet's request.
     * @param response is the http-sevlet's response.
     * @throws ServletException    if servlet error occurs.
     * @throws java.io.IOException if input or output error occurs.
     */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	/**
     * Performs an action which follows after user's request.
     *
     * @param request  is the http-servlet's request.
     * @param response is the http-servlet's response.
     * @throws ServletException    if servlet error occurs.
     * @throws java.io.IOException if input or output error occurs.
     */
	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String page = null;
		ActionFactory client = new ActionFactory();
		ActionCommand command = null;
		try { 
			command = client.defineCommand(request);
			page = command.execute(request);
			request.getSession(true).setAttribute(ATTR_CURRENT_PAGE, page);
		} catch (DevTeamApplicationException e) {
			logger.error(e.getMessage());
			request.getSession().setAttribute(ATTR_ERROR_MESSAGE, e.getMessage());
			page = ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_ERROR);
		}
		if (page != null) {
			switch (command.defineRedirectionType()) {
			case FORWARD:
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
				dispatcher.forward(request, response);
				break;
			case SEND_REDIRECT:
				response.sendRedirect(request.getContextPath() + page);
				break;
			default:
				RequestDispatcher disp = getServletContext().getRequestDispatcher(page);
				disp.forward(request, response);
				break;
			}
		} else {
			page = ConfigurationManager.getProperty(ConfigurationManager.PATH_PAGE_INDEX);
			request.getSession().setAttribute("nullPage", MessageManager.getProperty("message.nullpage"));
			response.sendRedirect(request.getContextPath() + page);
		}
	}
}