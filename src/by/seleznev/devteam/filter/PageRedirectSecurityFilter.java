package by.seleznev.devteam.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.seleznev.devteam.manager.ConfigurationManager;

/**
 * The class {@code PageRedirectSecurityFilter} is an implementation of
 * {@code Filter} that realizes all its necessary methods that
 * forms a life-cycle of a filter. Gives an ability to redirect
 * the call to the main page with direct reference to jsp/view.
 */
@WebFilter(urlPatterns = { "/jsp/view/*" })
public class PageRedirectSecurityFilter implements Filter {
	
	private String indexPath;
	private static final String PATH_INDEX_PAGE = "path.page.index";
	
	/**
     * Filter's main method that processes the {@code request} and also
     * works with {@code response}. Besides this, calls {@code doFilter()}
     * method that starts other filters and servlet.
     * @param   request the instance of {@code ServletRequest}.
     * @param   response the instance of {@code ServletResponse}.
     * @param   chain the instance of {@code FilterChain}.
     * @throws  java.io.IOException if any input/output errors occurs.
     * @throws  ServletException if any servlet errors occurs.
     */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
	    HttpServletResponse httpResponse = (HttpServletResponse) response;
	    httpResponse.sendRedirect(httpRequest.getContextPath() + indexPath);
		chain.doFilter(request, response);
	}
	
	/**
     * Filter starting method. Starts filter with every servlet's
     * call.
     * @param   fConfig the basic data of for filter.
     * @throws  ServletException if any servlet errors occurs.
     */
	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		indexPath = ConfigurationManager.getProperty(PATH_INDEX_PAGE);
	}
	
	/**
     * Filter ending method. Calls when filter stops its existence.
     */
	@Override
	public void destroy() {
		
	}

}
