package by.seleznev.devteam.listener;

import java.util.Locale;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.seleznev.devteam.exception.DevTeamConnectionPoolException;
import by.seleznev.devteam.manager.ConfigurationManager;
import by.seleznev.devteam.manager.MessageManager;
import by.seleznev.devteam.pool.ConnectionPool;

/**
 * The listener interface for receiving project events.
 * The class that is interested in processing a project
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addProjectListener<code> method. When
 * the project event occurs, that object's appropriate
 * method is invoked.
 *
 * @see ProjectEvent
 */
@WebListener
public class ProjectListener implements HttpSessionListener, ServletContextListener, ServletRequestListener {
	
	/**
	 * The logger.
	 */
	static Logger logger = LogManager.getLogger(ProjectListener.class);
	
	/**
	 * The Constant ATTR_LOCALE.
	 */
	public static final String ATTR_LOCALE = "locale";
	
	/**
	 * The Constant ATTR_CURRENT_PAGE.
	 */
	private static final String ATTR_CURRENT_PAGE = "currentPage";
	
	/**
	 * The Constant INIT_PAGE.
	 */
	private static final String INIT_PAGE = "path.page.login";
	
    /**
     * Instantiates a new project listener.
     */
    public ProjectListener() {

    }

    public void sessionCreated(HttpSessionEvent ev)  { 
    	ev.getSession().setAttribute(ATTR_LOCALE, Locale.getDefault());
    	ev.getSession().setAttribute(ATTR_CURRENT_PAGE, ConfigurationManager.getProperty(INIT_PAGE));
    	MessageManager.setCurrentLocale(Locale.getDefault().getLanguage());
    }

    public void requestDestroyed(ServletRequestEvent ev)  { 
    
    }

    public void requestInitialized(ServletRequestEvent ev)  { 
    
    }

    public void sessionDestroyed(HttpSessionEvent ev)  {
    	ev.getSession().getServletContext().setAttribute("sessionDestroyed", true);
    }   
    
    public void contextDestroyed(ServletContextEvent ev)  { 
    	try {
			ConnectionPool.getInstance().terminateConnectionPool();
		} catch (DevTeamConnectionPoolException e) {
			logger.fatal("Fatal exception occurred during terminate connection pool", e);
			throw new RuntimeException("Fatal exception occurred during terminate connection pool", e);
		}
    }

    public void contextInitialized(ServletContextEvent ev)  {
    	int attemptsCounter = 3;
	    ConnectionPool pool = null;
	    do {
	    	pool = ConnectionPool.getInstance();
	    	if (pool != null) {
	    		logger.info("Connection pool has been initialized successful");
	    	}
	    	attemptsCounter--;
	    } while (pool == null && attemptsCounter != 0);
	    
	    if (pool == null) {
	    	logger.fatal("Fatal exception occurred during init connection pool");
	    	throw new RuntimeException("Fatal exception occurred during init connection pool");
	    }
    }
	
}
