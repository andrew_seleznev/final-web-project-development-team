package by.seleznev.devteam.service.impl;

import java.util.List;

import by.seleznev.devteam.dao.factory.FactoryDAO;
import by.seleznev.devteam.dao.factory.FactoryDAO.DAOType;
import by.seleznev.devteam.dao.impl.DeveloperDAO;
import by.seleznev.devteam.entity.manager.Task;
import by.seleznev.devteam.entity.visitor.Visitor;
import by.seleznev.devteam.exception.DevTeamDAOException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.service.BaseService;

public class DeveloperService implements BaseService {

	public List<Task> defineTasks(Visitor visitor) throws DevTeamServiceException {
		List<Task> tasks = null;
		try {
			DeveloperDAO dao = (DeveloperDAO) FactoryDAO.getInstance().getDAO(DAOType.DEVELOPER);
			tasks = dao.findTasks(visitor.getVisitorID());
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return tasks;
	}

	public boolean acceptTask(String taskId, String taskTime) throws DevTeamServiceException {
		boolean isSuccess = false;
		try {
			DeveloperDAO dao = (DeveloperDAO) FactoryDAO.getInstance().getDAO(DAOType.DEVELOPER);
			isSuccess = dao.updateTaskStatus(Integer.valueOf(taskTime),
					Long.valueOf(taskId));
		} catch (NumberFormatException | DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return isSuccess;
	}

	public boolean finishTask(String taskId) throws DevTeamServiceException {
		boolean isSuccess = false;
		try {
			DeveloperDAO dao = (DeveloperDAO) FactoryDAO.getInstance().getDAO(DAOType.DEVELOPER);
			isSuccess = dao.completeTask(Long.valueOf(taskId));
		} catch (NumberFormatException | DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return isSuccess;
	}
}
