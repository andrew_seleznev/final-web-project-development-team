package by.seleznev.devteam.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.seleznev.devteam.builder.customer.RequirementItemBuilder;
import by.seleznev.devteam.builder.customer.TechnicalRequirementBuilder;
import by.seleznev.devteam.dao.factory.FactoryDAO;
import by.seleznev.devteam.dao.factory.FactoryDAO.DAOType;
import by.seleznev.devteam.dao.impl.CustomerDAO;
import by.seleznev.devteam.entity.customer.RequirementItem;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.entity.manager.BillItem;
import by.seleznev.devteam.entity.visitor.Visitor;
import by.seleznev.devteam.enumeration.Qualification;
import by.seleznev.devteam.enumeration.WorkStatus;
import by.seleznev.devteam.exception.DevTeamDAOException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.service.BaseService;

public class CustomerService implements BaseService {

	static Logger logger = LogManager.getLogger(CustomerService.class);

	public ArrayList<String> defineAllQualifications() throws DevTeamServiceException {
		ArrayList<String> qualifications = null;
		try {
			CustomerDAO dao = (CustomerDAO) FactoryDAO.getInstance().getDAO(DAOType.CUSTOMER);
			qualifications = dao.findAllQualifications();
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return qualifications;
	}

	public TechnicalRequirement removeItem(TechnicalRequirement requirement, String item)
			throws DevTeamServiceException {
		int index = 0;
		try {
			index = Integer.valueOf(item);
		} catch (NumberFormatException e) {
			throw new DevTeamServiceException(e);
		}
		requirement.getItems().remove(index - 1);
		return requirement;
	}

	public ArrayList<TechnicalRequirement> defineAllTechnicalRequirements(Long visitorId)
			throws DevTeamServiceException {
		ArrayList<TechnicalRequirement> requirements = null;
		try {
			CustomerDAO dao = (CustomerDAO) FactoryDAO.getInstance().getDAO(DAOType.CUSTOMER);
			requirements = dao.findAllTr(visitorId);
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return requirements;
	}

	public TechnicalRequirement defineRequirement(Long visitorId, String trId) throws DevTeamServiceException {
		TechnicalRequirement requirement = null;
		ArrayList<RequirementItem> items = null;
		Long requirementId = Long.valueOf(trId);
		try {
			CustomerDAO dao = (CustomerDAO) FactoryDAO.getInstance().getDAO(DAOType.CUSTOMER);
			requirement = dao.findRequirement(requirementId);
			items = dao.findAllTrItems(visitorId, requirementId);
			requirement.getItems().addAll(items);
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return requirement;
	}

	public TechnicalRequirement createRequirement(String title, String description, String time)
			throws DevTeamServiceException {
		TechnicalRequirement requirement = null;
		try {
			requirement = new TechnicalRequirementBuilder().requirementId(0L).title(title).time(Integer.valueOf(time))
					.description(description).status(WorkStatus.NEW).price(0.0).build();
		} catch (NumberFormatException | DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return requirement;
	}

	public RequirementItem createItem(String qualification, String devNumber, String description)
			throws DevTeamServiceException {
		RequirementItem item = null;
		try {
			item = new RequirementItemBuilder().qualification(Qualification.getQualificationType(qualification))
					.requiredDevAmount(Integer.valueOf(devNumber)).assignedDevAmount(0).itemDescription(description)
					.build();
		} catch (NumberFormatException | DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}

		return item;
	}

	public boolean addRequirement(Visitor visitor, TechnicalRequirement requirement) throws DevTeamServiceException {
		Long customerId = visitor.getVisitorID();
		boolean isSuccess = false;
		try {
			CustomerDAO dao = (CustomerDAO) FactoryDAO.getInstance().getDAO(DAOType.CUSTOMER);
			Integer workStatusId = dao.findWorkStatusIdByName(requirement.getStatus().getTypeName());
			isSuccess = dao.addRequirement(customerId, workStatusId, requirement);
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return isSuccess;
	}

	public List<BillItem> defineBillInfo(TechnicalRequirement requirement) throws DevTeamServiceException {
		List<BillItem> bill = null;
		try {
			CustomerDAO dao = (CustomerDAO) FactoryDAO.getInstance().getDAO(DAOType.CUSTOMER);
			bill = dao.findBillInfo(requirement.getRequirementId());
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return bill;
	}

	public boolean payBill(TechnicalRequirement requirement, Double price) throws DevTeamServiceException {
		boolean isSuccess = false;
		LocalDate beginning = LocalDate.now();
		LocalDate ending = beginning.plusDays(requirement.getTime());
		try {
			CustomerDAO dao = (CustomerDAO) FactoryDAO.getInstance().getDAO(DAOType.CUSTOMER);
			isSuccess = dao.makePayment(requirement.getRequirementId(), price, beginning, ending);
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return isSuccess;
	}

}
