package by.seleznev.devteam.service.impl;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;

import by.seleznev.devteam.builder.manager.ProjectBuilder;
import by.seleznev.devteam.builder.manager.TaskBuilder;
import by.seleznev.devteam.dao.factory.FactoryDAO;
import by.seleznev.devteam.dao.factory.FactoryDAO.DAOType;
import by.seleznev.devteam.dao.impl.ManagerDAO;
import by.seleznev.devteam.entity.customer.RequirementItem;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.entity.manager.BillItem;
import by.seleznev.devteam.entity.manager.Project;
import by.seleznev.devteam.entity.manager.Task;
import by.seleznev.devteam.entity.visitor.Developer;
import by.seleznev.devteam.entity.visitor.Visitor;
import by.seleznev.devteam.enumeration.WorkStatus;
import by.seleznev.devteam.exception.DevTeamDAOException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.manager.DevTeamConfiguration;
import by.seleznev.devteam.service.BaseService;

public class ManagerService implements BaseService {

	public List<TechnicalRequirement> defineAllRequirements() throws DevTeamServiceException {
		List<TechnicalRequirement> requirements = null;
		try {
			ManagerDAO dao = (ManagerDAO) FactoryDAO.getInstance().getDAO(DAOType.MANAGER);
			requirements = dao.findAllRequirements();
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return requirements;
	}

	public TechnicalRequirement defineRequirementById(String requirementId) throws DevTeamServiceException {
		Long id = Long.valueOf(requirementId);
		TechnicalRequirement requirement = null;
		List<RequirementItem> items = null;
		try {
			ManagerDAO dao = (ManagerDAO) FactoryDAO.getInstance().getDAO(DAOType.MANAGER);
			requirement = dao.findRequirementById(id);
			items = dao.findRequirementDescription(id);
			requirement.getItems().addAll(items);
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return requirement;
	}

	public Project createProject(TechnicalRequirement requirement, String projectTitle, String projectDescription)
			throws DevTeamServiceException {
		Project project = null;
		try {
			project = new ProjectBuilder().projectId(requirement.getRequirementId()).title(projectTitle)
					.description(projectDescription).maxTime(requirement.getTime()).status(WorkStatus.NEW)
					.beginning(LocalDate.now()).ending(LocalDate.now()).price(0.0).build();
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return project;
	}

	public List<Developer> defineDevelopers(TechnicalRequirement requirement, Integer index)
			throws DevTeamServiceException {
		List<Developer> developers = null;
		RequirementItem item = requirement.getItems().get(index - 1);
		try {
			ManagerDAO dao = (ManagerDAO) FactoryDAO.getInstance().getDAO(DAOType.MANAGER);
			developers = dao.findDevelopers(item.getQualification());
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return developers;
	}

	public Task createTask(Developer developer, String requirement, Project project) throws DevTeamServiceException {
		Task task = null;
		try {
			if (isFreeDeveloper(developer, project)) {
				task = new TaskBuilder().taskId(0L).developerId(developer.getVisitorID()).requirement(requirement)
						.status(WorkStatus.NEW).time(0).projectTime(project.getMaxTime()).beginning(LocalDate.now())
						.ending(LocalDate.now()).build();
			}
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException("Impossible create new task");
		}
		return task;
	}

	public Developer defineAssignedDeveloper(List<Developer> developers, String developerId)
			throws DevTeamServiceException {
		Long id = Long.valueOf(developerId);
		Iterator<Developer> iterator = developers.iterator();
		while (iterator.hasNext()) {
			Developer developer = iterator.next();
			if (id.equals(developer.getVisitorID())) {
				return developer;
			}
		}
		throw new DevTeamServiceException("Impossible define selected developer");
	}

	private boolean isFreeDeveloper(Developer developer, Project project) {
		List<Task> tasks = project.getTasks();
		if (!tasks.isEmpty()) {
			Iterator<Task> iterator = tasks.iterator();
			while (iterator.hasNext()) {
				Task task = iterator.next();
				if (developer.getVisitorID().equals(task.getDeveloperId())) {
					return false;
				}
			}
		}
		return true;
	}

	public void addAssignedDeveloper(TechnicalRequirement technicalRequirement, Integer index)
			throws DevTeamServiceException {
		RequirementItem item = technicalRequirement.getItems().get(index - 1);
		if (item.getRequiredDevAmount() > item.getAssignedDevAmount()) {
			item.increaseDeveloperCounter();
		} else {
			throw new DevTeamServiceException("Impossible assign more developers than required");
		}
	}

	public boolean addProject(Visitor visitor, TechnicalRequirement technicalRequirement, Project project)
			throws DevTeamServiceException {
		boolean isAddProject = false;
		try {
			ManagerDAO dao = (ManagerDAO) FactoryDAO.getInstance().getDAO(DAOType.MANAGER);
			isAddProject = dao.addProject(visitor, technicalRequirement, project);
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return isAddProject;
	}

	public List<Project> defineProjects(Visitor visitor) throws DevTeamServiceException {
		List<Project> projects = null;
		try {
			ManagerDAO dao = (ManagerDAO) FactoryDAO.getInstance().getDAO(DAOType.MANAGER);
			projects = dao.findAllProjects(visitor);
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return projects;
	}

	public Project defineProjectById(String projectId) throws DevTeamServiceException {
		Long id = Long.valueOf(projectId);
		Project project = null;
		try {
			ManagerDAO dao = (ManagerDAO) FactoryDAO.getInstance().getDAO(DAOType.MANAGER);
			project = dao.findProjectById(id);
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return project;
	}

	public boolean isPossibleCreateBill(Project project) {
		List<Task> tasks = project.getTasks();
		Iterator<Task> iterator = tasks.iterator();
		if (!project.getStatus().equals(WorkStatus.NEW)) {
			return false;
		}
		while (iterator.hasNext()) {
			Task task = iterator.next();
			if (!task.getStatus().equals(WorkStatus.ACCEPTED)) {
				return false;
			}
		}
		return true;
	}

	public List<BillItem> defineBillInfo(Project project) throws DevTeamServiceException {
		List<BillItem> bill = null;
		try {
			ManagerDAO dao = (ManagerDAO) FactoryDAO.getInstance().getDAO(DAOType.MANAGER);
			bill = dao.findBillInfo(project.getProjectId());
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return bill;
	}

	public double calculateTotalPrice(List<BillItem> bill) throws DevTeamServiceException {
		double vat = 0;
		double price = 0;
		try {
			vat = Double.valueOf(DevTeamConfiguration.getProperty(DevTeamConfiguration.BILL_VAT));
			Iterator<BillItem> iterator = bill.iterator();
			while (iterator.hasNext()) {
				BillItem item = iterator.next();
				price += item.getRate() * item.getTime();
			}
			price += price * vat / 100;
		} catch (NumberFormatException e) {
			throw new DevTeamServiceException(e);
		}
		return price;
	}

	public boolean addBill(Project project, List<BillItem> bill, Double price) throws DevTeamServiceException {
		boolean isSuccess = false;
		try {
			ManagerDAO dao = (ManagerDAO) FactoryDAO.getInstance().getDAO(DAOType.MANAGER);
			isSuccess = dao.addBill(project.getProjectId(), bill, price);
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return isSuccess;
	}

	public boolean finishProject(Project project) throws DevTeamServiceException {
		boolean isSuccess = false;
		try {
			ManagerDAO dao = (ManagerDAO) FactoryDAO.getInstance().getDAO(DAOType.MANAGER);
			isSuccess = dao.completeProject(project.getProjectId());
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return isSuccess;
	}
	
	public boolean cancelRequirement(TechnicalRequirement requirement) throws DevTeamServiceException {
		boolean isSuccess = false;
		try {
			ManagerDAO dao = (ManagerDAO) FactoryDAO.getInstance().getDAO(DAOType.MANAGER);
			isSuccess = dao.refuseRequirement(requirement);
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return isSuccess;
	}

}
