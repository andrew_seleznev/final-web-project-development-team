package by.seleznev.devteam.service.impl;

import java.util.List;

import by.seleznev.devteam.dao.factory.FactoryDAO;
import by.seleznev.devteam.dao.factory.FactoryDAO.DAOType;
import by.seleznev.devteam.dao.impl.AdminDAO;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.exception.DevTeamDAOException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.service.BaseService;

public class AdminService implements BaseService {
	
	public List<TechnicalRequirement> defineAllTr() throws DevTeamServiceException {
		List<TechnicalRequirement> requirements = null;
		try {
			AdminDAO dao = (AdminDAO) FactoryDAO.getInstance().getDAO(DAOType.ADMIN);
			requirements = dao.findAllTr();
		} catch (DevTeamDAOException e) {
			throw new DevTeamServiceException(e);
		}
		return requirements;
	}
	
	public boolean removeCompleteRequirement(String requirementId) throws DevTeamServiceException {
		boolean isSuccess = false;
		try {
			AdminDAO dao = (AdminDAO) FactoryDAO.getInstance().getDAO(DAOType.ADMIN);
			isSuccess = dao.deleteCompleteRequirement(Long.valueOf(requirementId));
		} catch (DevTeamDAOException | NumberFormatException e) {
			throw new DevTeamServiceException(e);
		}
		return isSuccess;
	}

}
