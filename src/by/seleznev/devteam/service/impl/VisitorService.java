package by.seleznev.devteam.service.impl;

import by.seleznev.devteam.dao.factory.FactoryDAO;
import by.seleznev.devteam.dao.factory.FactoryDAO.DAOType;
import by.seleznev.devteam.dao.impl.VisitorDAO;
import by.seleznev.devteam.encrypt.PasswordEncryption;
import by.seleznev.devteam.entity.visitor.Visitor;
import by.seleznev.devteam.enumeration.Role;
import by.seleznev.devteam.exception.DevTeamDAOException;
import by.seleznev.devteam.exception.DevTeamEncryptException;
import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.service.BaseService;

public class VisitorService implements BaseService {

	public String defineVisitorRole(String login, String password) throws DevTeamServiceException {
		String role = null;
		try {
			VisitorDAO dao = (VisitorDAO) FactoryDAO.getInstance().getDAO(DAOType.VISITOR);
			role = dao.findVisitorRole(login, PasswordEncryption.encrypt(password));
		} catch (DevTeamDAOException | DevTeamEncryptException e) {
			throw new DevTeamServiceException(e);
		}
		return role;
	}

	public Visitor defineNewCustomer(String login, String password) throws DevTeamServiceException {
		Visitor visitor = null;
		try {
			VisitorDAO dao = (VisitorDAO) FactoryDAO.getInstance().getDAO(DAOType.VISITOR);
			visitor = dao.findCustomer(login, PasswordEncryption.encrypt(password));
		} catch (DevTeamDAOException | DevTeamEncryptException e) {
			throw new DevTeamServiceException(e);
		}
		return visitor;
	}

	public Visitor defineVisitor(String login, String password, String role) throws DevTeamServiceException {
		Visitor visitor = null;
		String encryptPassword = null;
		try {
			VisitorDAO dao = (VisitorDAO) FactoryDAO.getInstance().getDAO(DAOType.VISITOR);
			encryptPassword = PasswordEncryption.encrypt(password);
			switch (Role.getRoleType(role)) {
			case ADMIN:
				visitor = dao.findAdmin(login, encryptPassword);
				break;
			case CUSTOMER:
				visitor = dao.findCustomer(login, encryptPassword);
				break;
			case MANAGER:
				visitor = dao.findManager(login, encryptPassword);
				break;
			case DEVELOPER:
				visitor = dao.findDeveloper(login, encryptPassword);
				break;
			default:
				throw new DevTeamServiceException("Impossible define visitor with required role");
			}
		} catch (DevTeamDAOException | DevTeamEncryptException e) {
			throw new DevTeamServiceException(e);
		}
		return visitor;
	}

	public boolean addCustomer(String firstName, String lastName, String company, String login, String password,
			String email, String mobile) throws DevTeamServiceException {
		boolean isSuccess = false;
		try {
			VisitorDAO dao = (VisitorDAO) FactoryDAO.getInstance().getDAO(DAOType.VISITOR);
			isSuccess = dao.addCustomer(firstName, lastName, company, login, PasswordEncryption.encrypt(password),
					email, Integer.valueOf(mobile));
		} catch (DevTeamDAOException | NumberFormatException | DevTeamEncryptException e) {
			throw new DevTeamServiceException(e);
		}
		return isSuccess;
	}

}
