package by.seleznev.devteam.service.factory;

import by.seleznev.devteam.exception.DevTeamServiceException;
import by.seleznev.devteam.service.BaseService;
import by.seleznev.devteam.service.impl.AdminService;
import by.seleznev.devteam.service.impl.CustomerService;
import by.seleznev.devteam.service.impl.DeveloperService;
import by.seleznev.devteam.service.impl.ManagerService;
import by.seleznev.devteam.service.impl.VisitorService;

/**
 * The Class FactoryService.
 */
public class FactoryService {
	
	/**
	 * The Enum ServiceType.
	 */
	public enum ServiceType {
		
	VISITOR, CUSTOMER, MANAGER, DEVELOPER, ADMIN
	
	}
	
	/**
	 * Instantiates a new factory service.
	 */
	private FactoryService() {
	}
	
	/**
	 * The Class SingletonHolder.
	 */
	private static class SingletonHolder {
		
		/** The Constant INSTANCE. */
		private final static FactoryService INSTANCE = new FactoryService();
	}

	/**
	 * Gets the single instance of FactoryService.
	 *
	 * @return single instance of FactoryService
	 */
	public static FactoryService getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	/**
	 * Gets the service.
	 *
	 * @param type
	 * @return the service
	 * @throws DevTeamServiceException
	 */
	public BaseService getService(ServiceType type) throws DevTeamServiceException {
		switch (type) {
		case VISITOR:
			return new VisitorService();
		case CUSTOMER:
			return new CustomerService();
		case MANAGER:
			return new ManagerService();
		case DEVELOPER:
			return new DeveloperService();
		case ADMIN:
			return new AdminService();
		default:
			throw new DevTeamServiceException("Unsupported parameter for service instance creation");
		}
	}
	
}
