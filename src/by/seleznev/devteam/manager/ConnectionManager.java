package by.seleznev.devteam.manager;

import java.util.ResourceBundle;

public class ConnectionManager {
	
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.database");
	public static final String DB_POOLSIZE = "db.poolsize";
	public static final String DB_URL = "db.url";
	public static final String DB_DRIVER = "db.driver";
	public static final String DB_USER = "db.user";
	public static final String DB_PASSWORD = "db.password";
	public static final String DB_MAX_WAITING_TIME = "db.max.waiting.time";
	public static final String DB_USE_UNICODE = "db.useUnicode";
	public static final String DB_ENCODING = "db.encoding";
	
	private ConnectionManager() {
	}
	
	public static String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}
