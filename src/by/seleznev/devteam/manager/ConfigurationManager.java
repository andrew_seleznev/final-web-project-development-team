package by.seleznev.devteam.manager;

import java.util.ResourceBundle;

public class ConfigurationManager {

	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.config");
	public static final String PATH_PAGE_INDEX = "path.page.index";
	public static final String PATH_PAGE_LOGIN = "path.page.login";
	public static final String PATH_PAGE_MAIN = "path.page.main";
	public static final String PATH_PAGE_ERROR = "path.page.error";
	public static final String PATH_PAGE_GUEST = "path.page.guest";
	public static final String PATH_PAGE_ADMIN = "path.page.admin";
	public static final String PATH_PAGE_ADMIN_VIEW_ALL_REQUIREMENTS = "path.page.admin_view_all_requirements";
	public static final String PATH_PAGE_CUSTOMER = "path.page.customer";
	public static final String PATH_PAGE_SUCCESS_MANAGER = "path.page.success_manager";
	public static final String PATH_PAGE_SUCCESS_DEVELOPER = "path.page.success_developer";
	public static final String PATH_PAGE_SUCCESS_CUSTOMER = "path.page.success_customer";
	public static final String PATH_PAGE_SUCCESS_ADMIN = "path.page.success_admin";
	public static final String PATH_PAGE_CUSTOMER_CREATE_TR = "path.page.customer_create_tr";
	public static final String PATH_PAGE_CUSTOMER_VIEW_TR = "path.page.customer_view_tr";
	public static final String PATH_PAGE_CUSTOMER_VIEW_ALL_TR = "path.page.customer_view_all_tr";
	public static final String PATH_PAGE_CUSTOMER_VIEW_TR_DETAILS =  "path.page.customer_view_tr_details";
	public static final String PATH_PAGE_CUSTOMER_VIEW_BILL_DETAILS = "path.page.customer_view_bill_details";
	public static final String PATH_PAGE_DEVELOPER = "path.page.developer";
	public static final String PATH_PAGE_MANAGER = "path.page.manager";
	public static final String PATH_PAGE_MANAGER_VIEW_ALL_REQUIREMENTS = "path.page.manager_view_all_requirements";
	public static final String PATH_PAGE_MANAGER_VIEW_ALL_PROJECTS = "path.page.manager_view_all_projects";
	public static final String PATH_PAGE_MANAGER_VIEW_REQUIREMENT_DETAILS = "path.page.manager_requirement_details";
	public static final String PATH_PAGE_MANAGER_CREATE_PROJECT = "path.page.manager_create_project";
	public static final String PATH_PAGE_MANAGER_ASSIGN_DEVELOPERS = "path.page.manager_assign_developers";
	public static final String PATH_PAGE_MANAGER_VIEW_DEVELOPERS = "path.page.manager_view_developers";
	public static final String PATH_PAGE_MANAGER_VIEW_PROJECT_DETAILS = "path.page.manager_view_project_details";
	public static final String PATH_PAGE_MANAGER_VIEW_BILL_DETAILS = "path.page.manager_view_bill_details";

	private ConfigurationManager() {
	}

	public static String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}
