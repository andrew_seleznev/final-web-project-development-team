package by.seleznev.devteam.manager;

import java.util.ResourceBundle;

public class DevTeamConfiguration {
	
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.devteam");
	public static final String BILL_VAT = "bill.vat";
	
	private DevTeamConfiguration() {
	}
	
	public static String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}
