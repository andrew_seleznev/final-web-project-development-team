package by.seleznev.devteam.manager;

import java.util.Locale;
import java.util.ResourceBundle;

public class MessageManager {

	private static final String BUNDLE_NAME = "properties.text";
	private static Locale currentLocale = Locale.getDefault();
	private static ResourceBundle resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME, currentLocale);
	public static final String ERROR_SQL_MESSAGE = "messages.error_sql_message";
	public static final String ERROR_CONNECTON = "messages.error_connection";
	public static final String ERROR_ROLE_NAME = "messages.error_role_name";
	public static final String ERROR_TR_TITLE = "customer.error_tr_title_format";
	public static final String ERROR_TR_TIME = "customer.error_tr_time_format";
	public static final String ERROR_ITEM_DEV_NUMBER = "customer.error_item_dev_number_format";
	public static final String NULL_USER_DATA = "messages.null_user_data";
	public static final String NULL_REMOVE_ITEM = "messages.null_remove_item";
	public static final String NULL_SHOW_TR = "messages.null_show_tr";
	public static final String ERROR_ADD_VISITOR = "messages.error_add_new_visitor";

	public static final String ERROR_DURING_ADD_PROJECT = "messages.error_during_add_project";
	public static final String ERROR_DURING_ADD_TECHNICAL_REQUIREMENT = "messages.error_during_add_technical_requirement";
	public static final String ERROR_DURING_CANCEL_REQUIREMENT = "messages.error_during_cancel_requirement";
	public static final String ERROR_IMPOSSIBLE_PROJECT_CREATION = "messages.error_impossible_project_creation";
	public static final String ERROR_VIEW_PROJECTS = "messages.error_view_projects";
	public static final String ERROR_PROJECT_NOT_SELECTED = "messages.error_project_not_selected";
	public static final String ERROR_CREATE_BILL = "messages.error_create_bill";
	public static final String ERROR_VIEW_BILL = "messages.error_view_bill";
	public static final String ERROR_CONFIRM_BILL = "messages.error_confirm_bill";
	public static final String ERROR_TASK_TIME = "messages.error_task_time";
	public static final String ERROR_MAKE_PAYMENT = "messages.error_do_payment";
	public static final String ERROR_COMPLETE_PROJECT = "messages.error_complete_project";
	public static final String ERROR_COMPLETE_TASK = "messages.error_complete_task";
	public static final String ERROR_REMOVE_COMPLETE_REQUIREMENT = "messages.error_remove_complete_requirement";
	public static final String CHECK_ALL_REQUIRED_FIELDS = "messages.check_all_required_fields";

	private MessageManager() {
	}

	public static String getProperty(String key) {

		return resourceBundle.getString(key);
	}

	public static void setCurrentLocale(String language) {
		Locale locale = new Locale(language);
		currentLocale = locale;
		resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME, currentLocale);
	}
}
