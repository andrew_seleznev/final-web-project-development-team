package by.seleznev.devteam.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import by.seleznev.devteam.entity.visitor.Visitor;
import by.seleznev.devteam.enumeration.Role;

public class VisitorInfoTag extends TagSupport {

	private static final long serialVersionUID = 1L;

	private Visitor visitor;

	public void setVisitor(Visitor visitor) {
		this.visitor = visitor;
	}

	@Override
	public int doStartTag() throws JspException {
		if (visitor != null) {
			try {
				String to = null;
				switch (Role.getRoleType(visitor.getRole().getTypeName())) {
				case ADMIN:
					to = "<img src=\"images/admin-icon.png\" width=\"20\" height=\"20\"> ";
					break;
				case CUSTOMER:
					to = "<img src=\"images/customer-icon.png\" width=\"20\" height=\"20\"> ";
					break;
				case MANAGER:
					to = "<img src=\"images/manager-icon.png\" width=\"20\" height=\"20\"> ";
					break;
				case DEVELOPER:
					to = "<img src=\"images/developer-icon.png\" width=\"20\" height=\"20\"> ";
					break;
				default:
					to = "<img src=\"images/customer-icon.png\" width=\"20\" height=\"20\"> ";
					break;
				}
				pageContext.getOut().write(to + "<b>(" + visitor.getRole().getTypeName() + ")</b> "
						+ visitor.getFirstName() + " " + visitor.getLastName());
			} catch (IOException e) {
				throw new JspException(e.getMessage());
			}
		}
		return SKIP_BODY;
	}
}
