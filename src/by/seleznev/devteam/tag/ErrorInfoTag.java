package by.seleznev.devteam.tag;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class ErrorInfoTag extends TagSupport {

	private static final long serialVersionUID = 1L;

	private int rows;
	private LinkedList<Object> list;

	public void setRows(int rows) {
		this.rows = rows;
	}

	public void setList(LinkedList<Object> list) {
		this.list = list;
	}

	@Override
	public int doStartTag() throws JspException {
		JspWriter out = pageContext.getOut();
		try {
			out.write("<table><tbody><tr><th>");
			out.write(String.valueOf(list.poll()));
			out.write("</th><td>");
		} catch (IOException e) {
			throw new JspException(e);
		}
		return EVAL_BODY_INCLUDE;
	}

	@Override
	public int doAfterBody() throws JspException {
		if (rows-- > 1) {
			try {
				pageContext.getOut().write("</td></tr><tr><th>");
				pageContext.getOut().write(String.valueOf(list.poll()));
				pageContext.getOut().write("</th><td>");
			} catch (IOException e) {
				throw new JspTagException(e.getMessage());
			}
			return EVAL_BODY_AGAIN;
		} else {
			return SKIP_BODY;
		}
	}

	@Override
	public int doEndTag() throws JspException {
		try {
			pageContext.getOut().write("</td></tr></tbody></table>");
		} catch (IOException e) {
			throw new JspTagException(e.getMessage());
		}
		return EVAL_PAGE;
	}
}
