package by.seleznev.devteam.entity.manager;

import by.seleznev.devteam.entity.BaseEntity;
import by.seleznev.devteam.enumeration.Qualification;

/**
 * The Class BillItem.
 */
public class BillItem extends BaseEntity {
	
	/** The requirement id. */
	private Long requirementId;
	
	/** The qualification. */
	private Qualification qualification;
	
	/** The time. */
	private Integer time;
	
	/** The rate. */
	private Integer rate;
	
	/** The description. */
	private String description;
	
	/**
	 * Instantiates a new bill item.
	 *
	 * @param requirementId the requirement id
	 * @param qualification the qualification
	 * @param time the required time
	 * @param rate the developer rate
	 * @param description the developer's task description
	 */
	public BillItem(Long requirementId, Qualification qualification, Integer time, Integer rate, String description) {
		this.requirementId = requirementId;
		this.qualification = qualification;
		this.time = time;
		this.rate = rate;
		this.description = description;
	}

	/**
	 * Gets the requirement id.
	 *
	 * @return the requirement id
	 */
	public Long getRequirementId() {
		return requirementId;
	}

	/**
	 * Gets the qualification.
	 *
	 * @return the qualification
	 */
	public Qualification getQualification() {
		return qualification;
	}

	/**
	 * Gets the time.
	 *
	 * @return the time
	 */
	public Integer getTime() {
		return time;
	}

	/**
	 * Gets the rate.
	 *
	 * @return the rate
	 */
	public Integer getRate() {
		return rate;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((qualification == null) ? 0 : qualification.hashCode());
		result = prime * result + ((rate == null) ? 0 : rate.hashCode());
		result = prime * result + ((requirementId == null) ? 0 : requirementId.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BillItem other = (BillItem) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (qualification != other.qualification)
			return false;
		if (rate == null) {
			if (other.rate != null)
				return false;
		} else if (!rate.equals(other.rate))
			return false;
		if (requirementId == null) {
			if (other.requirementId != null)
				return false;
		} else if (!requirementId.equals(other.requirementId))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Bill [requirementId=" + requirementId + ", qualification=" + qualification + ", time=" + time
				+ ", rate=" + rate + ", description=" + description + "]";
	}
	
}
