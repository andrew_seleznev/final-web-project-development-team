package by.seleznev.devteam.entity.manager;

import java.time.LocalDate;

import by.seleznev.devteam.entity.BaseEntity;
import by.seleznev.devteam.enumeration.WorkStatus;

/**
 * The Class Task.
 */
public class Task extends BaseEntity {
	
	/** The task id. */
	private Long taskId;
	
	/** The developer id. */
	private Long developerId;
	
	/** The requirement description. */
	private String requirement;
	
	/** The task status. */
	private WorkStatus status;
	
	/** The task time. */
	private Integer time;
	
	/** The project time. */
	private Integer projectTime;
	
	/** The task beginning date. */
	private LocalDate beginning;
	
	/** The task ending date. */
	private LocalDate ending;
	
	/**
	 * Instantiates a new task.
	 *
	 * @param taskId the task id
	 * @param developerId the developer id
	 * @param requirement the requirement description
	 * @param status the task status
	 * @param time the task time
	 * @param projectTime the project time
	 * @param beginning the task beginning date
	 * @param ending the task ending date
	 */
	public Task(Long taskId, Long developerId, String requirement, WorkStatus status, Integer time, Integer projectTime,
			LocalDate beginning, LocalDate ending) {
		this.taskId = taskId;
		this.developerId = developerId;
		this.requirement = requirement;
		this.status = status;
		this.time = time;
		this.projectTime = projectTime;
		this.beginning = beginning;
		this.ending = ending;
	}

	/**
	 * Gets the task id.
	 *
	 * @return the task id
	 */
	public Long getTaskId() {
		return taskId;
	}

	/**
	 * Gets the developer id.
	 *
	 * @return the developer id
	 */
	public Long getDeveloperId() {
		return developerId;
	}

	/**
	 * Gets the requirement.
	 *
	 * @return the requirement
	 */
	public String getRequirement() {
		return requirement;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public WorkStatus getStatus() {
		return status;
	}

	/**
	 * Gets the time.
	 *
	 * @return the time
	 */
	public Integer getTime() {
		return time;
	}

	/**
	 * Gets the project time.
	 *
	 * @return the project time
	 */
	public Integer getProjectTime() {
		return projectTime;
	}

	/**
	 * Gets the beginning.
	 *
	 * @return the beginning
	 */
	public LocalDate getBeginning() {
		return beginning;
	}

	/**
	 * Gets the ending.
	 *
	 * @return the ending
	 */
	public LocalDate getEnding() {
		return ending;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((beginning == null) ? 0 : beginning.hashCode());
		result = prime * result + ((developerId == null) ? 0 : developerId.hashCode());
		result = prime * result + ((ending == null) ? 0 : ending.hashCode());
		result = prime * result + ((projectTime == null) ? 0 : projectTime.hashCode());
		result = prime * result + ((requirement == null) ? 0 : requirement.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((taskId == null) ? 0 : taskId.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Task other = (Task) obj;
		if (beginning == null) {
			if (other.beginning != null)
				return false;
		} else if (!beginning.equals(other.beginning))
			return false;
		if (developerId == null) {
			if (other.developerId != null)
				return false;
		} else if (!developerId.equals(other.developerId))
			return false;
		if (ending == null) {
			if (other.ending != null)
				return false;
		} else if (!ending.equals(other.ending))
			return false;
		if (projectTime == null) {
			if (other.projectTime != null)
				return false;
		} else if (!projectTime.equals(other.projectTime))
			return false;
		if (requirement == null) {
			if (other.requirement != null)
				return false;
		} else if (!requirement.equals(other.requirement))
			return false;
		if (status != other.status)
			return false;
		if (taskId == null) {
			if (other.taskId != null)
				return false;
		} else if (!taskId.equals(other.taskId))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Task [taskId=" + taskId + ", developerId=" + developerId + ", requirement=" + requirement + ", status="
				+ status + ", time=" + time + ", projectTime=" + projectTime + ", beginning=" + beginning + ", ending="
				+ ending + "]";
	}
	
}
