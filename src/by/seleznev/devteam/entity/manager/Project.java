package by.seleznev.devteam.entity.manager;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import by.seleznev.devteam.entity.BaseEntity;
import by.seleznev.devteam.enumeration.WorkStatus;

/**
 * The Class Project.
 */
public class Project extends BaseEntity {

	/** The project id. */
	private Long projectId;
	
	/** The project title. */
	private String title;
	
	/** The project description. */
	private String description;
	
	/** The project status. */
	private WorkStatus status;
	
	/** The max project time. */
	private Integer maxTime;
	
	/** The project price. */
	private Double price;
	
	/** The project beginning date. */
	private LocalDate beginning;
	
	/** The project ending date. */
	private LocalDate ending;
	
	/** The tasks. */
	private List<Task> tasks;
	
	/**
	 * Instantiates a new project.
	 *
	 * @param projectId the project id
	 * @param title the project title
	 * @param description the project description
	 * @param status the project status
	 * @param maxTime the max project time
	 * @param price the project price
	 * @param beginning the project beginning date
	 * @param ending the project ending date
	 */
	public Project(Long projectId, String title, String description, WorkStatus status, Integer maxTime, Double price,
			LocalDate beginning, LocalDate ending) {
		this.projectId = projectId;
		this.title = title;
		this.description = description;
		this.status = status;
		this.maxTime = maxTime;
		this.price = price;
		this.beginning = beginning;
		this.ending = ending;
		tasks = new ArrayList<>();
	}

	/**
	 * Gets the project id.
	 *
	 * @return the project id
	 */
	public Long getProjectId() {
		return projectId;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public WorkStatus getStatus() {
		return status;
	}

	/**
	 * Gets the max time.
	 *
	 * @return the max time
	 */
	public Integer getMaxTime() {
		return maxTime;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * Gets the beginning.
	 *
	 * @return the beginning
	 */
	public LocalDate getBeginning() {
		return beginning;
	}

	/**
	 * Gets the ending.
	 *
	 * @return the ending
	 */
	public LocalDate getEnding() {
		return ending;
	}

	/**
	 * Gets the tasks.
	 *
	 * @return the tasks
	 */
	public List<Task> getTasks() {
		return tasks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((beginning == null) ? 0 : beginning.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((ending == null) ? 0 : ending.hashCode());
		result = prime * result + ((maxTime == null) ? 0 : maxTime.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((projectId == null) ? 0 : projectId.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((tasks == null) ? 0 : tasks.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (beginning == null) {
			if (other.beginning != null)
				return false;
		} else if (!beginning.equals(other.beginning))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (ending == null) {
			if (other.ending != null)
				return false;
		} else if (!ending.equals(other.ending))
			return false;
		if (maxTime == null) {
			if (other.maxTime != null)
				return false;
		} else if (!maxTime.equals(other.maxTime))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (projectId == null) {
			if (other.projectId != null)
				return false;
		} else if (!projectId.equals(other.projectId))
			return false;
		if (status != other.status)
			return false;
		if (tasks == null) {
			if (other.tasks != null)
				return false;
		} else if (!tasks.equals(other.tasks))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Project [projectId=" + projectId + ", title=" + title + ", description=" + description + ", status="
				+ status + ", maxTime=" + maxTime + ", price=" + price + ", beginning=" + beginning + ", ending="
				+ ending + ", tasks=" + tasks + "]";
	}

}
