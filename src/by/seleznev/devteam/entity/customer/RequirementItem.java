package by.seleznev.devteam.entity.customer;

import by.seleznev.devteam.entity.BaseEntity;
import by.seleznev.devteam.enumeration.Qualification;

/**
 * The Class RequirementItem.
 */
public class RequirementItem extends BaseEntity {

	/** The qualification. */
	private Qualification qualification;
	
	/** The required dev amount. */
	private Integer requiredDevAmount;
	
	/** The assigned dev amount. */
	private Integer assignedDevAmount;
	
	/** The item description. */
	private String itemDescription;
	
	/**
	 * Instantiates a new requirement item.
	 *
	 * @param qualification the qualification
	 * @param requiredDevAmount the required dev amount
	 * @param assignedDevAmount the assigned dev amount
	 * @param itemDescription the item description
	 */
	public RequirementItem(Qualification qualification, Integer requiredDevAmount, Integer assignedDevAmount,
			String itemDescription) {
		this.qualification = qualification;
		this.requiredDevAmount = requiredDevAmount;
		this.assignedDevAmount = assignedDevAmount;
		this.itemDescription = itemDescription;
	}

	/**
	 * Gets the qualification.
	 *
	 * @return the qualification
	 */
	public Qualification getQualification() {
		return qualification;
	}

	/**
	 * Gets the required dev amount.
	 *
	 * @return the required dev amount
	 */
	public Integer getRequiredDevAmount() {
		return requiredDevAmount;
	}

	/**
	 * Gets the assigned dev amount.
	 *
	 * @return the assigned dev amount
	 */
	public Integer getAssignedDevAmount() {
		return assignedDevAmount;
	}

	/**
	 * Gets the item description.
	 *
	 * @return the item description
	 */
	public String getItemDescription() {
		return itemDescription;
	}
	
	/**
	 * Increase developer counter.
	 */
	public void increaseDeveloperCounter() {
		assignedDevAmount++;
	}
	
	/**
	 * Reduce developer counter.
	 */
	public void reduceDeveloperCounter() {
		if (assignedDevAmount > 0) {
			assignedDevAmount--;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((assignedDevAmount == null) ? 0 : assignedDevAmount.hashCode());
		result = prime * result + ((itemDescription == null) ? 0 : itemDescription.hashCode());
		result = prime * result + ((qualification == null) ? 0 : qualification.hashCode());
		result = prime * result + ((requiredDevAmount == null) ? 0 : requiredDevAmount.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequirementItem other = (RequirementItem) obj;
		if (assignedDevAmount == null) {
			if (other.assignedDevAmount != null)
				return false;
		} else if (!assignedDevAmount.equals(other.assignedDevAmount))
			return false;
		if (itemDescription == null) {
			if (other.itemDescription != null)
				return false;
		} else if (!itemDescription.equals(other.itemDescription))
			return false;
		if (qualification != other.qualification)
			return false;
		if (requiredDevAmount == null) {
			if (other.requiredDevAmount != null)
				return false;
		} else if (!requiredDevAmount.equals(other.requiredDevAmount))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RequirementItem [qualification=" + qualification + ", requiredDevAmount=" + requiredDevAmount
				+ ", assignedDevAmount=" + assignedDevAmount + ", itemDescription=" + itemDescription + "]";
	}
	
}
