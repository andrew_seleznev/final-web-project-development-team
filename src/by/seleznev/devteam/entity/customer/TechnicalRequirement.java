package by.seleznev.devteam.entity.customer;

import java.util.ArrayList;
import java.util.List;

import by.seleznev.devteam.entity.BaseEntity;
import by.seleznev.devteam.enumeration.WorkStatus;

/**
 * The Class TechnicalRequirement.
 */
public class TechnicalRequirement extends BaseEntity {

	/** The requirement id. */
	private Long requirementId;
	
	/** The title. */
	private String title;
	
	/** The description. */
	private String description;
	
	/** The time. */
	private Integer time;
	
	/** The status. */
	private WorkStatus status;
	
	/** The price. */
	private Double price;
	
	/** The items. */
	private List<RequirementItem> items;

	/**
	 * Instantiates a new technical requirement.
	 *
	 * @param requirementId the requirement id
	 * @param title the title
	 * @param description the description
	 * @param time the time
	 * @param status the status
	 * @param price the price
	 */
	public TechnicalRequirement(Long requirementId, String title, String description, Integer time, WorkStatus status,
			Double price) {
		this.requirementId = requirementId;
		this.title = title;
		this.description = description;
		this.time = time;
		this.status = status;
		this.price = price;
		items = new ArrayList<>();
	}

	/**
	 * Gets the requirement id.
	 *
	 * @return the requirement id
	 */
	public Long getRequirementId() {
		return requirementId;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the time.
	 *
	 * @return the time
	 */
	public Integer getTime() {
		return time;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public WorkStatus getStatus() {
		return status;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * Gets the items.
	 *
	 * @return the items
	 */
	public List<RequirementItem> getItems() {
		return items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((requirementId == null) ? 0 : requirementId.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TechnicalRequirement other = (TechnicalRequirement) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (requirementId == null) {
			if (other.requirementId != null)
				return false;
		} else if (!requirementId.equals(other.requirementId))
			return false;
		if (status != other.status)
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TechnicalRequirement [requirementId=" + requirementId + ", title=" + title + ", description="
				+ description + ", time=" + time + ", status=" + status + ", price=" + price + ", items=" + items + "]";
	}

}
