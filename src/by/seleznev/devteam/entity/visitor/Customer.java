package by.seleznev.devteam.entity.visitor;

import by.seleznev.devteam.enumeration.Role;

/**
 * The Class Customer.
 */
public class Customer extends Visitor {
	
	/** The company. */
	private String company;

	/**
	 * Instantiates a new customer.
	 *
	 * @param visitorID the customer id
	 * @param role the customer role
	 * @param login the customer login
	 * @param firstName the customer first name
	 * @param lastName the customer last name
	 * @param email the customer email
	 * @param mobile the customer mobile phone number
	 * @param company the customer company name
	 */
	public Customer(Long visitorID, Role role, String login, String firstName, String lastName, String email,
			String mobile, String company) {
		super(visitorID, role, login, firstName, lastName, email, mobile);
		this.company = company;
	}

	/**
	 * Gets the company.
	 *
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * Sets the company.
	 *
	 * @param company the new company
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Customer [" + super.toString() + ", company=" + company + "]";
	}

}
