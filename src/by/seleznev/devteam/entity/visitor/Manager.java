package by.seleznev.devteam.entity.visitor;

import by.seleznev.devteam.enumeration.EmploymentType;
import by.seleznev.devteam.enumeration.Role;

/**
 * The Class Manager.
 */
public class Manager extends Visitor {
	
	/** The employment type. */
	private EmploymentType employmentType;

	/**
	 * Instantiates a new manager.
	 *
	 * @param visitorID the visitor id
	 * @param role the role
	 * @param login the login
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param email the email
	 * @param mobile the mobile
	 * @param employmentType the employment type
	 */
	public Manager(Long visitorID, Role role, String login, String firstName, String lastName, String email,
			String mobile, EmploymentType employmentType) {
		super(visitorID, role, login, firstName, lastName, email, mobile);
		this.employmentType = employmentType;
	}

	/**
	 * Gets the employment type.
	 *
	 * @return the employment type
	 */
	public EmploymentType getEmploymentType() {
		return employmentType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((employmentType == null) ? 0 : employmentType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Manager other = (Manager) obj;
		if (employmentType != other.employmentType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Manager [" + super.toString() + ", employmentType=" + employmentType + "]";
	}
	
}
