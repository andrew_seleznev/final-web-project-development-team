package by.seleznev.devteam.entity.visitor;

import by.seleznev.devteam.entity.BaseEntity;
import by.seleznev.devteam.enumeration.Role;

/**
 * The Class Visitor.
 */
public class Visitor extends BaseEntity {

	/** The visitor id. */
	private Long visitorID;
	
	/** The role. */
	private Role role;
	
	/** The login. */
	private String login;
	
	/** The first name. */
	private String firstName;
	
	/** The last name. */
	private String lastName;
	
	/** The email. */
	private String email;
	
	/** The mobile. */
	private String mobile;
	
	/**
	 * Instantiates a new visitor.
	 *
	 * @param visitorID the visitor id
	 * @param role the role
	 * @param login the login
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param email the email
	 * @param mobile the mobile
	 */
	public Visitor(Long visitorID, Role role, String login, String firstName, String lastName, String email,
			String mobile) {
		this.visitorID = visitorID;
		this.role = role;
		this.login = login;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.mobile = mobile;
	}

	/**
	 * Gets the visitor id.
	 *
	 * @return the visitor id
	 */
	public Long getVisitorID() {
		return visitorID;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * Gets the login.
	 *
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Gets the mobile.
	 *
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((mobile == null) ? 0 : mobile.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((visitorID == null) ? 0 : visitorID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Visitor other = (Visitor) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (mobile == null) {
			if (other.mobile != null)
				return false;
		} else if (!mobile.equals(other.mobile))
			return false;
		if (role != other.role)
			return false;
		if (visitorID == null) {
			if (other.visitorID != null)
				return false;
		} else if (!visitorID.equals(other.visitorID))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Visitor [visitorID=" + visitorID + ", role=" + role + ", login=" + login + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email=" + email + ", mobile=" + mobile + "]";
	}
	
}
