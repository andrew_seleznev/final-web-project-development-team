package by.seleznev.devteam.entity.visitor;

import by.seleznev.devteam.enumeration.EmploymentType;
import by.seleznev.devteam.enumeration.Role;

/**
 * The Class Admin.
 */
public class Admin extends Visitor {
	
	/** The employment type. */
	private EmploymentType employmentType;

	/**
	 * Instantiates a new admin.
	 *
	 * @param visitorID the admin id
	 * @param role the admin role
	 * @param login the admin login
	 * @param firstName the admin first name
	 * @param lastName the admin last name
	 * @param email the admin email
	 * @param mobile the admin mobile
	 * @param employmentType the admin employment type
	 */
	public Admin(Long visitorID, Role role, String login, String firstName, String lastName, String email,
			String mobile, EmploymentType employmentType) {
		super(visitorID, role, login, firstName, lastName, email, mobile);
		this.employmentType = employmentType;
	}

	/**
	 * Gets the employment type.
	 *
	 * @return the employment type
	 */
	public EmploymentType getEmploymentType() {
		return employmentType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((employmentType == null) ? 0 : employmentType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Admin other = (Admin) obj;
		if (employmentType != other.employmentType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Admin [" + super.toString() + ", employmentType=" + employmentType + "]";
	}
}
