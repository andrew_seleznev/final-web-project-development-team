package by.seleznev.devteam.entity.visitor;

import by.seleznev.devteam.enumeration.DevStatus;
import by.seleznev.devteam.enumeration.EmploymentType;
import by.seleznev.devteam.enumeration.Qualification;
import by.seleznev.devteam.enumeration.Role;

/**
 * The Class Developer.
 */
public class Developer extends Visitor {
	
	/** The qualification. */
	private Qualification qualification;
	
	/** The employment type. */
	private EmploymentType employmentType;
	
	/** The dev status. */
	private DevStatus devStatus;
	
	/**
	 * Instantiates a new developer.
	 *
	 * @param visitorID the visitor id
	 * @param role the role
	 * @param login the login
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param email the email
	 * @param mobile the mobile
	 * @param qualification the qualification
	 * @param employmentType the employment type
	 * @param devStatus the dev status
	 */
	public Developer(Long visitorID, Role role, String login, String firstName, String lastName, String email,
			String mobile, Qualification qualification, EmploymentType employmentType, DevStatus devStatus) {
		super(visitorID, role, login, firstName, lastName, email, mobile);
		this.qualification = qualification;
		this.employmentType = employmentType;
		this.devStatus = devStatus;
	}

	/**
	 * Gets the qualification.
	 *
	 * @return the qualification
	 */
	public Qualification getQualification() {
		return qualification;
	}

	/**
	 * Gets the employment type.
	 *
	 * @return the employment type
	 */
	public EmploymentType getEmploymentType() {
		return employmentType;
	}

	/**
	 * Gets the dev status.
	 *
	 * @return the dev status
	 */
	public DevStatus getDevStatus() {
		return devStatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((devStatus == null) ? 0 : devStatus.hashCode());
		result = prime * result + ((employmentType == null) ? 0 : employmentType.hashCode());
		result = prime * result + ((qualification == null) ? 0 : qualification.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Developer other = (Developer) obj;
		if (devStatus != other.devStatus)
			return false;
		if (employmentType != other.employmentType)
			return false;
		if (qualification != other.qualification)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Developer [qualification=" + qualification + ", employmentType=" + employmentType + ", devStatus="
				+ devStatus + "]";
	}
	
}
