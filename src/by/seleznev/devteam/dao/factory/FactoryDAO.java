package by.seleznev.devteam.dao.factory;

import by.seleznev.devteam.dao.BaseDAO;
import by.seleznev.devteam.dao.impl.AdminDAO;
import by.seleznev.devteam.dao.impl.CustomerDAO;
import by.seleznev.devteam.dao.impl.DeveloperDAO;
import by.seleznev.devteam.dao.impl.ManagerDAO;
import by.seleznev.devteam.dao.impl.VisitorDAO;
import by.seleznev.devteam.exception.DevTeamDAOException;

public class FactoryDAO {
	
	public enum DAOType {
		VISITOR, CUSTOMER, MANAGER, DEVELOPER, ADMIN
	}
	
	private FactoryDAO() {
	}
	
	private static class SingletonHolder {
		private final static FactoryDAO INSTANCE = new FactoryDAO();
	}

	public static FactoryDAO getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	public BaseDAO getDAO(DAOType type) throws DevTeamDAOException {
		switch (type) {
		case VISITOR:
			return new VisitorDAO();
		case CUSTOMER:
			return new CustomerDAO();
		case MANAGER:
			return new ManagerDAO();
		case DEVELOPER:
			return new DeveloperDAO();
		case ADMIN:
			return new AdminDAO();
		default:
			throw new DevTeamDAOException("Unsupported parameter for DAO instance creation");
		}
	}
}
