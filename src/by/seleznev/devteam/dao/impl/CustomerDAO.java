package by.seleznev.devteam.dao.impl;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import by.seleznev.devteam.builder.customer.RequirementItemBuilder;
import by.seleznev.devteam.builder.customer.TechnicalRequirementBuilder;
import by.seleznev.devteam.builder.manager.BillItemBuilder;
import by.seleznev.devteam.dao.BaseDAO;
import by.seleznev.devteam.entity.customer.RequirementItem;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.entity.manager.BillItem;
import by.seleznev.devteam.enumeration.Qualification;
import by.seleznev.devteam.enumeration.WorkStatus;
import by.seleznev.devteam.exception.DevTeamConnectionPoolException;
import by.seleznev.devteam.exception.DevTeamDAOException;
import by.seleznev.devteam.pool.ConnectionPool;
import by.seleznev.devteam.pool.ProxyConnection;

public class CustomerDAO implements BaseDAO {

	private static final String COL_QUALIFICATION_NAME = "qualification_name";
	private static final String COL_TR_ID = "requirement_id";
	private static final String COL_TR_STATUS = "status";
	private static final String COL_TR_TITLE = "title";
	private static final String COL_TR_DESCRIPTION = "requirement_description";
	private static final String COL_TR_TIME = "time";
	private static final String COL_TR_PRICE = "price";
	private static final String COL_ITEM_QUALIFICATION = "qualification_name";
	private static final String COL_ITEM_DEV_NUMBER = "dev_number";
	private static final String COL_ITEM_DESCRIPTION = "item_description";
	private static final String COL_WORK_STATUS_ID = "status_id";
	private static final String COL_QUALIFICATION_ID = "qualification_id";
	private static final String COL_BILL_QUALIFICATION_NAME = "qualification_name";
	private static final String COL_BILL_TASK_TIME = "time";
	private static final String COL_BILL_QUALIFICATION_RATE = "qualification_rate";
	private static final String COL_BILL_TASK_REQUIREMENT = "requirement";

	private static final String SQL_FIND_ALL_QUALIFICATIONS = "SELECT qualification.qualification_name FROM qualification";
	private static final String SQL_FIND_ALL_TR = "SELECT requirement.requirement_id, work_status.status, requirement.title, "
			+ "requirement.requirement_description, requirement.time, requirement.price FROM requirement "
			+ "JOIN work_status ON requirement.status_id = work_status.status_id "
			+ "WHERE requirement.customer_id = ?";
	private static final String SQL_FIND_TR_DESCRIPTION = "SELECT qualification.qualification_name, item.dev_number, "
			+ "item.item_description FROM customer "
			+ "JOIN requirement ON requirement.customer_id = customer.customer_id "
			+ "JOIN item ON requirement.requirement_id = item.requirement_id "
			+ "JOIN qualification ON item.qualification_id = qualification.qualification_id "
			+ "WHERE customer.customer_id = ? and requirement.requirement_id = ?";
	private static final String SQL_FIND_REQUIREMENT = "SELECT requirement.requirement_id, work_status.status, requirement.title, "
			+ "requirement.requirement_description, requirement.time, requirement.price FROM requirement "
			+ "JOIN work_status ON requirement.status_id = work_status.status_id "
			+ "WHERE requirement.requirement_id = ?";
	private static final String SQL_FIND_BILL_ITEMS = "SELECT qualification.qualification_name, task.time, "
			+ "qualification.qualification_rate, task.requirement FROM task "
			+ "JOIN developer ON developer.developer_id = task.developer_id "
			+ "JOIN qualification ON qualification.qualification_id = developer.qualification_id "
			+ "WHERE task.project_id = ?";
	private static final String SQL_FIND_WORK_STATUS = "SELECT work_status.status_id FROM work_status "
			+ "WHERE work_status.status = ?";
	private static final String SQL_FIND_QUALIFICATION = "SELECT qualification.qualification_id FROM qualification "
			+ "WHERE qualification.qualification_name = ?";
	private static final String SQL_ADD_REQUIREMENT = "INSERT INTO requirement (requirement.customer_id, requirement.status_id, "
			+ "requirement.title, requirement.requirement_description, requirement.time, requirement.price)  "
			+ "VALUES(?, ?, ?, ?, ?, ?)";
	private static final String SQL_ADD_ITEM = "INSERT INTO item (item.requirement_id, item.qualification_id, item.dev_number, "
			+ "item.item_description) VALUES((SELECT requirement.requirement_id FROM requirement "
			+ "WHERE requirement.title = ? AND requirement.requirement_description = ?), ?, ?, ?)";
	private static final String SQL_UPDATE_REQUIREMENT_STATUS = "UPDATE requirement "
			+ "SET requirement.status_id = (SELECT work_status.status_id FROM work_status WHERE work_status.status = ?), "
			+ "requirement.price = ? WHERE requirement.requirement_id = ?";
	private static final String SQL_UPDATE_PROJECT_STATUS = "UPDATE project "
			+ "SET project.status_id = (SELECT work_status.status_id FROM work_status WHERE work_status.status = ?), "
			+ "project.project_beginning = ?, project.project_ending = ? WHERE project.project_id = ?";
	private static final String SQL_UPDATE_TASK_STATUS = "UPDATE task "
			+ "SET task.status_id = (SELECT work_status.status_id FROM work_status WHERE work_status.status = ?), "
			+ "task.task_beginning = ?, task.task_ending = ? WHERE task.project_id = ?";

	public ArrayList<String> findAllQualifications() throws DevTeamDAOException {
		ArrayList<String> qualifications = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL_QUALIFICATIONS)) {
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				qualifications.add(resultSet.getString(COL_QUALIFICATION_NAME));
			}
		} catch (SQLException e) {
			throw new DevTeamDAOException(e);
		} catch (DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		}
		return qualifications;
	}

	public ArrayList<TechnicalRequirement> findAllTr(Long customerId) throws DevTeamDAOException {
		ArrayList<TechnicalRequirement> requirements = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL_TR)) {
			preparedStatement.setLong(1, customerId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				TechnicalRequirement requirement = new TechnicalRequirementBuilder()
						.requirementId(resultSet.getLong(COL_TR_ID))
						.status(WorkStatus.getRoleType(resultSet.getString(COL_TR_STATUS)))
						.title(resultSet.getString(COL_TR_TITLE)).description(resultSet.getString(COL_TR_DESCRIPTION))
						.time(resultSet.getInt(COL_TR_TIME)).price(resultSet.getDouble(COL_TR_PRICE)).build();
				requirements.add(requirement);
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		}
		return requirements;
	}

	public ArrayList<RequirementItem> findAllTrItems(Long customerId, Long requirementId) throws DevTeamDAOException {
		ArrayList<RequirementItem> items = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_TR_DESCRIPTION)) {
			preparedStatement.setLong(1, customerId);
			preparedStatement.setLong(2, requirementId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				RequirementItem item = new RequirementItemBuilder()
						.qualification(Qualification.getQualificationType(resultSet.getString(COL_ITEM_QUALIFICATION)))
						.requiredDevAmount(resultSet.getInt(COL_ITEM_DEV_NUMBER)).assignedDevAmount(0)
						.itemDescription(resultSet.getString(COL_ITEM_DESCRIPTION)).build();
				items.add(item);
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return items;
	}

	public TechnicalRequirement findRequirement(Long requirementId) throws DevTeamDAOException {
		TechnicalRequirement requirement = null;
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_REQUIREMENT)) {
			preparedStatement.setLong(1, requirementId);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.first()) {
					requirement = new TechnicalRequirementBuilder().requirementId(resultSet.getLong(COL_TR_ID))
							.status(WorkStatus.getRoleType(resultSet.getString(COL_TR_STATUS)))
							.title(resultSet.getString(COL_TR_TITLE))
							.description(resultSet.getString(COL_TR_DESCRIPTION)).time(resultSet.getInt(COL_TR_TIME))
							.price(resultSet.getDouble(COL_TR_PRICE)).build();
				}
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return requirement;
	}

	public Integer findWorkStatusIdByName(String statusName) throws DevTeamDAOException {
		Integer statusId = null;
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_WORK_STATUS)) {
			preparedStatement.setString(1, statusName);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.first()) {
					statusId = resultSet.getInt(COL_WORK_STATUS_ID);
				} else {
					throw new DevTeamDAOException("Impossible to find work status by name");
				}
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return statusId;
	}

	private Integer findQualificationIdByName(String qualificationName) throws DevTeamDAOException {
		Integer qualificationId = null;
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_QUALIFICATION)) {
			preparedStatement.setString(1, qualificationName);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.first()) {
					qualificationId = resultSet.getInt(COL_QUALIFICATION_ID);
				} else {
					throw new DevTeamDAOException("Impossible to find qualification by name");
				}
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return qualificationId;
	}

	public boolean addRequirement(Long customerId, Integer workStatusId, TechnicalRequirement requirement)
			throws DevTeamDAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
			connection.setAutoCommit(false);
			try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_REQUIREMENT)) {
				preparedStatement.setLong(1, customerId);
				preparedStatement.setLong(2, workStatusId);
				preparedStatement.setString(3, requirement.getTitle());
				preparedStatement.setString(4, requirement.getDescription());
				preparedStatement.setInt(5, requirement.getTime());
				preparedStatement.setDouble(6, requirement.getPrice());
				if (preparedStatement.executeUpdate() == 0) {
					connection.rollback();
					connection.setAutoCommit(true);
					return false;
				}
			} catch (SQLException e) {
				connection.rollback();
				connection.setAutoCommit(true);
				throw new DevTeamDAOException(e);
			}
			List<RequirementItem> items = requirement.getItems();
			Iterator<RequirementItem> iterator = items.iterator();
			while (iterator.hasNext()) {
				RequirementItem item = iterator.next();
				try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_ITEM)) {
					preparedStatement.setString(1, requirement.getTitle());
					preparedStatement.setString(2, requirement.getDescription());
					preparedStatement.setLong(3, findQualificationIdByName(item.getQualification().getTypeName()));
					preparedStatement.setInt(4, item.getRequiredDevAmount());
					preparedStatement.setString(5, item.getItemDescription());
					if (preparedStatement.executeUpdate() == 0) {
						connection.rollback();
						connection.setAutoCommit(true);
						return false;
					}
				} catch (SQLException e) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DevTeamDAOException(e);
				}
			}
			connection.commit();
			connection.setAutoCommit(true);
			return true;
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
	}

	public List<BillItem> findBillInfo(Long requirementId) throws DevTeamDAOException {
		List<BillItem> bill = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BILL_ITEMS)) {
			preparedStatement.setLong(1, requirementId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				BillItem item = new BillItemBuilder().requirementId(requirementId)
						.qualification(
								Qualification.getQualificationType(resultSet.getString(COL_BILL_QUALIFICATION_NAME)))
						.time(resultSet.getInt(COL_BILL_TASK_TIME)).rate(resultSet.getInt(COL_BILL_QUALIFICATION_RATE))
						.description(resultSet.getString(COL_BILL_TASK_REQUIREMENT)).build();
				bill.add(item);
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		}
		return bill;
	}

	public boolean makePayment(Long requirementId, Double price, LocalDate beginning, LocalDate ending)
			throws DevTeamDAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
			connection.setAutoCommit(false);
			try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_REQUIREMENT_STATUS)) {
				preparedStatement.setString(1, WorkStatus.ACTIVE.getTypeName());
				preparedStatement.setDouble(2, price);
				preparedStatement.setLong(3, requirementId);
				if (preparedStatement.executeUpdate() == 0) {
					connection.rollback();
					connection.setAutoCommit(true);
					return false;
				}
			} catch (SQLException e) {
				connection.rollback();
				connection.setAutoCommit(true);
				throw new DevTeamDAOException("Impossible to update requirement status after payment", e);
			}
			try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_PROJECT_STATUS)) {
				preparedStatement.setString(1, WorkStatus.ACTIVE.getTypeName());
				preparedStatement.setDate(2, Date.valueOf(beginning));
				preparedStatement.setDate(3, Date.valueOf(ending));
				preparedStatement.setLong(4, requirementId);
				if (preparedStatement.executeUpdate() == 0) {
					connection.rollback();
					connection.setAutoCommit(true);
					return false;
				}
			} catch (SQLException e) {
				connection.rollback();
				connection.setAutoCommit(true);
				throw new DevTeamDAOException("Impossible to update project status after payment", e);
			}
			try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_TASK_STATUS)) {
				preparedStatement.setString(1, WorkStatus.ACTIVE.getTypeName());
				preparedStatement.setDate(2, Date.valueOf(beginning));
				preparedStatement.setDate(3, Date.valueOf(ending));
				preparedStatement.setLong(4, requirementId);
				if (preparedStatement.executeUpdate() == 0) {
					connection.rollback();
					connection.setAutoCommit(true);
					return false;
				}
			} catch (SQLException e) {
				connection.rollback();
				connection.setAutoCommit(true);
				throw new DevTeamDAOException("Impossible to update task status after payment", e);
			}
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		}
		return true;
	}
	
}
