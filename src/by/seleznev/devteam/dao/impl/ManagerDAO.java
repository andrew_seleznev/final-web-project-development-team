package by.seleznev.devteam.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import by.seleznev.devteam.builder.customer.RequirementItemBuilder;
import by.seleznev.devteam.builder.customer.TechnicalRequirementBuilder;
import by.seleznev.devteam.builder.manager.BillItemBuilder;
import by.seleznev.devteam.builder.manager.ProjectBuilder;
import by.seleznev.devteam.builder.manager.TaskBuilder;
import by.seleznev.devteam.builder.visitor.DeveloperBuilder;
import by.seleznev.devteam.dao.BaseDAO;
import by.seleznev.devteam.entity.customer.RequirementItem;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.entity.manager.BillItem;
import by.seleznev.devteam.entity.manager.Project;
import by.seleznev.devteam.entity.manager.Task;
import by.seleznev.devteam.entity.visitor.Developer;
import by.seleznev.devteam.entity.visitor.Visitor;
import by.seleznev.devteam.enumeration.DevStatus;
import by.seleznev.devteam.enumeration.EmploymentType;
import by.seleznev.devteam.enumeration.Qualification;
import by.seleznev.devteam.enumeration.Role;
import by.seleznev.devteam.enumeration.WorkStatus;
import by.seleznev.devteam.exception.DevTeamConnectionPoolException;
import by.seleznev.devteam.exception.DevTeamDAOException;
import by.seleznev.devteam.pool.ConnectionPool;
import by.seleznev.devteam.pool.ProxyConnection;

public class ManagerDAO implements BaseDAO {

	private static final String COL_REQUIREMENT_ID = "requirement_id";
	private static final String COL_REQUIREMENT_STATUS = "status";
	private static final String COL_REQUIREMENT_TITLE = "title";
	private static final String COL_REQUIREMENT_DESCRIPTION = "requirement_description";
	private static final String COL_REQUIREMENT_TIME = "time";
	private static final String COL_REQUIREMENT_PRICE = "price";
	private static final String COL_ITEM_QUALIFICATION = "qualification_name";
	private static final String COL_ITEM_DEV_NUMBER = "dev_number";
	private static final String COL_ITEM_DESCRIPTION = "item_description";
	private static final String COL_DEVELOPER_ID = "visitor_id";
	private static final String COL_DEVELOPER_LOGIN = "login";
	private static final String COL_DEVELOPER_FIRST_NAME = "first_name";
	private static final String COL_DEVELOPER_LAST_NAME = "last_name";
	private static final String COL_DEVELOPER_EMAIL = "email";
	private static final String COL_DEVELOPER_MOBILE = "mobile";
	private static final String COL_DEVELOPER_TYPE = "type_of_employment";
	private static final String COL_PROJECT_ID = "project_id";
	private static final String COL_PROJECT_STATUS = "status";
	private static final String COL_PROJECT_TITLE = "project_title";
	private static final String COL_PROJECT_DESCRIPTION = "project_description";
	private static final String COL_PROJECT_TIME = "project_time";
	private static final String COL_PROJECT_PRICE = "project_price";
	private static final String COL_PROJECT_BEGINING = "project_beginning";
	private static final String COL_PROJECT_ENDING = "project_ending";
	private static final String COL_TASK_ID = "task_id";
	private static final String COL_TASK_DEVELOPER_ID = "developer_id";
	private static final String COL_TASK_STATUS = "status";
	private static final String COL_TASK_DESCRIPTION = "requirement";
	private static final String COL_TASK_TIME = "time";
	private static final String COL_TASK_PROJECT_TIME = "project_time";
	private static final String COL_TASK_BEGINNING = "task_beginning";
	private static final String COL_TASK_ENDING = "task_ending";
	private static final String COL_BILL_QUALIFICATION_NAME = "qualification_name";
	private static final String COL_BILL_TASK_TIME = "time";
	private static final String COL_BILL_QUALIFICATION_RATE = "qualification_rate";
	private static final String COL_BILL_TASK_REQUIREMENT = "requirement";

	private static final String SQL_FIND_ALL_REQUIREMENTS = "SELECT requirement.requirement_id, work_status.status, "
			+ "requirement.title, requirement.requirement_description, requirement.time, requirement.price FROM requirement  "
			+ "JOIN work_status ON requirement.status_id = work_status.status_id ORDER BY work_status.status";
	private static final String SQL_FIND_REQUIREMENT_DETAILS = "SELECT qualification.qualification_name, item.dev_number, "
			+ "item.item_description FROM requirement "
			+ "JOIN item ON requirement.requirement_id = item.requirement_id "
			+ "JOIN qualification ON item.qualification_id = qualification.qualification_id  "
			+ "WHERE requirement.requirement_id = ?";
	private static final String SQL_FIND_REQUIREMENT = "SELECT requirement.requirement_id, work_status.status, "
			+ "requirement.title, requirement.requirement_description, requirement.time, requirement.price FROM requirement "
			+ "JOIN work_status ON requirement.status_id = work_status.status_id "
			+ "WHERE requirement.requirement_id = ?";
	private static final String SQL_FIND_DEVELOPERS = "SELECT visitor.visitor_id, visitor.login, visitor.first_name, "
			+ "visitor.last_name, visitor.email, visitor.mobile, dev_status.dev_status_name, type_of_empl.type_of_employment "
			+ "FROM visitor JOIN role ON role.role_id = visitor.role_id "
			+ "JOIN developer ON developer.developer_id = visitor.visitor_id "
			+ "JOIN qualification ON qualification.qualification_id = developer.qualification_id "
			+ "JOIN dev_status ON dev_status.dev_status_id = developer.dev_status_id "
			+ "JOIN type_of_empl ON type_of_empl.type_of_empl_id = developer.type_of_empl_id "
			+ "WHERE role.role = ? AND qualification.qualification_name = ? AND dev_status.dev_status_name = ?";
	private static final String SQL_FIND_PROJECTS = "SELECT project.project_id, work_status.status, project.project_title, "
			+ "project.project_description, project.project_time, project.project_price, project.project_beginning, "
			+ "project.project_ending FROM project "
			+ "JOIN manager ON manager.manager_id = project.manager_id "
			+ "JOIN work_status ON project.status_id = work_status.status_id "
			+ "JOIN visitor ON visitor.visitor_id = project.manager_id " + "WHERE manager.manager_id = ?";
	private static final String SQL_FIND_PROJECT_BY_ID = "SELECT work_status.status, project.project_title, "
			+ "project.project_description, project.project_time, project.project_price, project.project_beginning, "
			+ "project.project_ending FROM project "
			+ "JOIN work_status ON project.status_id = work_status.status_id " + "WHERE project.project_id = ?";
	private static final String SQL_FIND_PROJECT_DETAILS = "SELECT task.task_id, task.developer_id, work_status.status, "
			+ "task.requirement, task.time, task.project_time, task.task_beginning, task.task_ending FROM task " 
			+ "JOIN work_status ON work_status.status_id = task.status_id WHERE task.project_id = ?";
	private static final String SQL_FIND_BILL_ITEMS = "SELECT qualification.qualification_name, task.time, "
			+ "qualification.qualification_rate, task.requirement FROM task "
			+ "JOIN developer ON developer.developer_id = task.developer_id "
			+ "JOIN qualification ON qualification.qualification_id = developer.qualification_id "
			+ "WHERE task.project_id = ?";
	private static final String SQL_ADD_PROJECT = "INSERT INTO project (project.project_id, project.manager_id, "
			+ "project.status_id, project.project_title, project.project_description, project.project_time) "
			+ "VALUES (?, ?, (SELECT work_status.status_id FROM work_status WHERE work_status.status = ?), ?, ?, ?)";
	private static final String SQL_ADD_TASK = "INSERT INTO task (task.developer_id, task.project_id, task.status_id, "
			+ "task.requirement, task.time) "
			+ "VALUES (?, ?, (SELECT work_status.status_id FROM work_status WHERE work_status.status = ?), ?, ?)";
	private static final String SQL_ADD_BILL = "INSERT INTO bill (bill.requirement_id, bill.qualification_id, "
			+ "bill.working_hours, bill.rate, bill.description) "
			+ "VALUES (?, "
			+ "(SELECT qualification.qualification_id FROM qualification WHERE qualification.qualification_name = ?), "
			+ "?, ?, ?)";
	private static final String SQL_UPDATE_REQUIREMENT_STATUS = "UPDATE requirement "
			+ "SET requirement.status_id = (SELECT work_status.status_id FROM work_status WHERE work_status.status = ?) "
			+ "WHERE requirement.requirement_id = ?";
	private static final String SQL_UPDATE_DEVELOPER_STATUS = "UPDATE developer "
			+ "SET developer.dev_status_id = (SELECT dev_status.dev_status_id FROM dev_status "
			+ "WHERE dev_status.dev_status_name = ?) WHERE developer.developer_id = ?";
	private static final String SQL_UPDATE_PROJECT_STATUS = "UPDATE project "
			+ "SET project.status_id = (SELECT work_status.status_id FROM work_status WHERE work_status.status = ?) "
			+ "WHERE project.project_id = ?";
	private static final String SQL_UPDATE_PROJECT_PRICE = "UPDATE project SET project.project_price = ? "
			+ "WHERE project.project_id = ?";
	private static final String SQL_SET_COMPLETED_STATUS_PROJECT = "UPDATE project "
			+ "SET project.status_id = (SELECT work_status.status_id FROM work_status WHERE work_status.status = ?) "
			+ "WHERE project.project_id = ?";
	private static final String SQL_SET_COMPLETED_STATUS_REQUIREMENT = "UPDATE requirement "
			+ "SET requirement.status_id = (SELECT work_status.status_id FROM work_status WHERE work_status.status = ?) "
			+ "WHERE requirement.requirement_id = ?";
	private static final String SQL_SET_FREE_STATUS_DEVELOPER = "UPDATE developer "
			+ "JOIN task ON task.developer_id = developer.developer_id "
			+ "SET developer.dev_status_id = (SELECT dev_status.dev_status_id FROM dev_status "
			+ "WHERE dev_status.dev_status_name = ?) WHERE developer.developer_id "
			+ "IN (SELECT task.developer_id FROM task WHERE task.project_id = ?)";

	public List<TechnicalRequirement> findAllRequirements() throws DevTeamDAOException {
		List<TechnicalRequirement> requirements = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL_REQUIREMENTS)) {
			while (resultSet.next()) {
				TechnicalRequirement requirement = new TechnicalRequirementBuilder()
						.requirementId(resultSet.getLong(COL_REQUIREMENT_ID))
						.status(WorkStatus.getRoleType(resultSet.getString(COL_REQUIREMENT_STATUS)))
						.title(resultSet.getString(COL_REQUIREMENT_TITLE))
						.description(resultSet.getString(COL_REQUIREMENT_DESCRIPTION))
						.time(resultSet.getInt(COL_REQUIREMENT_TIME)).price(resultSet.getDouble(COL_REQUIREMENT_PRICE))
						.build();
				requirements.add(requirement);
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return requirements;
	}

	public List<BillItem> findBillInfo(Long projectId) throws DevTeamDAOException {
		List<BillItem> bill = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BILL_ITEMS)) {
			preparedStatement.setLong(1, projectId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				BillItem item = new BillItemBuilder()
						.requirementId(projectId)
						.qualification(
								Qualification.getQualificationType(resultSet.getString(COL_BILL_QUALIFICATION_NAME)))
						.time(resultSet.getInt(COL_BILL_TASK_TIME)).rate(resultSet.getInt(COL_BILL_QUALIFICATION_RATE))
						.description(resultSet.getString(COL_BILL_TASK_REQUIREMENT)).build();
				bill.add(item);
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return bill;
	}

	public List<Project> findAllProjects(Visitor visitor) throws DevTeamDAOException {
		List<Project> projects = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_PROJECTS)) {
			preparedStatement.setLong(1, visitor.getVisitorID());
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Project project = new ProjectBuilder().projectId(resultSet.getLong(COL_PROJECT_ID))
						.status(WorkStatus.getRoleType(resultSet.getString(COL_PROJECT_STATUS)))
						.description(resultSet.getString(COL_PROJECT_DESCRIPTION))
						.title(resultSet.getString(COL_PROJECT_TITLE))
						.maxTime(resultSet.getInt(COL_PROJECT_TIME))
						.price(resultSet.getDouble(COL_PROJECT_PRICE))
						.beginning(resultSet.getDate(COL_PROJECT_BEGINING).toLocalDate())
						.ending(resultSet.getDate(COL_PROJECT_ENDING).toLocalDate())
						.build();
				projects.add(project);
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return projects;
	}

	public Project findProjectById(Long projectId) throws DevTeamDAOException {
		Project project = null;
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_PROJECT_BY_ID)) {
			preparedStatement.setLong(1, projectId);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.first()) {
					project = new ProjectBuilder().projectId(projectId)
							.status(WorkStatus.getRoleType(resultSet.getString(COL_PROJECT_STATUS)))
							.description(resultSet.getString(COL_PROJECT_DESCRIPTION))
							.title(resultSet.getString(COL_PROJECT_TITLE))
							.maxTime(resultSet.getInt(COL_PROJECT_TIME))
							.price(resultSet.getDouble(COL_PROJECT_PRICE))
							.beginning(resultSet.getDate(COL_PROJECT_BEGINING).toLocalDate())
							.ending(resultSet.getDate(COL_PROJECT_ENDING).toLocalDate())
							.build();
					project.getTasks().addAll(findProjectDescription(projectId));
				} else {
					throw new DevTeamDAOException("Impossible to find project by id");
				}
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return project;
	}

	private List<Task> findProjectDescription(Long projectId) throws DevTeamDAOException {
		List<Task> tasks = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_PROJECT_DETAILS)) {
			preparedStatement.setLong(1, projectId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Task task = new TaskBuilder().taskId(resultSet.getLong(COL_TASK_ID))
						.developerId(resultSet.getLong(COL_TASK_DEVELOPER_ID))
						.requirement(resultSet.getString(COL_TASK_DESCRIPTION))
						.status(WorkStatus.getRoleType(resultSet.getString(COL_TASK_STATUS)))
						.time(resultSet.getInt(COL_TASK_TIME))
						.projectTime(resultSet.getInt(COL_TASK_PROJECT_TIME))
						.beginning(resultSet.getDate(COL_TASK_BEGINNING).toLocalDate())
						.ending(resultSet.getDate(COL_TASK_ENDING).toLocalDate())
						.build();
				tasks.add(task);
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return tasks;
	}

	public List<RequirementItem> findRequirementDescription(Long requirementId) throws DevTeamDAOException {
		List<RequirementItem> items = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_REQUIREMENT_DETAILS)) {
			preparedStatement.setLong(1, requirementId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				RequirementItem item = new RequirementItemBuilder()
						.qualification(Qualification.getQualificationType(resultSet.getString(COL_ITEM_QUALIFICATION)))
						.requiredDevAmount(resultSet.getInt(COL_ITEM_DEV_NUMBER)).assignedDevAmount(0)
						.itemDescription(resultSet.getString(COL_ITEM_DESCRIPTION)).build();
				items.add(item);
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return items;
	}

	public TechnicalRequirement findRequirementById(Long requirementId) throws DevTeamDAOException {
		TechnicalRequirement requirement = null;
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_REQUIREMENT)) {
			preparedStatement.setLong(1, requirementId);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.first()) {
					requirement = new TechnicalRequirementBuilder().requirementId(resultSet.getLong(COL_REQUIREMENT_ID))
							.status(WorkStatus.getRoleType(resultSet.getString(COL_REQUIREMENT_STATUS)))
							.title(resultSet.getString(COL_REQUIREMENT_TITLE))
							.description(resultSet.getString(COL_REQUIREMENT_DESCRIPTION))
							.time(resultSet.getInt(COL_REQUIREMENT_TIME))
							.price(resultSet.getDouble(COL_REQUIREMENT_PRICE)).build();
				} else {
					throw new DevTeamDAOException("Impossible to find requirement by id");
				}
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return requirement;
	}

	public List<Developer> findDevelopers(Qualification qualification) throws DevTeamDAOException {
		List<Developer> developers = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_DEVELOPERS)) {
			preparedStatement.setString(1, Role.DEVELOPER.getTypeName());
			preparedStatement.setString(2, qualification.getTypeName());
			preparedStatement.setString(3, DevStatus.FREE.getTypeName());
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Developer developer = new DeveloperBuilder().visitorID(resultSet.getLong(COL_DEVELOPER_ID))
						.role(Role.DEVELOPER).login(resultSet.getString(COL_DEVELOPER_LOGIN))
						.firstName(resultSet.getString(COL_DEVELOPER_FIRST_NAME))
						.lastName(resultSet.getString(COL_DEVELOPER_LAST_NAME))
						.email(resultSet.getString(COL_DEVELOPER_EMAIL))
						.mobile(resultSet.getString(COL_DEVELOPER_MOBILE))
						.employmentType(EmploymentType.getEmploymentType(resultSet.getString(COL_DEVELOPER_TYPE)))
						.qualification(qualification).devStatus(DevStatus.FREE).build();
				developers.add(developer);
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return developers;
	}
	
	public boolean refuseRequirement(TechnicalRequirement requirement) throws DevTeamDAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_REQUIREMENT_STATUS)) {
			preparedStatement.setString(1, WorkStatus.CANCELED.getTypeName());
			preparedStatement.setLong(2, requirement.getRequirementId());
			if (preparedStatement.executeUpdate() == 0) {
				return false;
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return true;
	}

	public boolean addProject(Visitor visitor, TechnicalRequirement technicalRequirement, Project project)
			throws DevTeamDAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
			connection.setAutoCommit(false);
			try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_PROJECT)) {
				preparedStatement.setLong(1, project.getProjectId());
				preparedStatement.setLong(2, visitor.getVisitorID());
				preparedStatement.setString(3, project.getStatus().getTypeName());
				preparedStatement.setString(4, project.getTitle());
				preparedStatement.setString(5, project.getDescription());
				preparedStatement.setInt(6, technicalRequirement.getTime());
				if (preparedStatement.executeUpdate() == 0) {
					connection.rollback();
					connection.setAutoCommit(true);
					return false;
				}
			} catch (SQLException e) {
				connection.rollback();
				connection.setAutoCommit(true);
				throw new DevTeamDAOException("Impossible to add new project", e);
			}
			List<Task> tasks = project.getTasks();
			Iterator<Task> iterator = tasks.iterator();
			while (iterator.hasNext()) {
				Task task = iterator.next();
				try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_TASK)) {
					preparedStatement.setLong(1, task.getDeveloperId());
					preparedStatement.setLong(2, project.getProjectId());
					preparedStatement.setString(3, task.getStatus().getTypeName());
					preparedStatement.setString(4, task.getRequirement());
					preparedStatement.setInt(5, 0);
					if (preparedStatement.executeUpdate() == 0) {
						connection.rollback();
						connection.setAutoCommit(true);
						return false;
					}
				} catch (SQLException e) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DevTeamDAOException("Impossible to add new task", e);
				}
				try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_DEVELOPER_STATUS)) {
					preparedStatement.setString(1, DevStatus.BUSY.getTypeName());
					preparedStatement.setLong(2, task.getDeveloperId());
					if (preparedStatement.executeUpdate() == 0) {
						connection.rollback();
						connection.setAutoCommit(true);
						return false;
					}
				} catch (SQLException e) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DevTeamDAOException("Impossible to update developer status", e);
				}
			}
			try (PreparedStatement preparedStatement = connection
					.prepareStatement(SQL_UPDATE_REQUIREMENT_STATUS)) {
				preparedStatement.setString(1, WorkStatus.ACCEPTED.getTypeName());
				preparedStatement.setLong(2, technicalRequirement.getRequirementId());
				if (preparedStatement.executeUpdate() == 0) {
					connection.rollback();
					connection.setAutoCommit(true);
					return false;
				}
			} catch (SQLException e) {
				connection.rollback();
				connection.setAutoCommit(true);
				throw new DevTeamDAOException("Impossible to update requirement status", e);
			}
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return true;
	}

	public boolean addBill(Long projectId, List<BillItem> bill, Double price) throws DevTeamDAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
			connection.setAutoCommit(false);
			Iterator<BillItem> iterator = bill.iterator();
			while (iterator.hasNext()) {
				BillItem item = iterator.next();
				try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_BILL)) {
					preparedStatement.setLong(1, projectId);
					preparedStatement.setString(2, item.getQualification().getTypeName());
					preparedStatement.setInt(3, item.getTime());
					preparedStatement.setInt(4, item.getRate());
					preparedStatement.setString(5, item.getDescription());
					if (preparedStatement.executeUpdate() == 0) {
						connection.rollback();
						connection.setAutoCommit(true);
						return false;
					}
				} catch (SQLException e) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DevTeamDAOException("Impossible to add new bill", e);
				}
				try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_REQUIREMENT_STATUS)) {
					preparedStatement.setString(1, WorkStatus.WAITING_PAYMENT.getTypeName());
					preparedStatement.setLong(2, projectId);
					if (preparedStatement.executeUpdate() == 0) {
						connection.rollback();
						connection.setAutoCommit(true);
						return false;
					}
				} catch (SQLException e) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DevTeamDAOException("Impossible to update requirement status", e);
				}
				try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_PROJECT_STATUS)) {
					preparedStatement.setString(1, WorkStatus.WAITING_PAYMENT.getTypeName());
					preparedStatement.setLong(2, projectId);
					if (preparedStatement.executeUpdate() == 0) {
						connection.rollback();
						connection.setAutoCommit(true);
						return false;
					}
				} catch (SQLException e) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DevTeamDAOException("Impossible to update project status", e);
				}
				try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_PROJECT_PRICE)) {
					preparedStatement.setDouble(1, price);
					preparedStatement.setLong(2, projectId);
					if (preparedStatement.executeUpdate() == 0) {
						connection.rollback();
						connection.setAutoCommit(true);
						return false;
					}
				} catch (SQLException e) {
					connection.rollback();
					connection.setAutoCommit(true);
					throw new DevTeamDAOException("Impossible to update project price", e);
				}
			}
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return true;
	}
	
	public boolean completeProject(Long projectId) throws DevTeamDAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
			connection.setAutoCommit(false);
			try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SET_COMPLETED_STATUS_PROJECT)) {
				preparedStatement.setString(1, WorkStatus.COMPLETED.getTypeName());
				preparedStatement.setLong(2, projectId);
				if (preparedStatement.executeUpdate() == 0) {
					connection.rollback();
					connection.setAutoCommit(true);
					return false;
				}
			} catch (SQLException e) {
				connection.rollback();
				connection.setAutoCommit(true);
				throw new DevTeamDAOException("Impossible to set project completed status", e);
			}
			try (PreparedStatement preparedStatement = connection
					.prepareStatement(SQL_SET_COMPLETED_STATUS_REQUIREMENT)) {
				preparedStatement.setString(1, WorkStatus.COMPLETED.getTypeName());
				preparedStatement.setLong(2, projectId);
				if (preparedStatement.executeUpdate() == 0) {
					connection.rollback();
					connection.setAutoCommit(true);
					return false;
				}
			} catch (SQLException e) {
				connection.rollback();
				connection.setAutoCommit(true);
				throw new DevTeamDAOException("Impossible to set requirement completed status", e);
			}
			try (PreparedStatement preparedStatement = connection
					.prepareStatement(SQL_SET_FREE_STATUS_DEVELOPER)) {
				preparedStatement.setString(1, DevStatus.FREE.getTypeName());
				preparedStatement.setLong(2, projectId);
				if (preparedStatement.executeUpdate() == 0) {
					connection.rollback();
					connection.setAutoCommit(true);
					return false;
				}
			} catch (SQLException e) {
				connection.rollback();
				connection.setAutoCommit(true);
				throw new DevTeamDAOException("Impossible to set free developer status", e);
			}
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return true;
	}
}
