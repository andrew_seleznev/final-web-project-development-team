package by.seleznev.devteam.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.seleznev.devteam.builder.manager.TaskBuilder;
import by.seleznev.devteam.dao.BaseDAO;
import by.seleznev.devteam.entity.manager.Task;
import by.seleznev.devteam.enumeration.WorkStatus;
import by.seleznev.devteam.exception.DevTeamConnectionPoolException;
import by.seleznev.devteam.exception.DevTeamDAOException;
import by.seleznev.devteam.pool.ConnectionPool;
import by.seleznev.devteam.pool.ProxyConnection;

public class DeveloperDAO implements BaseDAO {
	
	private static final String COL_TASK_ID = "task_id";
	private static final String COL_TASK_DEVELOPER_ID = "developer_id";
	private static final String COL_TASK_STATUS = "status";
	private static final String COL_TASK_REQUIREMENT = "requirement";
	private static final String COL_TASK_TIME = "time";
	private static final String COL_TASK_PROJECT_TIME = "project_time";
	private static final String COL_TASK_BEGINNING = "task_beginning";
	private static final String COL_TASK_ENDING = "task_ending";

	private static final String SQL_FIND_TASKS = "SELECT task.task_id, task.developer_id, work_status.status, task.requirement, "
			+ "task.time, task.project_time, task.task_beginning, task.task_ending FROM task "
			+ "JOIN work_status ON work_status.status_id = task.status_id "
			+ "WHERE task.developer_id = ?";
	private static final String SQL_UPDATE_TASK_STATUS = "UPDATE task SET task.time = ?, "
			+ "task.status_id = (SELECT work_status.status_id FROM work_status WHERE work_status.status = ?) "
			+ "WHERE task.task_id = ?";
	private static final String SQL_SET_COMPLETE_TASK_STATUS = "UPDATE task "
			+ "SET task.status_id = (SELECT work_status.status_id FROM work_status WHERE work_status.status = ?) "
			+ "WHERE task.task_id = ?";
	
	public List<Task> findTasks(Long developerId) throws DevTeamDAOException {
		List<Task> tasks = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_TASKS)) {
			preparedStatement.setLong(1, developerId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Task task = new TaskBuilder()
						.taskId(resultSet.getLong(COL_TASK_ID))
						.developerId(resultSet.getLong(COL_TASK_DEVELOPER_ID))
						.status(WorkStatus.getRoleType(resultSet.getString(COL_TASK_STATUS)))
						.requirement(resultSet.getString(COL_TASK_REQUIREMENT))
						.time(resultSet.getInt(COL_TASK_TIME))
						.projectTime(resultSet.getInt(COL_TASK_PROJECT_TIME))
						.beginning(resultSet.getDate(COL_TASK_BEGINNING).toLocalDate())
						.ending(resultSet.getDate(COL_TASK_ENDING).toLocalDate())
						.build();
				tasks.add(task);
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return tasks;
	}
	
	public boolean updateTaskStatus(Integer time, Long taskId) throws DevTeamDAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_TASK_STATUS)) {
			preparedStatement.setInt(1, time);
			preparedStatement.setString(2, WorkStatus.ACCEPTED.getTypeName());
			preparedStatement.setLong(3, taskId);
			if (preparedStatement.executeUpdate() == 0) {
				return false;
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return true;
	}
	
	public boolean completeTask(Long taskId) throws DevTeamDAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_SET_COMPLETE_TASK_STATUS)) {
			preparedStatement.setString(1, WorkStatus.COMPLETED.getTypeName());
			preparedStatement.setLong(2, taskId);
			if (preparedStatement.executeUpdate() == 0) {
				return false;
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return true;
	}
	
}
