package by.seleznev.devteam.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.seleznev.devteam.builder.customer.TechnicalRequirementBuilder;
import by.seleznev.devteam.dao.BaseDAO;
import by.seleznev.devteam.entity.customer.TechnicalRequirement;
import by.seleznev.devteam.enumeration.WorkStatus;
import by.seleznev.devteam.exception.DevTeamConnectionPoolException;
import by.seleznev.devteam.exception.DevTeamDAOException;
import by.seleznev.devteam.pool.ConnectionPool;
import by.seleznev.devteam.pool.ProxyConnection;

public class AdminDAO implements BaseDAO {

	private static final String COL_TR_ID = "requirement_id";
	private static final String COL_TR_STATUS = "status";
	private static final String COL_TR_TITLE = "title";
	private static final String COL_TR_DESCRIPTION = "requirement_description";
	private static final String COL_TR_TIME = "time";
	private static final String COL_TR_PRICE = "price";

	private static final String SQL_FIND_ALL_TR = "SELECT requirement.requirement_id, work_status.status, requirement.title, "
			+ "requirement.requirement_description, requirement.time, requirement.price FROM requirement "
			+ "JOIN work_status ON requirement.status_id = work_status.status_id";
	private static final String SQL_DELETE_TECHNICAL_REQUIREMENT = "DELETE FROM requirement WHERE requirement.requirement_id = ?";

	public List<TechnicalRequirement> findAllTr() throws DevTeamDAOException {
		List<TechnicalRequirement> requirements = new ArrayList<>();
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL_TR)) {
			while (resultSet.next()) {
				TechnicalRequirement requirement = new TechnicalRequirementBuilder()
						.requirementId(resultSet.getLong(COL_TR_ID))
						.status(WorkStatus.getRoleType(resultSet.getString(COL_TR_STATUS)))
						.title(resultSet.getString(COL_TR_TITLE)).description(resultSet.getString(COL_TR_DESCRIPTION))
						.time(resultSet.getInt(COL_TR_TIME)).price(resultSet.getDouble(COL_TR_PRICE)).build();
				requirements.add(requirement);
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return requirements;
	}
	
	public boolean deleteCompleteRequirement(Long requirementId) throws DevTeamDAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_TECHNICAL_REQUIREMENT)) {
			preparedStatement.setLong(1, requirementId);
			if (preparedStatement.executeUpdate() == 0) {
				return false;
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return true;
	}

}
