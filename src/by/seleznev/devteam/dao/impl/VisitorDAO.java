package by.seleznev.devteam.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import by.seleznev.devteam.builder.visitor.AdminBuilder;
import by.seleznev.devteam.builder.visitor.CustomerBuilder;
import by.seleznev.devteam.builder.visitor.DeveloperBuilder;
import by.seleznev.devteam.builder.visitor.ManagerBuilder;
import by.seleznev.devteam.dao.BaseDAO;
import by.seleznev.devteam.entity.visitor.Admin;
import by.seleznev.devteam.entity.visitor.Customer;
import by.seleznev.devteam.entity.visitor.Developer;
import by.seleznev.devteam.entity.visitor.Manager;
import by.seleznev.devteam.entity.visitor.Visitor;
import by.seleznev.devteam.enumeration.DevStatus;
import by.seleznev.devteam.enumeration.EmploymentType;
import by.seleznev.devteam.enumeration.Qualification;
import by.seleznev.devteam.enumeration.Role;
import by.seleznev.devteam.exception.DevTeamConnectionPoolException;
import by.seleznev.devteam.exception.DevTeamDAOException;
import by.seleznev.devteam.pool.ConnectionPool;
import by.seleznev.devteam.pool.ProxyConnection;

public class VisitorDAO implements BaseDAO {

	private static final String COL_VISITOR_ID = "visitor_id";
	private static final String COL_PASSWORD = "password";
	private static final String COL_ROLE_NAME = "role";
	private static final String COL_LOGIN = "login";
	private static final String COL_FIRST_NAME = "first_name";
	private static final String COL_LAST_NAME = "last_name";
	private static final String COL_EMAIL = "email";
	private static final String COL_MOBILE = "mobile";
	private static final String COL_TYPE_OF_EMPLOYMENT = "type_of_employment";
	private static final String COL_QUALIFICATION = "qualification_name";
	private static final String COL_DEV_STATUS = "dev_status_name";
	private static final String COL_COMPANY = "company";

	private static final String SQL_FIND_VISITOR_ROLE = "SELECT visitor.password, role.role FROM role JOIN visitor "
			+ "ON role.role_id = visitor.role_id WHERE visitor.login = ? AND visitor.password = ?";

	private static final String SQL_FIND_ADMIN = "SELECT visitor.visitor_id, visitor.login, visitor.password, visitor.first_name, "
			+ "visitor.last_name, visitor.email, visitor.mobile, role.role, type_of_empl.type_of_employment FROM visitor "
			+ "JOIN role ON visitor.role_id = role.role_id  "
			+ "JOIN admin ON admin.admin_id = visitor.visitor_id "
			+ "JOIN type_of_empl ON type_of_empl.type_of_empl_id = admin.type_of_empl_id "
			+ "WHERE visitor.login = ? AND visitor.password = ?";
	
	private static final String SQL_FIND_CUSTOMER = "SELECT visitor.visitor_id, visitor.login, visitor.password, visitor.first_name, "
			+ "visitor.last_name, visitor.email, visitor.mobile, role.role, customer.company FROM visitor "
			+ "JOIN role ON visitor.role_id = role.role_id  "
			+ "JOIN customer ON visitor.visitor_id = customer.customer_id "
			+ "WHERE visitor.login = ? AND visitor.password = ?";
	
	private static final String SQL_FIND_MANAGER = "SELECT visitor.visitor_id, visitor.login, visitor.password, visitor.first_name, "
			+ "visitor.last_name, visitor.email, visitor.mobile, role.role, type_of_empl.type_of_employment FROM visitor "
			+ "JOIN role ON visitor.role_id = role.role_id "
			+ "JOIN manager ON manager.manager_id = visitor.visitor_id "
			+ "JOIN type_of_empl ON type_of_empl.type_of_empl_id = manager.type_of_empl_id "
			+ "WHERE visitor.login = ? AND visitor.password = ?";
	
	private static final String SQL_FIND_DEVELOPER = "SELECT visitor.visitor_id, visitor.login, visitor.password, "
			+ "visitor.first_name, visitor.last_name, visitor.email, visitor.mobile, role.role, type_of_empl.type_of_employment, "
			+ "qualification.qualification_name, dev_status.dev_status_name FROM visitor "
			+ "JOIN role ON visitor.role_id = role.role_id "
			+ "JOIN developer ON developer.developer_id = visitor.visitor_id "
			+ "JOIN dev_status ON dev_status.dev_status_id = developer.dev_status_id "
			+ "JOIN qualification ON qualification.qualification_id = developer.qualification_id "
			+ "JOIN type_of_empl ON type_of_empl.type_of_empl_id = developer.type_of_empl_id "
			+ "WHERE visitor.login = ? AND visitor.password = ?";
	
	private static final String SQL_ADD_NEW_VISITOR = "INSERT INTO visitor (visitor.role_id, visitor.login, visitor.password, "
			+ "visitor.first_name, visitor.last_name, visitor.email, visitor.mobile) "
			+ "VALUES((SELECT role.role_id FROM role WHERE role.role = ?), ?, ?, ?, ?, ?, ?)";
	
	private static final String SQL_ADD_NEW_CUSTOMER = "INSERT INTO customer (customer.customer_id, customer.company) "
			+ "VALUES((SELECT visitor.visitor_id FROM visitor WHERE visitor.login = ? AND visitor.password = ?), ?)";

	public String findVisitorRole(String login, String password) throws DevTeamDAOException {
		String role = null;
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_VISITOR_ROLE);) {
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, password);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.first()) {
					if (password.equals(resultSet.getString(COL_PASSWORD))) {
						role = resultSet.getString(COL_ROLE_NAME);
					} else {
						throw new DevTeamDAOException();
					}
				} else {
					throw new DevTeamDAOException();
				}
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return role;
	}

	public Visitor findAdmin(String login, String password) throws DevTeamDAOException {
		Admin admin = null;
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ADMIN);) {
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, password);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.first()) {
					if (password.equals(resultSet.getString(COL_PASSWORD))) {
						admin = new AdminBuilder().visitorID(resultSet.getLong(COL_VISITOR_ID))
								.role(Role.getRoleType(resultSet.getString(COL_ROLE_NAME)))
								.login(resultSet.getString(COL_LOGIN))
								.firstName(resultSet.getString(COL_FIRST_NAME))
								.lastName(resultSet.getString(COL_LAST_NAME))
								.email(resultSet.getString(COL_EMAIL))
								.mobile(resultSet.getString(COL_MOBILE))
								.employmentType(EmploymentType.getEmploymentType(resultSet.getString(COL_TYPE_OF_EMPLOYMENT)))
								.build();
					} else {
						throw new DevTeamDAOException();
					}
				} else {
					throw new DevTeamDAOException();
				}
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return admin;
	}
	
	public Visitor findManager(String login, String password) throws DevTeamDAOException {
		Manager manager = null;
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_MANAGER);) {
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, password);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.first()) {
					if (password.equals(resultSet.getString(COL_PASSWORD))) {
						manager = new ManagerBuilder().visitorID(resultSet.getLong(COL_VISITOR_ID))
								.role(Role.getRoleType(resultSet.getString(COL_ROLE_NAME)))
								.login(resultSet.getString(COL_LOGIN))
								.firstName(resultSet.getString(COL_FIRST_NAME))
								.lastName(resultSet.getString(COL_LAST_NAME))
								.email(resultSet.getString(COL_EMAIL))
								.mobile(resultSet.getString(COL_MOBILE))
								.employmentType(EmploymentType.getEmploymentType(resultSet.getString(COL_TYPE_OF_EMPLOYMENT)))
								.build();
					} else {
						throw new DevTeamDAOException();
					}
				} else {
					throw new DevTeamDAOException();
				}
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return manager;
	}
	
	public Visitor findCustomer(String login, String password) throws DevTeamDAOException {
		Customer customer = null;
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_CUSTOMER);) {
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, password);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.first()) {
					if (password.equals(resultSet.getString(COL_PASSWORD))) {
						customer = new CustomerBuilder().visitorID(resultSet.getLong(COL_VISITOR_ID))
								.role(Role.getRoleType(resultSet.getString(COL_ROLE_NAME)))
								.login(resultSet.getString(COL_LOGIN))
								.firstName(resultSet.getString(COL_FIRST_NAME))
								.lastName(resultSet.getString(COL_LAST_NAME))
								.email(resultSet.getString(COL_EMAIL))
								.mobile(resultSet.getString(COL_MOBILE))
								.company(resultSet.getString(COL_COMPANY)).build();
					} else {
						throw new DevTeamDAOException();
					}
				} else {
					throw new DevTeamDAOException();
				}
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return customer;
	}
	
	public Visitor findDeveloper(String login, String password) throws DevTeamDAOException {
		Developer developer = null;
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_DEVELOPER);) {
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, password);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				if (resultSet.first()) {
					if (password.equals(resultSet.getString(COL_PASSWORD))) {
						developer = new DeveloperBuilder()
								.visitorID(resultSet.getLong(COL_VISITOR_ID))
								.role(Role.getRoleType(resultSet.getString(COL_ROLE_NAME)))
								.login(resultSet.getString(COL_LOGIN))
								.firstName(resultSet.getString(COL_FIRST_NAME))
								.lastName(resultSet.getString(COL_LAST_NAME))
								.email(resultSet.getString(COL_EMAIL))
								.mobile(resultSet.getString(COL_MOBILE))
								.employmentType(EmploymentType.getEmploymentType(resultSet.getString(COL_TYPE_OF_EMPLOYMENT)))
								.qualification(Qualification.getQualificationType(resultSet.getString(COL_QUALIFICATION)))
								.devStatus(DevStatus.getDevStatusType(resultSet.getString(COL_DEV_STATUS)))
								.build();
					} else {
						throw new DevTeamDAOException();
					}
				} else {
					throw new DevTeamDAOException();
				}
			}
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return developer;
	}

	public boolean addCustomer(String firstName, String lastName, String company, String login, String password,
			String email, Integer mobile) throws DevTeamDAOException {
		try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
			connection.setAutoCommit(false);
			try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_NEW_VISITOR)) {
				preparedStatement.setString(1, Role.CUSTOMER.getTypeName());
				preparedStatement.setString(2, login);
				preparedStatement.setString(3, password);
				preparedStatement.setString(4, firstName);
				preparedStatement.setString(5, lastName);
				preparedStatement.setString(6, email);
				preparedStatement.setInt(7, mobile);
				if (preparedStatement.executeUpdate() == 0) {
					connection.rollback();
					connection.setAutoCommit(true);
					return false;
				}
			} catch (SQLException e) {
				connection.rollback();
				connection.setAutoCommit(true);
				throw new DevTeamDAOException(e);
			}
			try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_NEW_CUSTOMER)) {
				preparedStatement.setString(1, login);
				preparedStatement.setString(2, password);
				preparedStatement.setString(3, company);
				if (preparedStatement.executeUpdate() == 0) {
					connection.rollback();
					connection.setAutoCommit(true);
					return false;
				}
			} catch (SQLException e) {
				connection.rollback();
				connection.setAutoCommit(true);
				throw new DevTeamDAOException(e);
			}
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException | DevTeamConnectionPoolException e) {
			throw new DevTeamDAOException(e);
		} 
		return true;
	}
}
