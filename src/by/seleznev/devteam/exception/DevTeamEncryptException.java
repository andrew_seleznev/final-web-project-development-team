package by.seleznev.devteam.exception;

public class DevTeamEncryptException extends Exception {

	private static final long serialVersionUID = 1L;

	public DevTeamEncryptException() {
		super();
	}

	public DevTeamEncryptException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DevTeamEncryptException(String message, Throwable cause) {
		super(message, cause);
	}

	public DevTeamEncryptException(String message) {
		super(message);
	}

	public DevTeamEncryptException(Throwable cause) {
		super(cause);
	}
}
