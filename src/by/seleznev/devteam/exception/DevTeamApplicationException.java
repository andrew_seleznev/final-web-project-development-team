package by.seleznev.devteam.exception;

public class DevTeamApplicationException extends Exception {

	private static final long serialVersionUID = 1L;

	public DevTeamApplicationException() {
		super();
	}

	public DevTeamApplicationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DevTeamApplicationException(String message, Throwable cause) {
		super(message, cause);
	}

	public DevTeamApplicationException(String message) {
		super(message);
	}

	public DevTeamApplicationException(Throwable cause) {
		super(cause);
	}
}
