package by.seleznev.devteam.exception;

public class DevTeamConnectionPoolException extends Exception {

	private static final long serialVersionUID = 1L;

	public DevTeamConnectionPoolException() {
		super();
	}

	public DevTeamConnectionPoolException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DevTeamConnectionPoolException(String message, Throwable cause) {
		super(message, cause);
	}

	public DevTeamConnectionPoolException(String message) {
		super(message);
	}

	public DevTeamConnectionPoolException(Throwable cause) {
		super(cause);
	}
}
