package by.seleznev.devteam.exception;

public class DevTeamDAOException extends Exception {

	private static final long serialVersionUID = 1L;

	public DevTeamDAOException() {
		super();
	}

	public DevTeamDAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DevTeamDAOException(String message, Throwable cause) {
		super(message, cause);
	}

	public DevTeamDAOException(String message) {
		super(message);
	}

	public DevTeamDAOException(Throwable cause) {
		super(cause);
	}
}
