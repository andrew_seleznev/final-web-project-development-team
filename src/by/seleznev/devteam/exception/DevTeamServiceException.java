package by.seleznev.devteam.exception;

public class DevTeamServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	public DevTeamServiceException() {
		super();
	}

	public DevTeamServiceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DevTeamServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public DevTeamServiceException(String message) {
		super(message);
	}

	public DevTeamServiceException(Throwable cause) {
		super(cause);
	}
}
