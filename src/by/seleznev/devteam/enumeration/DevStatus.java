package by.seleznev.devteam.enumeration;

public enum DevStatus {
	
	FREE("free"),
	BUSY("busy");
	
	private String typeName;

	private DevStatus(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeName() {
		return typeName;
	}
	
	public static DevStatus getDevStatusType(String typeName) {
		for (DevStatus element : values()) {
			if (typeName.equals(element.getTypeName())) {
				return element;
			}
		}
		return null;
	}
}
