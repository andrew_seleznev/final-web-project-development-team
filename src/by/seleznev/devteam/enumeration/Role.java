package by.seleznev.devteam.enumeration;

public enum Role {
	
	ADMIN("admin"), 
	GUEST("guest"), 
	CUSTOMER("customer"), 
	DEVELOPER("developer"), 
	MANAGER("manager");
	
	private String typeName;

	private Role(String typeName) {
		this.typeName = typeName;
	}
	
	public String getTypeName() {
		return typeName;
	}
	
	public static Role getRoleType(String typeName) {
		for (Role element : values()) {
			if (typeName.equals(element.getTypeName())) {
				return element;
			}
		}
		return null;
	}
}
