package by.seleznev.devteam.enumeration;

public enum Qualification {
	
	SYSTEMS_ENGINEER("Systems Engineer"),
	IT_PROJECT_MANAGER("IT Project Manager"),
	SYSTEMS_ADMINISTRATOR("Systems Administrator"),
	NETWORK_ENGINEER("Network Engineer"),
	BUSINESS_ANALYST("Business Analyst"),
	QA_TESTING_ENGINEER("Quality Assurance /Software Testing Engineer"),
	ARCHITECT("Architect"),
	IT_SYSTEMS_ANALYST("IT Systems Analyst"),
	DEVELOPER("Developer"),
	DESKTOP_SUPPORT("Help Desk/Desktop Support");
	
	private String typeName;

	private Qualification(String typeName) {
		this.typeName = typeName;
	}
	
	public String getTypeName() {
		return typeName;
	}

	public static Qualification getQualificationType(String typeName) {
		for (Qualification element : values()) {
			if (typeName.equals(element.getTypeName())) {
				return element;
			}
		}
		return null;
	}
}
