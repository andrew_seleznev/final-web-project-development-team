package by.seleznev.devteam.enumeration;

public enum RedirectionType {
	
	FORWARD, SEND_REDIRECT
	
}
