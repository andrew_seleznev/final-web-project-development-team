package by.seleznev.devteam.enumeration;

public enum EmploymentType {
	
	FULL_TIME("Full-time"),
	PART_TIME("Part-time"),
	PROBATION("Probation"),
	OUTWORKERS("Outworkers");
	
	private String typeName;

	private EmploymentType(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeName() {
		return typeName;
	}
	
	public static EmploymentType getEmploymentType(String typeName) {
		for (EmploymentType element : values()) {
			if (typeName.equals(element.getTypeName())) {
				return element;
			}
		}
		return null;
	}
}
