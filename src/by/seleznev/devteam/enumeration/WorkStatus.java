package by.seleznev.devteam.enumeration;

public enum WorkStatus {
	
	NEW("new"),
	ACCEPTED("accepted"),
	WAITING_PAYMENT("waiting for payment"),
	ACTIVE("active"),
	COMPLETED("completed"),
	CANCELED("canceled");
	
	private String typeName;

	private WorkStatus(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeName() {
		return typeName;
	}

	public static WorkStatus getRoleType(String typeName) {
		for (WorkStatus element : values()) {
			if (typeName.equals(element.getTypeName())) {
				return element;
			}
		}
		return null;
	}
}
